<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCabangsSubcabangsDealersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cabangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->timestamps();
		});

		Schema::create('subcabangs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('cabang_id')->nullable()->unsigned()->index();
			// $table->foreign('cabang_id')->references('id')->on('cabangs')->onDelete('cascade');
			$table->timestamps();
		});

		Schema::create('dealers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('subcabang_id')->nullable()->unsigned()->index();
			// $table->foreign('subcabang_id')->references('id')->on('subcabangs')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dealers');
		Schema::drop('subcabangs');
		Schema::drop('cabangs');
	}

}
