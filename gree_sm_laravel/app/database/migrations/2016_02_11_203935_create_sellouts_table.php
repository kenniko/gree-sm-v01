<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSelloutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sellouts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sales_id')->unsigned()->index();
			$table->integer('cabang_id')->unsigned()->index();
			$table->integer('subcabang_id')->unsigned()->index();
			$table->integer('dealer_id')->unsigned()->index();
			$table->integer('spm_id')->unsigned()->index();
			$table->string('type');
			$table->integer('customer_id')->unsigned()->index();
			$table->datetime('datetime');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sellouts');
	}

}
