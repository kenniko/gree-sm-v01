<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sellins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sales_id')->unsigned()->index();
			$table->integer('cabang_id')->unsigned()->index();
			$table->integer('subcabang_id')->unsigned()->index();
			$table->integer('dealer_id')->unsigned()->index();
			$table->dateTime('datetime');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sellins');
	}

}
