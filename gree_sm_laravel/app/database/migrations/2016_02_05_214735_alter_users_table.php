<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('username')->after('email');
			$table->enum('gender', array('Male', 'Female'))->after('last_name');
			$table->string('phone')->after('gender');
			$table->integer('cabang_id')->nullable()->unsigned()->index()->after('phone');
			$table->integer('dealer_id')->nullable()->unsigned()->index()->after('cabang_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('phone');
			$table->dropColumn('gender');
			$table->dropColumn('username');
			$table->dropColumn('cabang_id');
			$table->dropColumn('dealer_id');
		});
	}

}
