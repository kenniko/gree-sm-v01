<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RangkingsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$cabangs = Cabang::all();

		foreach($cabangs as &$cabang)
		{
			$cabang->percentage = $faker->numberBetween($min = 30, $max = 70);
			$cabang->save();
		}
	}

}