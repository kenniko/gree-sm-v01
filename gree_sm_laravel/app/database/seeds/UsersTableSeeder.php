<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{

		$faker = Faker\Factory::create();

		//
		// truncate DB
		// ==========================================================================
			DB::table('users')->truncate();
			DB::table('roles')->truncate();
			DB::table('role_users')->truncate();


		//
		// create Super Admin Role and Account
		// ==========================================================================

			Sentinel::getRoleRepository()->createModel()->create([
				'name' => 'Super Admin',
				'slug' => 'superadmin',
				'permissions' => [
					'superadmin' => true,
					'sellin' => true,
					'sellout' => true,
					'stockopname' => true,
					'report' => true,
					'rangking' => true,
					'cabang' => true,
					'subcabang' => true,
					'dealer' => true,
					'product' => true,
					'customer' => true,
					'user' => true,
					'role' => true
				]
			]);
			$superadminRole = Sentinel::findRoleBySlug('superadmin');

			$superadminUser = Sentinel::registerAndActivate([
				'email'    		=> 'kenniko.okta@gmail.com',
				'username'    	=> 'superadmin',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
			]);

			$superadminRole->users()->attach($superadminUser);


		//
		// create Sales Role and Account
		// ==========================================================================
		
			Sentinel::getRoleRepository()->createModel()->create([
				'name' => 'Sales',
				'slug' => 'sales',
				'permissions' => [
					'sellin' => true,
					'stockopname' => true,
					'report' => true,
					'product' => true
				]
			]);
			$salesRole = Sentinel::findRoleBySlug('sales');

			$cabangs = Cabang::all();

			$sales01 = Sentinel::registerAndActivate([
				'email'    		=> 'sales01@gmail.com',
				'username'    	=> 'sales01',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameFemale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'cabang_id'		=> $cabangs[0]->id
			]);
			$salesRole->users()->attach($sales01);

			$sales02 = Sentinel::registerAndActivate([
				'email'    		=> 'sales02@gmail.com',
				'username'    	=> 'sales02',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameFemale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'cabang_id'		=> $cabangs[1]->id
			]);
			$salesRole->users()->attach($sales02);


		//
		// create SPM Role and Account
		// ==========================================================================
		
			Sentinel::getRoleRepository()->createModel()->create([
				'name' => 'Sales Promotion Man',
				'slug' => 'spm',
				'permissions' => [
					'sellout' => true,
					'stockopname' => true,
					'report' => true,
					'product' => true,
					'customer' => true
				]
			]);
			$spmRole = Sentinel::findRoleBySlug('spm');

			$dealers = Dealer::all();

			$spm01 = Sentinel::registerAndActivate([
				'email'    		=> 'spm01@gmail.com',
				'username'    	=> 'spm01',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[0]->id
			]);
			$spmRole->users()->attach($spm01);

			$spm02 = Sentinel::registerAndActivate([
				'email'    		=> 'spm02@gmail.com',
				'username'    	=> 'spm02',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[1]->id
			]);
			$spmRole->users()->attach($spm02);

			$spm03 = Sentinel::registerAndActivate([
				'email'    		=> 'spm03@gmail.com',
				'username'    	=> 'spm03',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[2]->id
			]);
			$spmRole->users()->attach($spm03);

			$spm04 = Sentinel::registerAndActivate([
				'email'    		=> 'spm04@gmail.com',
				'username'    	=> 'spm04',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[3]->id
			]);
			$spmRole->users()->attach($spm04);

			$spm05 = Sentinel::registerAndActivate([
				'email'    		=> 'spm05@gmail.com',
				'username'    	=> 'spm05',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[4]->id
			]);
			$spmRole->users()->attach($spm05);

			$spm06 = Sentinel::registerAndActivate([
				'email'    		=> 'spm06@gmail.com',
				'username'    	=> 'spm06',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[5]->id
			]);
			$spmRole->users()->attach($spm06);

			$spm07 = Sentinel::registerAndActivate([
				'email'    		=> 'spm07@gmail.com',
				'username'    	=> 'spm07',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[6]->id
			]);
			$spmRole->users()->attach($spm07);

			$spm08 = Sentinel::registerAndActivate([
				'email'    		=> 'spm08@gmail.com',
				'username'    	=> 'spm08',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'dealer_id'		=> $dealers[7]->id
			]);
			$spmRole->users()->attach($spm08);


		//
		// create Cabang Role and Account
		// ==========================================================================
		
			Sentinel::getRoleRepository()->createModel()->create([
				'name' => 'PIC Cabang',
				'slug' => 'cabang',
				'permissions' => [
					'sellin' => true,
					'sellout' => true,
					'stockopname' => true,
					'report' => true,
					'cabang' => true,
					'subcabang' => true,
					'dealer' => true,
					'product' => true,
					'customer' => true,
					'user' => true
				]
			]);
			$cabangRole = Sentinel::findRoleBySlug('cabang');

			$cabang01 = Sentinel::registerAndActivate([
				'email'    		=> 'cabang01@gmail.com',
				'username'    	=> 'cabang01',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'cabang_id'		=> $cabangs[0]->id
			]);
			$cabangRole->users()->attach($cabang01);

			$cabang02 = Sentinel::registerAndActivate([
				'email'    		=> 'cabang02@gmail.com',
				'username'    	=> 'cabang02',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
				'cabang_id'		=> $cabangs[1]->id
			]);
			$cabangRole->users()->attach($cabang02);


		//
		// create Admin Pusat Role and Account
		// ==========================================================================
		
			Sentinel::getRoleRepository()->createModel()->create([
				'name' => 'Admin Pusat',
				'slug' => 'adminpusat',
				'permissions' => [
					'sellin' => true,
					'sellout' => true,
					'stockopname' => true,
					'report' => true,
					'rangking' => true,
					'cabang' => true,
					'subcabang' => true,
					'dealer' => true,
					'product' => true,
					'customer' => true,
					'user' => true,
					'role' => true
				]
			]);
			$pusatRole = Sentinel::findRoleBySlug('adminpusat');

			$pusatUser = Sentinel::registerAndActivate([
				'email'    		=> 'adminpusat@gmail.com',
				'username'    	=> 'adminpusat',
				'password' 		=> '123123',
				'first_name'    => $faker->firstNameMale,
				'last_name'   	=> $faker->lastName,
				'gender'   		=> $faker->randomElement($array = array ('Male','Female')),
				'phone'   		=> $faker->phoneNumber,
			]);
			$pusatRole->users()->attach($pusatUser);


			
	}

}
