<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SellinsProductinsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$cabangs = Cabang::all();
		$products = Product::all();

		$sales_role = Sentinel::findRoleBySlug('sales');
		
		foreach(range(0, 1) as $index)
		{
			$sales_user = $sales_role->users()->where('cabang_id', $cabangs[$index]->id)->first();

			$sellin = Sellin::create([
				'sales_id' => $sales_user->id,
				'cabang_id' => $cabangs[$index]->id,
				'subcabang_id' => $cabangs[$index]->subcabangs[$index]->id,
				'dealer_id' => $cabangs[$index]->subcabangs[$index]->dealers[$index]->id,
				'datetime' => Carbon\Carbon::now()->toDateTimeString()
			]);

			foreach(range(0, 3) as $index_inside)
			{
				$product_id = intval($faker->numberBetween(0, $products->count()-1));
				$price = intval($faker->numberBetween(1000, 10000));
				$price = intval($price * 1000);

				Productin::create([
					'product_id' => $products[$product_id]->id,
					'quantity' => $faker->randomDigitNotNull,
					'price' => $price,
					'sellin_id' => $sellin->id
				]);
			}
		}
	}

}