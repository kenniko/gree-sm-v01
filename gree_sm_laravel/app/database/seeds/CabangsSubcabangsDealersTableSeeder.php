<?php

class CabangsSubcabangsDealersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker\Factory::create();

		// DB::table('dealers')->truncate();
		// DB::table('subcabangs')->truncate();
		// DB::table('cabangs')->truncate();

		foreach(range(0, 1) as $index)
		{
			$cabang = Cabang::create([
				'name' => $faker->state()
			]);

			foreach(range(0, 1) as $subindex)
			{
				$subcabang = Subcabang::create([
					'cabang_id' => $cabang->id,
					'name' => $faker->city()
				]);

				foreach(range(0, 1) as $dealerindex)
				{
					Dealer::create([
						'id' => $faker->randomNumber($nbDigits = 6),
						'subcabang_id' => $subcabang->id,
						'name' => $faker->company()
					]);
				}
			}
				
		}
	}

}