<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CustomersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('customers')->truncate();

		foreach(range(1, 10) as $index)
		{
			Customer::create([
				'name' => $faker->name,
				'phone' => $faker->phoneNumber
			]);
		}
	}

}