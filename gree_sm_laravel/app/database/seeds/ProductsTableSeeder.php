<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('products')->truncate();

		foreach(range(1, 10) as $index)
		{
			Product::create([
				'name' => $faker->numerify('Product-###'),
				'code' => $faker->swiftBicNumber,
			]);
		}
	}

}