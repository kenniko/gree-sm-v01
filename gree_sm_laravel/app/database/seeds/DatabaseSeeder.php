<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CabangsSubcabangsDealersTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ProductsTableSeeder');
		$this->call('CustomersTableSeeder');
		$this->call('SellinsProductinsTableSeeder');
		$this->call('SelloutsProductoutsTableSeeder');
		$this->call('RangkingsTableSeeder');
	}

}
