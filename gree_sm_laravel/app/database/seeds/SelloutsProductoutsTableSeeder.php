<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SelloutsProductoutsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$cabangs = Cabang::all();
		$products = Product::all();
		$customers = Customer::all();

		$sales_role = Sentinel::findRoleBySlug('sales');
		$spm_role = Sentinel::findRoleBySlug('spm');

		foreach(range(0, 1) as $index)
		{
			$sales_user = $sales_role->users()->where('cabang_id', $cabangs[$index]->id)->first();
			$spm_user = $spm_role->users()->where('dealer_id', $cabangs[$index]->subcabangs[$index]->dealers[$index]->id)->first();

			$sellout = Sellout::create([
				'sales_id' => $sales_user->id,
				'cabang_id' => $cabangs[$index]->id,
				'subcabang_id' => $cabangs[$index]->subcabangs[$index]->id,
				'dealer_id' => $cabangs[$index]->subcabangs[$index]->dealers[$index]->id,
				'spm_id' => $spm_user->id,
				'type' => $faker->randomElement($array = array ('Retail','Project')),
				'customer_id' => $customers[$index]->id,
				'datetime' => Carbon\Carbon::now()->toDateTimeString()
			]);

			foreach(range(0, 3) as $index_inside)
			{
				$product_id = intval($faker->numberBetween(0, $products->count()-1));
				$price = intval($faker->numberBetween(1000, 10000));
				$price = intval($price * 1000);

				Productout::create([
					'product_id' => $products[$product_id]->id,
					'quantity' => $faker->randomDigitNotNull,
					'price' => $price,
					'sellout_id' => $sellout->id
				]);
			}
		}
	}

}