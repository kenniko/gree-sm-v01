<?php

class Dealer extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'id' => 'required|numeric|unique:dealers',
		'name' => 'required|unique:dealers',
		'subcabang_id' => 'required|integer',
	];

	// Don't forget to fill this array
	protected $fillable = ['id', 'name', 'subcabang_id'];

	public function subcabang()
    {
        return $this->belongsTo('Subcabang');
    }

    public function spm()
    {
        return $this->hasMany('User', 'dealer_id');
    }

}