<?php

class Sellin extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'sales_id' => 'required|integer',
		'cabang_id' => 'required|integer',
		'subcabang_id' => 'required|integer',
        'dealer_id' => 'required|integer',
		'datetime' => 'required|date'
	];

	// Don't forget to fill this array
	protected $fillable = ['sales_id', 'cabang_id', 'subcabang_id', 'dealer_id', 'datetime'];

	public function productins()
    {
    	return $this->hasMany('Productin');

    	// return $this->hasOne('Phone');
    	// return $this->hasManyThrough('Post', 'User');
     //    return $this->belongsTo('Phone');
     //    return $this->belongsToMany('Role');
     //    return $this->morphMany('Photo', 'imageable');
     //    return $this->morphToMany('Tag', 'taggable');
     //    return $this->morphedByMany('Post', 'taggable');
    }

    public function sales()
    {
    	return $this->belongsTo('User', 'sales_id');
    }

    public function cabang()
    {
    	return $this->belongsTo('Cabang');
    }

    public function subcabang()
    {
    	return $this->belongsTo('Subcabang');
    }

    public function dealer()
    {
    	return $this->belongsTo('Dealer');
    }



    public function getTotalQtyAttribute()
    {
        return $this->productins()->sum('quantity');
    }

    // public function getTotalAmountAttribute()
    // {
    //     $TotalPrice = $this->productins()->sum('price');
    //     return 'Rp ' . number_format($TotalPrice, 0, '', '.');
    // }

}