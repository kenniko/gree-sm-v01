<?php

class Sellout extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'sales_id' => 'required|integer',
		'cabang_id' => 'required|integer',
		'subcabang_id' => 'required|integer',
        'dealer_id' => 'required|integer',
        'spm_id' => 'integer',
        'type' => 'required',
        'customer_id' => 'required|integer',
		'datetime' => 'required|date'
	];

	// Don't forget to fill this array
	protected $fillable = ['sales_id', 'cabang_id', 'subcabang_id', 'dealer_id', 'spm_id', 'type', 'customer_id', 'datetime'];

	public function productouts()
    {
    	return $this->hasMany('Productout');

    	// return $this->hasOne('Phone');
    	// return $this->hasManyThrough('Post', 'User');
     //    return $this->belongsTo('Phone');
     //    return $this->belongsToMany('Role');
     //    return $this->morphMany('Photo', 'imageable');
     //    return $this->morphToMany('Tag', 'taggable');
     //    return $this->morphedByMany('Post', 'taggable');
    }

    public function sales()
    {
    	return $this->belongsTo('User', 'sales_id');
    }

    public function cabang()
    {
    	return $this->belongsTo('Cabang');
    }

    public function subcabang()
    {
    	return $this->belongsTo('Subcabang');
    }

    public function dealer()
    {
    	return $this->belongsTo('Dealer');
    }

    public function spm()
    {
    	return $this->belongsTo('User', 'spm_id');
    }

    public function customer()
    {
    	return $this->belongsTo('Customer');
    }



    public function getTotalQtyAttribute()
    {
        return $this->productouts()->sum('quantity');
    }

    // public function getTotalAmountAttribute()
    // {
    //     $TotalPrice = $this->productouts()->sum('price');
    //     return 'Rp ' . number_format($TotalPrice, 0, '', '.');
    // }

}