<?php

class Productout extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'product_id' => 'required|integer',
		'quantity' => 'required|integer|min:1',
		'price' => 'required|integer|min:1',
		'sellout_id' => 'required|integer'
	];

	// Don't forget to fill this array
	protected $fillable = ['product_id', 'quantity', 'price', 'sellout_id'];

	public function sellout()
    {
    	return $this->belongsTo('Sellout');

    	// return $this->hasOne('Phone');
    	// return $this->hasMany('Comment');
    	// return $this->hasManyThrough('Post', 'User');
     //    return $this->belongsTo('Phone');
     //    return $this->belongsToMany('Role');
     //    return $this->morphMany('Photo', 'imageable');
     //    return $this->morphToMany('Tag', 'taggable');
     //    return $this->morphedByMany('Post', 'taggable');
    }

    public function product()
    {
    	return $this->belongsTo('Product');
    }

}