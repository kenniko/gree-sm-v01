<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class User extends SentinelUser implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $fillable = [
        'email',
        'username',
        'password',
        'last_name',
        'first_name',
        'gender',
        'phone',
        'permissions',
        'cabang_id',
        'dealer_id'
    ];

    public static $rules = [
        // 'login'    => 'required|min:3',
        'password' => 'required|min:3|confirmed',
        'password_confirmation' => 'required|min:3',
        'email' => 'required|email|unique:users',
        'first_name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'username' => 'required|min:3',
        'gender' => 'required',
        'role'=> 'required',
        'phone' => 'numeric'
    ];

    public $errors;

    protected $loginNames = ['email', 'username'];

    public function isValid() {
        $validation = Validator::make($this->attributes, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();
        return false;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getRoleAttribute()
    {
        // if ($user = Sentinel::getUser()) {
        //     return $user->roles()->first()->name;
        // }
        return $this->roles()->first()->name;
    }

    public function cabang()
    {
        return $this->belongsTo('Cabang', 'cabang_id');
    }

    public function dealer()
    {
        return $this->belongsTo('Dealer', 'dealer_id');
    }

}
