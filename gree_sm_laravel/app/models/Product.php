<?php

class Product extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
        'name' => 'required|unique:products',
		'code' => 'required|unique:products'
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'code'];

	// public function xxx()
 //    {
 //    	return $this->hasOne('Phone');
 //    	return $this->hasMany('Comment');
 //    	return $this->hasManyThrough('Post', 'User');
 //        return $this->belongsTo('Phone');
 //        return $this->belongsToMany('Role');
 //        return $this->morphMany('Photo', 'imageable');
 //        return $this->morphToMany('Tag', 'taggable');
 //        return $this->morphedByMany('Post', 'taggable');
 //    }

	public function getNameAndCodeAttribute()
    {
        return $this->name . " / " . $this->code;
    }

    public function productins()
    {
        return $this->hasMany('Productin');
    }

    public function productouts()
    {
        return $this->hasMany('Productout');
    }

}