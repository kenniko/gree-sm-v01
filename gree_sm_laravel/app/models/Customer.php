<?php

class Customer extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required|unique:customers',
		// 'phone' => 'unique:customers',
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'phone'];

	// public function xxx()
 //    {
 //    	return $this->hasOne('Phone');
 //    	return $this->hasMany('Comment');
 //    	return $this->hasManyThrough('Post', 'User');
 //        return $this->belongsTo('Phone');
 //        return $this->belongsToMany('Role');
 //        return $this->morphMany('Photo', 'imageable');
 //        return $this->morphToMany('Tag', 'taggable');
 //        return $this->morphedByMany('Post', 'taggable');
 //    }

}