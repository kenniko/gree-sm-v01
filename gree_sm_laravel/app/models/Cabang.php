<?php

class Cabang extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
        'name' => 'required|unique:cabangs'
	];

	// Don't forget to fill this array
	protected $fillable = ['name'];

	public function subcabangs()
    {
        return $this->hasMany('Subcabang');
    }

    public function dealers()
    {
        return $this->hasManyThrough('Dealer', 'Subcabang');
    }

    public function sales()
    {
        return $this->hasMany('User', 'cabang_id');
    }

    public function sellins()
    {
        return $this->hasMany('Sellin');
    }

    public function productins()
    {
        return $this->hasManyThrough('Productin', 'Sellin');
    }

    public function sellouts()
    {
        return $this->hasMany('Sellout');
    }

    public function productouts()
    {
        return $this->hasManyThrough('Productout', 'Sellout');
    }

    public function totalAmountSellinDay()
    {
        return $this->productins()->sum('quantity');
    }

}