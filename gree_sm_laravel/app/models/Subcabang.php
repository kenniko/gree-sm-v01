<?php

class Subcabang extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required|unique:subcabangs',
		'cabang_id' => 'required|integer',
	];

	// Don't forget to fill this array
	protected $fillable = ['name', 'cabang_id'];

	public function dealers()
    {
        return $this->hasMany('Dealer');
    }

    public function cabang()
    {
        return $this->belongsTo('Cabang');
    }
}