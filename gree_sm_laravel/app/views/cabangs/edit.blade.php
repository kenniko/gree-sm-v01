@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/select.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Cabang <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('cabang')}}">Cabang</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<!-- form start -->
        		{{ Form::model($cabang, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['cabang.update', $cabang->id]]) }}
        			<div class="box-body">
        				<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
		        					{{ Form::label('name', 'Name *') }}
		        					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Cabang name')) }}
		        					<p class="help-block">{{ $errors->first('name') }}</p>
		        				</div>	
		        			</div>
		        		</div>
                        <hr>

                        <!-- Sub Cabang -->
                        <div class="sub-box">
                            <div class="sub-box-header">
                                <a href="{{ URL::to('subcabang/create?org=cabang&cabid='.$cabang->id)}}" class="btn btn-default btn-sm"><i class="fa fa-file"></i>&nbsp;&nbsp;New Sub Cabang</a>&nbsp;
                                <button id="BtnAddSubcabang" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Sub Cabang</button>&nbsp;
                                <button type="button" id="BtnRemoveSelectedSubCabang" class="btn btn-default btn-sm" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove Selected</button>
                            </div>
                            @if ($cabang->subcabangs->count() > 0)
                                <table id="TableSubCabang" class="table table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <input type="checkbox" name="select_all" value="1" id="example-select-all-subcabang">
                                            </th>
                                            <th>ID</th>
                                            <th>Sub Cabang Name</th>
                                            <th>Dealer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cabang->subcabangs as $key => $value)
                                            <tr href="{{ URL::to('subcabang/' . $value->id.'?org=cabang&cabid='.$cabang->id)}}">
                                                <td disabled></td>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>{{ $value->dealers->count() }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                        <hr>

                        <!-- Sales -->
                        <div class="sub-box">
                            <div class="sub-box-header">
                                <a href="{{ URL::to('user/create?org=cabang&cabid='.$cabang->id)}}" class="btn btn-default btn-sm"><i class="fa fa-file"></i>&nbsp;&nbsp;New Sales</a>&nbsp;
                                <button id="BtnAddSales" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Sales</button>&nbsp;
                                <button type="button" id="BtnRemoveSelectedSales" class="btn btn-default btn-sm" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove Selected</button>
                            </div>
                            @if ($cabang->sales->count() > 0)
                                <table id="TableSales" class="table table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <input type="checkbox" name="select_all" value="1" id="example-select-all-sales">
                                            </th>
                                            <th>ID</th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sales as $key => $value)
                                            <tr href="{{ URL::to('user/' . $value->id.'?org=cabang&cabid='.$cabang->id)}}">
                                                <td disabled></td>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->full_name }}</td>
                                                <td>{{ $value->username }}</td>
                                                <td>{{ $value->email }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>

        			</div>
        			<!-- /.box-body -->

        			<div class="box-footer">
                        {{ Form::hidden('cabang_id', $cabang->id ) }}
        				{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
        				<a href="{{ URL::to('cabang')}}" class="btn btn-default">Cancel</a>
        				<button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
        			</div>
        		{{ Form::close() }}

                {{ Form::model($cabang, ['id'=>'FormDelete', 'method' => 'DELETE', 'route' => ['cabang.destroy', $cabang->id]]) }}
                	{{ Form::text('name', $cabang->name, array('class' => 'hidden')) }}
                {{ Form::close() }}
        	</div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Modal Add Sub Cabang -->
    <div class="modal fade" id="ModalAddSubCabang" tabindex="-1" role="dialog" aria-labelledby="ModalAddSubCabangLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalAddSubCabangLabel">All Sub Cabang</h4>
                </div>
                {{Form::open(['url' => 'subcabangs/update', 'method' => 'PATCH', 'id'=>'FormUpdateSelected'])}}
                <div class="modal-body">
                    <table id="TableAllSubCabang" class="table table-bordered table-hover table-select" width="100%">
                        <thead>
                            <tr>
                                <th width="5">
                                    <input type="checkbox" name="select_all_modal" value="1" id="example-select-all">
                                </th>
                                <th>ID</th>
                                <th>Sub Cabang Name</th>
                                <th>Cabang</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    {{ Form::hidden('cabang_id', $cabang->id ) }}
                    <button id="BtnSubmitModalSubCabang" type="submit" class="btn btn-primary" disabled>Add selected</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <!-- Modal Add Sales -->
    <div class="modal fade" id="ModalAddSales" tabindex="-1" role="dialog" aria-labelledby="ModalAddSalesLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalAddSalesLabel">All Sales</h4>
                </div>
                {{Form::open(['url' => 'users/update', 'method' => 'PATCH', 'id'=>'FormUpdateSelected'])}}
                <div class="modal-body">
                    <table id="TableAllSales" class="table table-bordered table-hover table-select" width="100%">
                        <thead>
                            <tr>
                                <th width="5">
                                    <input type="checkbox" name="select_all_modal" value="1" id="example-select-all">
                                </th>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Cabang</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    {{ Form::hidden('cabang_id', $cabang->id ) }}
                    <button id="BtnSubmitModalSales" type="submit" class="btn btn-primary" disabled>Add selected</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.select.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });


    //
    // Table Main
    // ==========================================================================
    
    var $TableSubCabang = $('#TableSubCabang').DataTable({
        // 'ajax': "{{ route('subcabang.index') }}",
        'columnDefs': [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    // console.log(full);
                    return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    // Handle click on "Select all" control
    $('#example-select-all-subcabang').on('click', function(){
        // Get all rows with search applied
        var rows = $TableSubCabang.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('#TableSubCabang').find('input[type="checkbox"]', rows).prop('checked', this.checked);
        checkCheckedCheckboxsSubCabang();
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#TableSubCabang tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#example-select-all-subcabang').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control as 'indeterminate'
                el.indeterminate = true;
            }
        }
        checkCheckedCheckboxsSubCabang();
    });

    // If there any checked checkboxs
    var checkCheckedCheckboxsSubCabang = function(){
        if ($('#TableSubCabang tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnRemoveSelectedSubCabang').attr('disabled','disabled');
        } else {
            $('#BtnRemoveSelectedSubCabang').removeAttr('disabled');
        }
    }

    // on Click Selected Sub Cabang
    $('#BtnRemoveSelectedSubCabang').click(function(ev){
        alertify.confirm("Are you sure you want to remove selected items?", function () {
            $('#FormMain').attr("action","{{ URL::to('subcabangs/remove') }}");
            // $('#FormMain').attr("method","post");
            $('#FormMain').submit();
        });
    });



    //
    // Table Modal
    // ==========================================================================
    
    var $TableModalSubCabang = {};

    // click Add Sub Cabang
    $('#BtnAddSubcabang').click(function(ev){
        ev.preventDefault();

        // load Data Sub Cabang
        $TableModalSubCabang = $('#TableAllSubCabang').DataTable({
            columnDefs: [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full['id']+'">';
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            order: [[1, 'asc']],
            processing: true,
            serverSide: true,
            ajax: "{{ URL::to('ajax/subcabangs?origin=editcabang&id='.$cabang->id) }}",
            columns: [
                { data: '', name: '' },
                { data: 'id', name: 'subcabangs.id' },
                { data: 'name', name: 'subcabangs.name' },
                { data: 'cabang', name: 'cabangs.name' },
            ]
        });

    });

    // show Modal after finish
    $('#TableAllSubCabang').on( 'draw.dt', function () {
        $('#ModalAddSubCabang').modal('show');

        // Handle click on heading containing "Select all" control
        $('thead', $TableModalSubCabang.table().container()).on('click', 'th:first-child', function(e) {
            $('input[type="checkbox"]', this).trigger('click');
        });
    });

    // on Modal hidden
    $('#ModalAddSubCabang').on('hidden.bs.modal', function (e) {
        $TableModalSubCabang.destroy();
        $TableModalSubCabang = {};
    });

    // Handle row selection event
    $('#TableAllSubCabang').on('select.dt deselect.dt', function(e, api, type, items) {
        if (e.type === 'select') {
        $('tr.selected input[type="checkbox"]', api.table().container()).prop('checked', true);
        } else {
        $('tr:not(.selected) input[type="checkbox"]', api.table().container()).prop('checked', false);
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSubCabang);  
        checkCheckedCheckboxsSubCabangModal();    
    });

    // Handle click on "Select all" control
    $('#TableAllSubCabang thead').on('click', 'input[type="checkbox"]', function(e) {
        if (this.checked) {
        $TableModalSubCabang.rows({ page: 'current' }).select();        
        } else {
        $TableModalSubCabang.rows({ page: 'current' }).deselect();
        }

        e.stopPropagation();
    });

    // Handle table draw event
    $('#TableAllSubCabang').on('draw.dt', function() {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSubCabang);
        checkCheckedCheckboxsSubCabangModal();
    });

    // If there any checked checkboxs inside modal
    var checkCheckedCheckboxsSubCabangModal = function(){
        if ($('#TableAllSubCabang tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnSubmitModalSubCabang').attr('disabled','disabled');
        } else {
            $('#BtnSubmitModalSubCabang').removeAttr('disabled');
        }
    }





    // ==========================================================================
    // Sales
    // ==========================================================================
    
    //
    // Table Main
    // ==========================================================================
    
    var $TableSales = $('#TableSales').DataTable({
        // 'ajax': "{{ route('subcabang.index') }}",
        'columnDefs': [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    // console.log(full);
                    return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    // Handle click on "Select all" control
    $('#example-select-all-sales').on('click', function(){
        // Get all rows with search applied
        var rows = $TableSales.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('#TableSales').find('input[type="checkbox"]', rows).prop('checked', this.checked);
        checkCheckedCheckboxsSales();
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#TableSales tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#example-select-all-sales').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control as 'indeterminate'
                el.indeterminate = true;
            }
        }
        checkCheckedCheckboxsSales();
    });

    // If there any checked checkboxs
    var checkCheckedCheckboxsSales = function(){
        if ($('#TableSales tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnRemoveSelectedSales').attr('disabled','disabled');
        } else {
            $('#BtnRemoveSelectedSales').removeAttr('disabled');
        }
    }

    // on Click Selected Sub Cabang
    $('#BtnRemoveSelectedSales').click(function(ev){
        alertify.confirm("Are you sure you want to remove selected items?", function () {
            $('#FormMain').attr("action","{{ URL::to('users/remove') }}");
            // $('#FormMain').attr("method","post");
            $('#FormMain').submit();
        });
    });



    // //
    // // Table Modal
    // // ==========================================================================
    
    var $TableModalSales = {};

    // click Add Sales
    $('#BtnAddSales').click(function(ev){
        ev.preventDefault();

        // load Data Sales
        $TableModalSales = $('#TableAllSales').DataTable({
            columnDefs: [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full['id']+'">';
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            order: [[1, 'asc']],
            processing: true,
            serverSide: true,
            ajax: "{{ URL::to('ajax/sales?origin=editcabang&id='.$cabang->id) }}",
            columns: [
                { data: '', name: '' },
                { data: 'id', name: 'users.id' },
                { data: 'first_name', name: 'users.first_name' },
                { data: 'last_name', name: 'users.last_name' },
                { data: 'username', name: 'users.username' },
                { data: 'cabang', name: 'cabangs.name' }
            ]
        });

    });

    // show Modal after finish
    $('#TableAllSales').on( 'draw.dt', function () {
        $('#ModalAddSales').modal('show');

        // Handle click on heading containing "Select all" control
        $('thead', $TableModalSales.table().container()).on('click', 'th:first-child', function(e) {
            $('input[type="checkbox"]', this).trigger('click');
        });
    });

    // on Modal hidden
    $('#ModalAddSales').on('hidden.bs.modal', function (e) {
        $TableModalSales.destroy();
        $TableModalSales = {};
    });

    // Handle row selection event
    $('#TableAllSales').on('select.dt deselect.dt', function(e, api, type, items) {
        if (e.type === 'select') {
        $('tr.selected input[type="checkbox"]', api.table().container()).prop('checked', true);
        } else {
        $('tr:not(.selected) input[type="checkbox"]', api.table().container()).prop('checked', false);
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSales);  
        checkCheckedCheckboxsSalesModal();    
    });

    // Handle click on "Select all" control
    $('#TableAllSales thead').on('click', 'input[type="checkbox"]', function(e) {
        if (this.checked) {
        $TableModalSales.rows({ page: 'current' }).select();        
        } else {
        $TableModalSales.rows({ page: 'current' }).deselect();
        }

        e.stopPropagation();
    });

    // Handle table draw event
    $('#TableAllSales').on('draw.dt', function() {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSales);
        checkCheckedCheckboxsSalesModal();
    });

    // If there any checked checkboxs inside modal
    var checkCheckedCheckboxsSalesModal = function(){
        if ($('#TableAllSales tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnSubmitModalSales').attr('disabled','disabled');
        } else {
            $('#BtnSubmitModalSales').removeAttr('disabled');
        }
    }
    




});


//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {
   var $table = table.table().container();
   var $chkbox_all = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all = $('thead input[type="checkbox"]', $table).get(0);

   // If none of the checkboxes are checked
   if ($chkbox_checked.length === 0) {
      chkbox_select_all.checked = false;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length) {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = true;
      }
   }
}
</script>
@stop