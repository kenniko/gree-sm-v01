@extends('master')

@section('css')
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ URL::to('css/daterangepicker-bs3.css') }}">
@stop

@section('header')
    <section class="content-header title--print">
        <h4>PT GREE - Report&nbsp;&nbsp;<small>(<span class="DateStart"></span> - <span class="DateEnd"></span>)</small></h4>
    </section>
    <section class="content-header">
        <h1>Report <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Report</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">

                    <!-- Print Button -->
                    <button class="btn btn-warning pull-right btn--print"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>

                    <!-- Export Button -->
                    <div class="dropdown pull-right" style="margin-right: 10px;">
                      <button id="dLabel" class="btn btn-success" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download"></i>&nbsp;&nbsp;Export&nbsp;&nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="{{ URL::to('export/report/xls')}}">Excel</a></li>
                        <li><a href="{{ URL::to('export/report/csv')}}">CSV</a></li>
                      </ul>
                    </div>

                    <!-- <a href="{{ URL::to('export')}}" id="ExportLink" class="btn btn-success pull-right"><i class="fa fa-download"></i>&nbsp;&nbsp;Export Reports</a> -->

                    <!-- Filter Date Range -->
                    <div class="filter-on-head pull-right" style="margin-right: 10px;">
                        <label for="FilterDateRange">Sell In &amp; Sell Out Date Range: </label>
                        <div id="FilterDateRange" class="btn btn-default pull-right">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;&nbsp;
                            <span class="DateStart"></span> - <span class="DateEnd"></span>&nbsp;&nbsp;<b class="caret"></b>
                        </div>
                    </div>
                    
                    <!-- <button type="button" id="BtnDeleteSelected" class="btn btn-default" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Selected</button> -->
                </div>
                <div class="box-body">
                    <table id="table" class="table table-bordered table-hover thead-center-center" width="100%">
                        <thead class="thead-center-center">
                            <tr>
                                <th rowspan="3">Cabang</th>
                                <th colspan="4" class="success">Hari</th>
                                <th colspan="4" class="danger">Bulan</th>
                                <th colspan="4" class="info">Tahun</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="success">Sale In</th>
                                <th colspan="2" class="success">Sale Out</th>
                                <th colspan="2" class="danger">Sale In</th>
                                <th colspan="2" class="danger">Sale Out</th>
                                <th colspan="2" class="info">Sale In</th>
                                <th colspan="2" class="info">Sale Out</th>
                            </tr>
                            <tr>
                                <th class="success">Qty</th>
                                <th class="success">Amount</th>
                                <th class="success">Qty</th>
                                <th class="success">Amount</th>
                                <th class="danger">Qty</th>
                                <th class="danger">Amount</th>
                                <th class="danger">Qty</th>
                                <th class="danger">Amount</th>
                                <th class="info">Qty</th>
                                <th class="info">Amount</th>
                                <th class="info">Qty</th>
                                <th class="info">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($cabangs as $key => $value)
	                            <tr class="tr-v-bottom">
                                    <td disabled>{{ $value->name }}</td>
                                    <td disabled class="text-center total_quantity_sellin_hari">{{ $value->total_quantity_sellin_hari }}</td>
                                    <td disabled class="text-right total_price_sellin_hari">{{ $value->total_price_sellin_hari }}</td>
                                    <td disabled class="text-center total_quantity_sellout_hari">{{ $value->total_quantity_sellout_hari }}</td>
                                    <td disabled class="text-right total_price_sellout_hari">{{ $value->total_price_sellout_hari }}</td>
                                    <td disabled class="text-center total_quantity_sellin_bulan">{{ $value->total_quantity_sellin_bulan }}</td>
                                    <td disabled class="text-right total_price_sellin_bulan">{{ $value->total_price_sellin_bulan }}</td>
                                    <td disabled class="text-center total_quantity_sellout_bulan">{{ $value->total_quantity_sellout_bulan }}</td>
                                    <td disabled class="text-right total_price_sellout_bulan">{{ $value->total_price_sellout_bulan }}</td>
                                    <td disabled class="text-center total_quantity_sellin_tahun">{{ $value->total_quantity_sellin_tahun }}</td>
                                    <td disabled class="text-right total_price_sellin_tahun">{{ $value->total_price_sellin_tahun }}</td>
                                    <td disabled class="text-center total_quantity_sellout_tahun">{{ $value->total_quantity_sellout_tahun }}</td>
                                    <td disabled class="text-right total_price_sellout_tahun">{{ $value->total_price_sellout_tahun }}</td>
	                            </tr>
	                        @endforeach
                            <tr class="tr-v-bottom">
                                <td disabled class="text-bold">TOTAL</td>
                                <td disabled class="grand_total_quantity_sellin_hari text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellin_hari text-right text-bold"></td>
                                <td disabled class="grand_total_quantity_sellout_hari text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellout_hari text-right text-bold"></td>
                                <td disabled class="grand_total_quantity_sellin_bulan text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellin_bulan text-right text-bold"></td>
                                <td disabled class="grand_total_quantity_sellout_bulan text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellout_bulan text-right text-bold"></td>
                                <td disabled class="grand_total_quantity_sellin_tahun text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellin_tahun text-right text-bold"></td>
                                <td disabled class="grand_total_quantity_sellout_tahun text-center text-bold"></td>
                                <td disabled class="addCurrency grand_total_price_sellout_tahun text-right text-bold"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('second_content')
    <section class="content-header">
        <h1>Ranking <small>Cabang</small></h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="table" class="table table-bordered table-hover thead-center-center" width="100%">
                            <thead class="thead-center-center">
                                <tr>
                                    <th rowspan="2">Cabang</th>
                                    <th colspan="2" class="danger">Bulan</th>
                                    <th colspan="2" class="info">Tahun</th>
                                </tr>
                                <tr>
                                    <th class="danger">Point</th>
                                    <th class="danger">Ranking</th>
                                    <th class="info">Point</th>
                                    <th class="info">Ranking</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rangkings as $key => $value)
                                    <tr class="tr-v-bottom">
                                        <td disabled>{{ $value->name }}</td>
                                        <td disabled class="text-right total_quantity_sellin_hari">{{ $value->point_bulan }}</td>
                                        <td disabled class="text-right total_price_sellin_hari">{{ $value->rangking_bulan }}</td>
                                        <td disabled class="text-right total_quantity_sellout_hari">{{ $value->point_tahun }}</td>
                                        <td disabled class="text-right total_price_sellout_hari">{{ $value->rangking_tahun }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@stop

@section('js')
    <!-- date-range-picker -->
    <script src="{{ URL::to('js/daterangepicker.js') }}"></script>
@stop

@section('scripts')
    <script>
    function formatNumber(num)
    {
        return ("" + num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, function($1) { return $1 + "." });
    }

    $(function () {

        var grand_total_quantity_sellin_hari = 0,
            grand_total_quantity_sellin_bulan = 0,
            grand_total_quantity_sellin_tahun = 0,
            grand_total_quantity_sellout_hari = 0,
            grand_total_quantity_sellout_bulan = 0,
            grand_total_quantity_sellout_tahun = 0,
            grand_total_price_sellin_hari = 0,
            grand_total_price_sellin_bulan = 0,
            grand_total_price_sellin_tahun = 0,
            grand_total_price_sellout_hari = 0,
            grand_total_price_sellout_bulan = 0,
            grand_total_price_sellout_tahun = 0;

        $('.total_quantity_sellin_hari').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellin_hari = grand_total_quantity_sellin_hari + parseInt(qty);
            }
        });
        $('.total_quantity_sellin_bulan').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellin_bulan = grand_total_quantity_sellin_bulan + parseInt(qty);
            }
        });
        $('.total_quantity_sellin_tahun').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellin_tahun = grand_total_quantity_sellin_tahun + parseInt(qty);
            }
        });
        $('.total_quantity_sellout_hari').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellout_hari = grand_total_quantity_sellout_hari + parseInt(qty);
            }
        });
        $('.total_quantity_sellout_bulan').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellout_bulan = grand_total_quantity_sellout_bulan + parseInt(qty);
            }
        });
        $('.total_quantity_sellout_tahun').each(function(ev){
            var qty = $(this).text();
            if (!isNaN(qty)) {
                grand_total_quantity_sellout_tahun = grand_total_quantity_sellout_tahun + parseInt(qty);
            }
        });
        $('.total_price_sellin_hari').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellin_hari = grand_total_price_sellin_hari + parseInt(price);
            }
        });
        $('.total_price_sellin_bulan').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellin_bulan = grand_total_price_sellin_bulan + parseInt(price);
            }
        });
        $('.total_price_sellin_tahun').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellin_tahun = grand_total_price_sellin_tahun + parseInt(price);
            }
        });
        $('.total_price_sellout_hari').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellout_hari = grand_total_price_sellout_hari + parseInt(price);
            }
        });
        $('.total_price_sellout_bulan').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellout_bulan = grand_total_price_sellout_bulan + parseInt(price);
            }
        });
        $('.total_price_sellout_tahun').each(function(ev){
            var price = $(this).text().replace('Rp ','');
            price = price.replace('.', '');
            price = price.replace('.', '');
            // console.log(parseInt(price));
            if (!isNaN(price)) {
                grand_total_price_sellout_tahun = grand_total_price_sellout_tahun + parseInt(price);
            }
        });

        if(grand_total_quantity_sellin_hari == 0 || isNaN(grand_total_quantity_sellin_hari)) {
            grand_total_quantity_sellin_hari = '-';
        }
        if(grand_total_quantity_sellin_bulan == 0 || isNaN(grand_total_quantity_sellin_bulan)) {
            grand_total_quantity_sellin_bulan = '-';
        }
        if(grand_total_quantity_sellin_tahun == 0 || isNaN(grand_total_quantity_sellin_tahun)) {
            grand_total_quantity_sellin_tahun = '-';
        }
        if(grand_total_quantity_sellout_hari == 0 || isNaN(grand_total_quantity_sellout_hari)) {
            grand_total_quantity_sellout_hari = '-';
        }
        if(grand_total_quantity_sellout_bulan == 0 || isNaN(grand_total_quantity_sellout_bulan)) {
            grand_total_quantity_sellout_bulan = '-';
        }
        if(grand_total_quantity_sellout_tahun == 0 || isNaN(grand_total_quantity_sellout_tahun)) {
            grand_total_quantity_sellout_tahun = '-';
        }
        if(grand_total_price_sellin_hari == 0 || isNaN(grand_total_price_sellin_hari)) {
            grand_total_price_sellin_hari = '-';
        }
        if(grand_total_price_sellin_bulan == 0 || isNaN(grand_total_price_sellin_bulan)) {
            grand_total_price_sellin_bulan = '-';
        }
        if(grand_total_price_sellin_tahun == 0 || isNaN(grand_total_price_sellin_tahun)) {
            grand_total_price_sellin_tahun = '-';
        }
        if(grand_total_price_sellout_hari == 0 || isNaN(grand_total_price_sellout_hari)) {
            grand_total_price_sellout_hari = '-';
        }
        if(grand_total_price_sellout_bulan == 0 || isNaN(grand_total_price_sellout_bulan)) {
            grand_total_price_sellout_bulan = '-';
        }
        if(grand_total_price_sellout_tahun == 0 || isNaN(grand_total_price_sellout_tahun)) {
            grand_total_price_sellout_tahun = '-';
        }


        $('.grand_total_quantity_sellin_hari').html(grand_total_quantity_sellin_hari);
        $('.grand_total_quantity_sellin_bulan').html(grand_total_quantity_sellin_bulan);
        $('.grand_total_quantity_sellin_tahun').html(grand_total_quantity_sellin_tahun);
        $('.grand_total_quantity_sellout_hari').html(grand_total_quantity_sellout_hari);
        $('.grand_total_quantity_sellout_bulan').html(grand_total_quantity_sellout_bulan);
        $('.grand_total_quantity_sellout_tahun').html(grand_total_quantity_sellout_tahun);
        $('.grand_total_price_sellin_hari').html(grand_total_price_sellin_hari);
        $('.grand_total_price_sellin_bulan').html(grand_total_price_sellin_bulan);
        $('.grand_total_price_sellin_tahun').html(grand_total_price_sellin_tahun);
        $('.grand_total_price_sellout_hari').html(grand_total_price_sellout_hari);
        $('.grand_total_price_sellout_bulan').html(grand_total_price_sellout_bulan);
        $('.grand_total_price_sellout_tahun').html(grand_total_price_sellout_tahun);
        $('.addCurrency').each(function(ev){
            var price = $(this).text();
            if (price > 0) {
                $(this).html('Rp '+formatNumber(price));
            }
        });

        var $ExportLink = $('#ExportLink');

        // daterangepicker
        function cb(start, end) {
            if (isNaN(start) || isNaN(end)) {
                $('.DateStart').html('All');
                $('.DateEnd').html('Time');
                $ExportLink.attr('href', "{{ URL::to('export')}}");
            } else {
                $('.DateStart').html(start.format('MMMM D, YYYY'));
                $('.DateEnd').html(end.format('MMMM D, YYYY'));
                $ExportLink.attr('href', "{{ URL::to('export?start=')}}"+start.format('YYYY-MM-DD')+"&end="+end.format('YYYY-MM-DD'));
            }
        }
        cb(moment().startOf('month'), moment().endOf('month'));
        $('#FilterDateRange').daterangepicker({
            ranges: {
               'All Time': [null, null],
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        $('#FilterDateRange').data('daterangepicker').setStartDate(moment().startOf('month'));
        $('#FilterDateRange').data('daterangepicker').setEndDate(moment().endOf('month'));

      });
    </script>
@stop