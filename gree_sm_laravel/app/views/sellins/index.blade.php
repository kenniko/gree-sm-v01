@extends('master')

@section('css')
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ URL::to('css/daterangepicker-bs3.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header title--print">
        <h4>PT GREE - Sell In&nbsp;&nbsp;<small>(<span class="DateStart"></span> - <span class="DateEnd"></span>)</small></h4>
    </section>
    <section class="content-header">
        <h1>Sell In <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Sell In</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                {{Form::open(array('url' => 'sellins/delete', 'method' => 'DELETE', 'id'=>'FormDeleteSelected'))}}
                <div class="box-header with-border">

                    <!-- Button Controll -->
                    <a href="{{ URL::to('sellin/create')}}" class="btn btn-primary"><i class="fa fa-file"></i>&nbsp;&nbsp;New Sell In</a>&nbsp;&nbsp;
                    <button type="button" id="BtnDeleteSelected" class="btn btn-default" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Selected</button>
                    
                    <!-- Print Button -->
                    <button class="btn btn-warning pull-right btn--print"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</button>

                    <!-- Export Button-->
                    <div class="dropdown pull-right" style="margin-right: 10px;">
                      <button id="dLabel" class="btn btn-success" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-download"></i>&nbsp;&nbsp;Export&nbsp;&nbsp;<span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="{{ URL::to('export/sellin/xls')}}">Excel</a></li>
                        <li><a href="{{ URL::to('export/sellin/csv')}}">CSV</a></li>
                      </ul>
                    </div>

                    <!-- Filter Date Range -->
                    <div class="filter-on-head pull-right" style="margin-right: 10px;">
                        <label for="FilterDateRange">Filter Date: </label>
                        <div id="FilterDateRange" class="btn btn-default pull-right">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;&nbsp;
                            <span class="DateStart"></span> - <span class="DateEnd"></span>&nbsp;&nbsp;<b class="caret"></b>
                        </div>
                    </div>
                </div>
                <div class="box-body">

                    <table id="table" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="10">
                                    <input type="checkbox" name="select_all" value="1" id="example-select-all">
                                </th>
                                <th>ID</th>
                                <th>Cabang</th>
                                <th>Sub Cabang</th>
                                <th>Dealer</th>
                                <th>Sales</th>
                                <th>Date</th>
                                {{-- <th>Time</th> --}}
                                <th>Total QTY</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($sellins as $key => $value)
	                            <tr href="{{ URL::to('sellin/' . $value->id) }}">
                                    <td disabled></td>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ (empty($value->cabang->name))?'':$value->cabang->name }}</td>
                                    <td>{{ (empty($value->subcabang->name))?'':$value->subcabang->name }}</td>
                                    <td>{{ (empty($value->dealer->name))?'':$value->dealer->name }}</td>
	                                <td>{{ (empty($value->sales->first_name))?'':$value->sales->first_name }} {{ (empty($value->sales->last_name))?'':$value->sales->last_name }}</td>
                                    <td class="prettify-date text-center">{{ $value->datetime }}</td>
                                    {{-- <td class="prettify-time text-center">{{ $value->datetime }}</td> --}}
                                    <td class="text-right">{{ (empty($value->total_qty))?'':$value->total_qty }}</td>
                                    <td class="text-right">{{ (empty($value->total_price))?'':'Rp ' . number_format($value->total_price, 0, '', '.') }}</td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                {{ Form::close() }}

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ URL::to('js/daterangepicker.js') }}"></script>
@stop

@section('scripts')
	<script>

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var dateStart = Date.parse($(".DateStart").text());
            var dateEnd = Date.parse($(".DateEnd").text());
            var evalDate= Date.parse(aData[6]);

            // console.log(dateStart);
            // console.log(dateEnd);
            // console.log(aData[6]);
            // console.log(evalDate);

            if (isNaN(dateStart) || isNaN(dateEnd)) {
                return true;
            } else if (evalDate >= dateStart && evalDate <= dateEnd) { 
                return true;
            } else { 
                return false; 
            }
        }
    );

    $(function() {

        var $Table = $('#table').DataTable({
            // 'ajax': "{{ route('subcabang.index') }}",
            'columnDefs': [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                    }
                }
            ],
            'order': [[1, 'asc']]
        });

        // Handle click on "Select all" control
        $('#example-select-all').on('click', function(){
            // Get all rows with search applied
            var rows = $Table.rows({ 'search': 'applied' }).nodes();
            // Check/uncheck checkboxes for all rows in the table
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
            checkCheckedCheckboxs();
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#table tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is not checked
            if(!this.checked){
                var el = $('#example-select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if(el && el.checked && ('indeterminate' in el)){
                    // Set visual state of "Select all" control as 'indeterminate'
                    el.indeterminate = true;
                }
            }
            checkCheckedCheckboxs();
        });

        // If there any checked checkboxs
        var checkCheckedCheckboxs = function(){
            if ($('#table tbody').find('input[type="checkbox"]:checked').length == 0) {
                $('#BtnDeleteSelected').attr('disabled','disabled');
            } else {
                $('#BtnDeleteSelected').removeAttr('disabled');
            }
        }

        // on Delete
        $('#BtnDeleteSelected').click(function(ev){
            ev.preventDefault();
            alertify.confirm("Are you sure you want to delete selected items?", function () {
                $('#FormDeleteSelected').submit(); 
            });
        });

        // daterangepicker
        function cb(start, end) {
            if (isNaN(start) || isNaN(end)) {
                $('.DateStart').html('All');
                $('.DateEnd').html('Time');
            } else {
                $('.DateStart').html(start.format('MMMM D, YYYY'));
                $('.DateEnd').html(end.format('MMMM D, YYYY'));
            }
            $Table.draw();
        }
        cb(moment().startOf('month'), moment().endOf('month'));
        // cb('', '');
        $('#FilterDateRange').daterangepicker({
            ranges: {
               'All Time': [null, null],
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        $('#FilterDateRange').data('daterangepicker').setStartDate(moment().startOf('month'));
        $('#FilterDateRange').data('daterangepicker').setEndDate(moment().endOf('month'));

    });
	</script>
@stop