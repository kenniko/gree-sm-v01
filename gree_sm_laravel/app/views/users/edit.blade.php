@extends('master')

@section('header')
    <section class="content-header">
        @if (!empty($_GET['org']))
            @if ($_GET['org'] == 'cabang')
                <h1>Sales <small>Create</small></h1>
            @elseif($_GET['org'] == 'dealer')
                <h1>SPM <small>Create</small></h1>
            @endif
        @else
            <h1>User <small>Create</small></h1>
        @endif
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('user')}}">User</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($user, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['user.update', $user->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : null }}">
                                    {{ Form::label('username', 'Username *') }}
                                    {{ Form::text('username', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('username') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : null }}">
                                    {{ Form::label('email', 'Email *') }}
                                    {{ Form::email('email', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('email') }}</p>
                                </div>  
                            </div>
                            @if (empty($_GET['org']) && $user->id !== getCurrectUser()->id)
                                <div class="col-md-4">
                                    <div class="form-group{{ $errors->has('role') ? ' has-error' : null }}">
                                        {{ Form::label('role', 'Role *') }}
                                        {{Form::select('role', ([null => 'Select role'] + $roles), $user->roles()->first()->slug, ['id'=>'SelectRole', 'class'=>'form-control SelectRole'])}}
                                        <p class="help-block">{{ $errors->first('role') }}</p>
                                    </div>  
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : null }}">
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('password') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : null }}">
                                    {{ Form::label('password_confirmation', 'Confirm Password') }}
                                    {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('password_confirmation') }}</p>
                                </div>  
                            </div>
                            @if (empty($_GET['org']))
                                <div class="col-md-4">
                                    @if(Input::old('role') == 'sales' || Input::old('role') == 'cabang')
                                        <div class="form-group{{ $errors->has('cabang') ? ' has-error' : null }}">
                                            {{ Form::label('cabang', 'Cabang *') }}
                                            {{Form::select('cabang', ([null => 'Select cabang'] + $cabangs), Input::old('cabang'), ['id'=>'SelectCabang', 'class'=>'form-control SelectCabang'])}}
                                            <p class="help-block">{{ $errors->first('cabang') }}</p>
                                        </div>
                                    @elseif($user->roles()->first()->slug == 'sales' || $user->roles()->first()->slug == 'cabang')
                                        <div class="form-group{{ $errors->has('cabang') ? ' has-error' : null }}">
                                            {{ Form::label('cabang', 'Cabang *') }}
                                            {{Form::select('cabang', ([null => 'Select cabang'] + $cabangs), $user->cabang_id, ['id'=>'SelectCabang', 'class'=>'form-control SelectCabang'])}}
                                            <p class="help-block">{{ $errors->first('cabang') }}</p>
                                        </div>
                                    @else
                                        <div class="form-group hidden{{ $errors->has('cabang') ? ' has-error' : null }}">
                                            {{ Form::label('cabang', 'Cabang *') }}
                                            {{Form::select('cabang', ([null => 'Select cabang'] + $cabangs), null, ['id'=>'SelectCabang', 'class'=>'form-control SelectCabang', 'disabled'=>'disabled'])}}
                                            <p class="help-block">{{ $errors->first('cabang') }}</p>
                                        </div>
                                    @endif

                                    @if(Input::old('role') == 'spm')
                                        <div class="form-group{{ $errors->has('dealer') ? ' has-error' : null }}">
                                            {{ Form::label('dealer', 'Dealer *') }}
                                            {{Form::select('dealer', ([null => 'Select dealer'] + $dealers), Input::old('dealer'), ['id'=>'SelectDealer', 'class'=>'form-control SelectDealer'])}}
                                            <p class="help-block">{{ $errors->first('dealer') }}</p>
                                        </div>
                                    @elseif($user->roles()->first()->slug == 'spm')
                                        <div class="form-group{{ $errors->has('dealer') ? ' has-error' : null }}">
                                            {{ Form::label('dealer', 'Dealer *') }}
                                            {{Form::select('dealer', ([null => 'Select dealer'] + $dealers), $user->dealer_id, ['id'=>'SelectDealer', 'class'=>'form-control SelectDealer'])}}
                                            <p class="help-block">{{ $errors->first('dealer') }}</p>
                                        </div>
                                    @else
                                        <div class="form-group hidden{{ $errors->has('dealer') ? ' has-error' : null }}">
                                            {{ Form::label('dealer', 'Dealer *') }}
                                            {{Form::select('dealer', ([null => 'Select dealer'] + $dealers), null, ['id'=>'SelectDealer', 'class'=>'form-control SelectDealer', 'disabled'=>'disabled'])}}
                                            <p class="help-block">{{ $errors->first('dealer') }}</p>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : null }}">
                                    {{ Form::label('first_name', 'First Name *') }}
                                    {{ Form::text('first_name', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('first_name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : null }}">
                                    {{ Form::label('last_name', 'Last Name *') }}
                                    {{ Form::text('last_name', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('last_name') }}</p>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : null }}">
                                    {{ Form::label('phone', 'Phone') }}
                                    {{ Form::text('phone', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : null }}">
                                    {{ Form::label('gender', 'Gender *') }}
                                    <div class="radio">
                                      <label class="radio-inline">
                                        {{ Form::radio('gender', 'Male'); }}
                                        Male
                                      </label>
                                      <label class="radio-inline">
                                        {{ Form::radio('gender', 'Female'); }}
                                        Female
                                      </label>
                                    </div>
                                    <p class="help-block">{{ $errors->first('gender') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        @if (hasAccess('user'))
                            @if (!empty($_GET['org']))
                                @if ($_GET['org'] == 'cabang')
                                    {{ Form::hidden('role', 'sales' ) }}
                                    {{ Form::hidden('cabang', $_GET['cabid'] ) }}
                                    {{ Form::hidden('route_origin', $_GET['org'] ) }}
                                    {{ Form::hidden('id_origin', $_GET['cabid'] ) }}
                                    <a href="{{ URL::to('cabang/'.$_GET['cabid'])}}" class="btn btn-default">Back to cabang</a>
                                @elseif($_GET['org'] == 'dealer')
                                    {{ Form::hidden('role', 'spm' ) }}
                                    {{ Form::hidden('dealer', $_GET['dealerid'] ) }}
                                    {{ Form::hidden('route_origin', $_GET['org'] ) }}
                                    {{ Form::hidden('id_origin', $_GET['dealerid'] ) }}
                                    <a href="{{ URL::to('dealer/'.$_GET['dealerid'])}}" class="btn btn-default">Back to dealer</a>
                                @endif
                            @else
                                <a href="{{ URL::to('user')}}" class="btn btn-default">Cancel</a>
                            @endif
                        @else
                            <a href="{{ URL::to('/')}}" class="btn btn-default">Cancel</a>
                        @endif
                        @if (getCurrectUser()->id != $user->id)
                            <button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
                        @endif
                    </div>
                {{ Form::close() }}
                
                @if (getCurrectUser()->id != $user->id)
                    {{ Form::model($user, ['id'=>'FormDelete','method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) }}
                        {{ Form::hidden('username', $user->username ) }}
                    {{ Form::close() }}
                @endif
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });

    // on Change Role
    var $SelectCabang = $('.SelectCabang'),
        $SelectDealer = $('.SelectDealer');

    $('#SelectRole').change(function(ev){
        var val = $(this).val();
        if (val == 'sales' || val == 'cabang') {
            // console.log($(this).val());
            $SelectCabang.removeAttr('disabled').parent().removeClass('hidden');
            $SelectDealer.attr('disabled', true).parent().addClass('hidden');

        } else if (val == 'spm') {
            $SelectCabang.attr('disabled', true).parent().addClass('hidden');
            $SelectDealer.removeAttr('disabled').parent().removeClass('hidden');
        } else {
            $SelectCabang.attr('disabled', true).parent().addClass('hidden');
            $SelectDealer.attr('disabled', true).parent().addClass('hidden');
        }
    });
});
</script>
@stop