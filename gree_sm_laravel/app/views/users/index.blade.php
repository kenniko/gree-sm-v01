@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>User <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">User</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                {{Form::open(array('url' => 'users/delete', 'method' => 'DELETE', 'id'=>'FormDeleteSelected'))}}
                <div class="box-header with-border">
                    <a href="{{ URL::to('user/create')}}" class="btn btn-primary"><i class="fa fa-file"></i>&nbsp;&nbsp;New User</a>&nbsp;&nbsp;
                    <button type="button" id="BtnDeleteSelected" class="btn btn-default" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Selected</button>
                    <a href="{{ URL::to('role')}}" class="btn btn-default"><i class="fa fa-unlock-alt"></i>&nbsp;&nbsp;Manage Role</a>&nbsp;&nbsp;
                    <div class="filter-on-head label-absolute pull-right">
                        {{ Form::label('roles', 'Filter Role:') }}
                        {{Form::select('roles', ([null => 'All Role'] + $roles), null, ['id'=>'SelectRole', 'class'=>'form-control SelectRole'])}}
                    </div>
                </div>
                <div class="box-body" style='position: relative;'>
                    <table id="table" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="10">
                                    <input type="checkbox" name="select_all" value="1" id="example-select-all">
                                </th>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Under Cabang / Dealer</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($users as $key => $value)
	                            <tr href="{{ URL::to('user/' . $value->id) }}">
                                    <td disabled></td>
                                    <td>{{ $value->id }}</td>
                                    <td>{{ $value->full_name }}</td>
                                    <td>{{ $value->username }}</td>
	                                <td>{{ $value->email }}</td>
                                    <td>{{ $value->role }}</td>
                                    <td>{{ $value->under }}</td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                {{ Form::close() }}

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
	<script>
    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var roleValue = $(".SelectRole").val();
            var evalRole= aData[5];

            // console.log(roleValue);
            // console.log(evalRole);
            // console.log(aData[6]);
            // console.log(evalDate);

            if (roleValue == evalRole || roleValue == '') {
                return true;
            }
        }
    );

    $(function() {

        var $Table = $('#table').DataTable({
            // 'ajax': "{{ route('subcabang.index') }}",
            'columnDefs': [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                    }
                }
            ],
            'order': [[1, 'asc']]
        });

        // Filter Role
        $('#SelectRole').change( function() { 
            $Table.draw();
        });

        // Handle click on "Select all" control
        $('#example-select-all').on('click', function(){
            // Get all rows with search applied
            var rows = $Table.rows({ 'search': 'applied' }).nodes();
            // Check/uncheck checkboxes for all rows in the table
            $('input[type="checkbox"]', rows).prop('checked', this.checked);
            checkCheckedCheckboxs();
        });

        // Handle click on checkbox to set state of "Select all" control
        $('#table tbody').on('change', 'input[type="checkbox"]', function(){
            // If checkbox is not checked
            if(!this.checked){
                var el = $('#example-select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if(el && el.checked && ('indeterminate' in el)){
                    // Set visual state of "Select all" control as 'indeterminate'
                    el.indeterminate = true;
                }
            }
            checkCheckedCheckboxs();
        });

        // If there any checked checkboxs
        var checkCheckedCheckboxs = function(){
            if ($('#table tbody').find('input[type="checkbox"]:checked').length == 0) {
                $('#BtnDeleteSelected').attr('disabled','disabled');
            } else {
                $('#BtnDeleteSelected').removeAttr('disabled');
            }
        }

        // on Delete
        $('#BtnDeleteSelected').click(function(ev){
            ev.preventDefault();
            alertify.confirm("Are you sure you want to delete selected items?", function () {
                $('#FormDeleteSelected').submit(); 
            });
        });

    });
	</script>
@stop