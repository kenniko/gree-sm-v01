@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
    <!-- selectize.js -->
    <link rel="stylesheet" href="{{ URL::to('css/selectize.bootstrap3.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Stock Opname <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Stock Opname</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                {{--<div class="box-header with-border">
                    <a href="{{ URL::to('product/create')}}" class="btn btn-primary"><i class="fa fa-file"></i>&nbsp;&nbsp;New Product</a>&nbsp;&nbsp;
                    <button type="button" id="BtnDeleteSelected" class="btn btn-default" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Selected</button>
                </div>--}}
                <div class="box-body">
                    @if(getUserRole()->slug != 'spm')
                        <div class="filter-on-body col-sm-3">
                            {{ Form::label('dealers', 'Filter Dealer:') }}
                            {{Form::select('dealers', ([null => 'All Dealers', 'all' => '- All Dealers'] + $dealers), null, ['id'=>'SelectDealer', 'class'=>'form-control'])}}
                        </div>
                    @endif
                    <table id="TableStockOpname" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Product Code</th>
                                <th>Total Sell In</th>
                                <th>Total Sell Out</th>
                                <th>Ongoing Sell In</th>
                                <th>Ongoing Sell Out</th>
                                <th>Balance Stock</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <!-- selectize.js -->
    <script src="{{ URL::to('js/selectize.standalone.min.js') }}"></script>
@stop

@section('scripts')
	<script>
    $(function() {

        var TableStockOpname,
            $SelectDealer,
            SelectizeDealer;

        // load Data Sub Cabang
        TableStockOpname = $('#TableStockOpname').DataTable({
            order: [[1, 'asc']],
            processing: true,
            serverSide: true,
            @if(getUserRole()->slug == 'sales' || getUserRole()->slug == 'cabang')
                ajax: "{{ URL::to('ajax/stockopname?cabang_id=').getCurrectUser()->cabang->id }}",
            @elseif(getUserRole()->slug == 'spm')
                ajax: "{{ URL::to('ajax/stockopname?dealer_id=').getCurrectUser()->dealer->id }}",
            @else
                ajax: "{{ URL::to('ajax/stockopname') }}",
            @endif
            columns: [
                { data: 'name', name: 'products.name' },
                { data: 'code', name: 'products.code' },
                { data: 'total_sellin', name: 'total_sellin', orderable: false, searchable: false },
                { data: 'total_sellout', name: 'total_sellout', orderable: false, searchable: false },
                { data: 'ongoing_sellin', name: 'ongoing_sellin', orderable: false, searchable: false },
                { data: 'ongoing_sellout', name: 'ongoing_sellout', orderable: false, searchable: false },
                { data: 'balance_stock', name: 'balance_stock', orderable: false, searchable: false },
            ]
        });

        $SelectDealer = $('#SelectDealer').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            onItemRemove: function(value) {
                @if(getUserRole()->slug == 'sales' || getUserRole()->slug == 'cabang')
                    TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname?cabang_id=').getCurrectUser()->cabang->id }}");
                @else
                    TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname') }}");
                @endif
                TableStockOpname.draw();
            },
            onItemAdd: function(value, $item) {
                if (value == 'all') {
                    @if(getUserRole()->slug == 'sales' || getUserRole()->slug == 'cabang')
                        TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname?cabang_id=').getCurrectUser()->cabang->id }}");
                    @else
                        TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname') }}");
                    @endif
                    TableStockOpname.draw();      
                } else {
                    @if(getUserRole()->slug == 'sales' || getUserRole()->slug == 'cabang')
                        TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname?cabang_id=').getCurrectUser()->cabang->id.'&dealer_id=' }}" + value );
                    @else
                        TableStockOpname.ajax.url("{{ URL::to('ajax/stockopname?dealer_id=') }}" + value );
                    @endif
                    TableStockOpname.draw();
                }
            },
        });

        SelectizeDealer  = $SelectDealer[0].selectize;

    });
	</script>
@stop