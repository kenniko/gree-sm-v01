@extends('master')

@section('header')
    <section class="content-header">
        <h1>Product <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('product')}}">Product</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($product, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['product.update', $product->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Product name')) }}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('code') ? ' has-error' : null }}">
                                    {{ Form::label('code', 'Code *') }}
                                    {{ Form::text('code', null, array('class' => 'form-control', 'placeholder' => 'Product code')) }}
                                    <p class="help-block">{{ $errors->first('code') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::hidden('product_id', $product->id ) }}
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        <a href="{{ URL::to('product')}}" class="btn btn-default">Cancel</a>
                        <button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
                    </div>
                {{ Form::close() }}

                {{ Form::model($product, ['id'=>'FormDelete','method' => 'DELETE', 'route' => ['product.destroy', $product->id]]) }}
                {{ Form::close() }}
            </div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });
});
</script>
@stop