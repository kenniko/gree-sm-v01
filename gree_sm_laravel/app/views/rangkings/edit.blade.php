@extends('master')

@section('header')
    <section class="content-header">
        <h1>Ranking <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('rangking')}}">Ranking</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($cabang, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['rangking.update', $cabang->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Rangking name', 'readonly'=>'readonly')) }}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('percentage') ? ' has-error' : null }}">
                                    {{ Form::label('percentage', 'Persentase') }}
                                    {{ Form::number('percentage', null, array('class' => 'form-control', 'placeholder' => 'Rangking percentage')) }}
                                    <p class="help-block">{{ $errors->first('percentage') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::hidden('cabang_id', $cabang->id ) }}
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        <a href="{{ URL::to('rangking')}}" class="btn btn-default">Cancel</a>
                    </div>
                {{ Form::close() }}

            </div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });
});
</script>
@stop