@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Ranking <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Ranking</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                {{--
                <div class="box-header with-border">
                    <a href="{{ URL::to('customer/create')}}" class="btn btn-primary"><i class="fa fa-file"></i>&nbsp;&nbsp;New Rangking</a>&nbsp;&nbsp;
                    <button type="button" id="BtnDeleteSelected" class="btn btn-default" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete Selected</button>
                </div>
                --}}
                <div class="box-body">
                    <table id="table" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Cabang</th>
                                <th>Persentase</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($cabangs as $key => $value)
	                            <tr href="{{ URL::to('rangking/' . $value->id) }}">
                                    <td>{{ $value->name }}</td>
	                                <td>{{ $value->percentage }}</td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
	<script>
    $(function() {

        var $Table = $('#table').DataTable({
            // 'ajax': "{{ route('subcabang.index') }}",
            // 'columnDefs': [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            //     {
            //         'targets': 0,
            //         'searchable': false,
            //         'orderable': false,
            //         'className': 'dt-body-center',
            //         'render': function (data, type, full, meta){
            //             return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
            //         }
            //     }
            // ],
            'order': [[1, 'asc']]
        });

    });
	</script>
@stop