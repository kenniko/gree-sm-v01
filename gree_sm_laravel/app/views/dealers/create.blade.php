@extends('master')

@section('header')
    <section class="content-header">
        <h1>Dealer <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('cabang')}}">Cabang</a></li>
            <li><a href="{{ URL::to('subcabang')}}">Sub Cabang</a></li>
            <li><a href="{{ URL::to('dealer')}}">Dealer</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<!-- form start -->
        		{{ Form::open(array('url' => 'dealer' ) ) }}
        			<div class="box-body">
        				<div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('id') ? ' has-error' : null }}">
                                    {{ Form::label('id', 'Dealer ID *') }}
                                    {{ Form::text('id', null, array('class' => 'form-control', 'placeholder' => 'Dealer ID')) }}
                                    @if($errors->first('id'))
                                        <p class="help-block">{{ $errors->first('id') }}</p>
                                    @else
                                        <p class="help-block">Numbers only.</p>
                                    @endif
                                </div>  
                            </div>
		        			<div class="col-md-4">
		        				<div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
		        					{{ Form::label('name', 'Dealer Name *') }}
		        					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Dealer name')) }}
		        					<p class="help-block">{{ $errors->first('name') }}</p>
		        				</div>	
		        			</div>
                            @if (empty($_GET['org']))
    		        			<div class="col-md-4">
    		        				<div class="form-group{{ $errors->has('subcabang_id') ? ' has-error' : null }}">
    		        					{{ Form::label('subcabang_id', 'Sub Cabang *') }}
    		        					{{ Form::select('subcabang_id', ([null => 'Select sub cabang'] + $subcabangs), null, array('class' => 'form-control')) }}
    		        					<p class="help-block">{{ $errors->first('subcabang_id') }}</p>
    		        				</div>
    		        			</div>
                            @endif
		        		</div>
        			</div>
        			<!-- /.box-body -->

        			<div class="box-footer">
        				{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}&nbsp;
                        @if (!empty($_GET['org']))
                            {{ Form::hidden('subcabang_id', $_GET['subcabid'] ) }}
                            {{ Form::hidden('route_origin', $_GET['org'] ) }}
                            {{ Form::hidden('id_origin', $_GET['subcabid'] ) }}
                            <a href="{{ URL::to('subcabang/'.$_GET['subcabid'])}}" class="btn btn-default">Back to sub cabang</a>
                        @else
                            <a href="{{ URL::to('dealer')}}" class="btn btn-default">Cancel</a>
                        @endif
        			</div>
        		{{ Form::close() }}
        	</div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop