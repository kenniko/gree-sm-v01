@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/select.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Dealer <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('cabang')}}">Cabang</a></li>
            <li><a href="{{ URL::to('subcabang')}}">Sub Cabang</a></li>
            <li><a href="{{ URL::to('dealer')}}">Dealer</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<!-- form start -->
        		{{ Form::model($dealer, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['dealer.update', $dealer->id]]) }}
        			<div class="box-body">
        				<div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('id') ? ' has-error' : null }}">
                                    {{ Form::label('id', 'Dealer ID *') }}
                                    {{ Form::text('id', null, array('class' => 'form-control', 'placeholder' => 'Dealer ID')) }}
                                    @if($errors->first('id'))
                                        <p class="help-block">{{ $errors->first('id') }}</p>
                                    @else
                                        <p class="help-block">Numbers only.</p>
                                    @endif
                                </div>  
                            </div>
		        			<div class="col-md-4">
		        				<div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
		        					{{ Form::label('name', 'Name *') }}
		        					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Dealer name')) }}
		        					<p class="help-block">{{ $errors->first('name') }}</p>
		        				</div>	
		        			</div>
                            @if (empty($_GET['org']))
    		        			<div class="col-md-4">
    		        				<div class="form-group{{ $errors->has('subcabang_id') ? ' has-error' : null }}">
    		        					{{ Form::label('subcabang_id', 'Sub Cabang *') }}
    		        					{{ Form::select('subcabang_id', ([null => 'Select sub cabang'] + $subcabangs), $dealer->subcabang_id, array('class' => 'SelectSubCabang form-control')) }}
    		        					<p class="help-block">{{ $errors->first('subcabang_id') }}</p>
    		        				</div>
    		        			</div>
                            @endif
		        		</div>
                        <hr>

                        <!-- Spm -->
                        <div class="sub-box">
                            <div class="sub-box-header">
                                <a href="{{ URL::to('user/create?org=dealer&dealerid='.$dealer->id)}}" class="btn btn-default btn-sm"><i class="fa fa-file"></i>&nbsp;&nbsp;New SPM</a>&nbsp;
                                <button id="BtnAddSpm" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add SPM</button>&nbsp;
                                <button type="button" id="BtnRemoveSelectedSpm" class="btn btn-default btn-sm" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove Selected</button>
                            </div>
                            @if ($dealer->spm->count() > 0)
                                <table id="TableSpm" class="table table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <input type="checkbox" name="select_all" value="1" id="example-select-all-spm">
                                            </th>
                                            <th>ID</th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dealer->spm as $key => $value)
                                            <tr href="{{ URL::to('user/' . $value->id.'?org=dealer&dealerid='.$dealer->id)}}">
                                                <td disabled></td>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->full_name }}</td>
                                                <td>{{ $value->username }}</td>
                                                <td>{{ $value->email }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>

        			</div>
        			<!-- /.box-body -->

        			<div class="box-footer">
                        {{ Form::hidden('dealer_id', $dealer->id ) }}
        				{{ Form::submit('Update', array('class' => 'BtnUpdate btn btn-primary')) }}&nbsp;
                        @if (!empty($_GET['org']))
                            {{ Form::hidden('subcabang_id', $_GET['subcabid'] ) }}
                            {{ Form::hidden('route_origin', $_GET['org'] ) }}
                            {{ Form::hidden('id_origin', $_GET['subcabid'] ) }}
                            <a href="{{ URL::to('subcabang/'.$_GET['subcabid'])}}" class="btn btn-default">Back to sub cabang</a>
                        @else
                            <a href="{{ URL::to('dealer')}}" class="btn btn-default">Cancel</a>
                        @endif
        				<button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
        			</div>
        		{{ Form::close() }}

                {{ Form::model($dealer, ['id'=>'FormDelete', 'method' => 'DELETE', 'route' => ['dealer.destroy', $dealer->id]]) }}
                    {{ Form::hidden('name', $dealer->name ) }}
                    @if (!empty($_GET['org']))
                        {{ Form::hidden('route_origin', $_GET['org'] ) }}
                        {{ Form::hidden('id_origin', $_GET['subcabid'] ) }}
                    @endif
                {{ Form::close() }}
        	</div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Modal Add Spm -->
    <div class="modal fade" id="ModalAddSpm" tabindex="-1" role="dialog" aria-labelledby="ModalAddSpmLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalAddSpmLabel">All SPM</h4>
                </div>
                {{Form::open(['url' => 'users/update', 'method' => 'PATCH', 'id'=>'FormUpdateSelected'])}}
                <div class="modal-body">
                    <table id="TableAllSpm" class="table table-bordered table-hover table-select" width="100%">
                        <thead>
                            <tr>
                                <th width="5">
                                    <input type="checkbox" name="select_all_modal" value="1" id="example-select-all">
                                </th>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Dealer</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    {{ Form::hidden('dealer_id', $dealer->id ) }}
                    <button id="BtnSubmitModalSpm" type="submit" class="btn btn-primary" disabled>Add selected</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.select.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
<script>
$(function () {

    var SubCabangVal = $('.SelectSubCabang').val();

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });

    // on Update
    // $('.BtnUpdate').click(function(ev){
    //     ev.preventDefault();
    //     if (SubCabangVal == $('.SelectSubCabang').val() || $('.SelectSubCabang').val() == '') {
    //         $('#FormMain').submit();
    //     } else {
    //         alertify.confirm("This update will also affect Sell In and Sell Out data. Continue to update?", function () {
    //             $('#FormMain').submit(); 
    //         });
    //     }
    // });


    // ==========================================================================
    // Spm
    // ==========================================================================
    
    //
    // Table Main
    // ==========================================================================
    
    var $TableSpm = $('#TableSpm').DataTable({
        // 'ajax': "{{ route('dealer.index') }}",
        'columnDefs': [
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    // console.log(full);
                    return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    // Handle click on "Select all" control
    $('#example-select-all-spm').on('click', function(){
        // Get all rows with search applied
        var rows = $TableSpm.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('#TableSpm').find('input[type="checkbox"]', rows).prop('checked', this.checked);
        checkCheckedCheckboxsSpm();
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#TableSpm tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#example-select-all-spm').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control as 'indeterminate'
                el.indeterminate = true;
            }
        }
        checkCheckedCheckboxsSpm();
    });

    // If there any checked checkboxs
    var checkCheckedCheckboxsSpm = function(){
        if ($('#TableSpm tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnRemoveSelectedSpm').attr('disabled','disabled');
        } else {
            $('#BtnRemoveSelectedSpm').removeAttr('disabled');
        }
    }

    // on Click Selected Sub Cabang
    $('#BtnRemoveSelectedSpm').click(function(ev){
        alertify.confirm("Are you sure you want to remove selected items?", function () {
            $('#FormMain').attr("action","{{ URL::to('users/remove') }}");
            // $('#FormMain').attr("method","post");
            $('#FormMain').submit();
        });
    });



    // //
    // // Table Modal
    // // ==========================================================================
    
    var $TableModalSpm = {};

    // click Add Spm
    $('#BtnAddSpm').click(function(ev){
        ev.preventDefault();

        // load Data Spm
        $TableModalSpm = $('#TableAllSpm').DataTable({
            columnDefs: [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full['id']+'">';
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            order: [[1, 'asc']],
            processing: true,
            serverSide: true,
            ajax: "{{ URL::to('ajax/spm?origin=editdealer&id='.$dealer->id) }}",
            columns: [
                { data: '', name: '' },
                { data: 'id', name: 'users.id' },
                { data: 'first_name', name: 'users.first_name' },
                { data: 'last_name', name: 'users.last_name' },
                { data: 'username', name: 'users.username' },
                { data: 'dealer', name: 'dealers.name' }
            ]
        });

    });

    // show Modal after finish
    $('#TableAllSpm').on( 'draw.dt', function () {
        $('#ModalAddSpm').modal('show');

        // Handle click on heading containing "Select all" control
        $('thead', $TableModalSpm.table().container()).on('click', 'th:first-child', function(e) {
            $('input[type="checkbox"]', this).trigger('click');
        });
    });

    // on Modal hidden
    $('#ModalAddSpm').on('hidden.bs.modal', function (e) {
        $TableModalSpm.destroy();
        $TableModalSpm = {};
    });

    // Handle row selection event
    $('#TableAllSpm').on('select.dt deselect.dt', function(e, api, type, items) {
        if (e.type === 'select') {
        $('tr.selected input[type="checkbox"]', api.table().container()).prop('checked', true);
        } else {
        $('tr:not(.selected) input[type="checkbox"]', api.table().container()).prop('checked', false);
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSpm);  
        checkCheckedCheckboxsSpmModal();    
    });

    // Handle click on "Select all" control
    $('#TableAllSpm thead').on('click', 'input[type="checkbox"]', function(e) {
        if (this.checked) {
        $TableModalSpm.rows({ page: 'current' }).select();        
        } else {
        $TableModalSpm.rows({ page: 'current' }).deselect();
        }

        e.stopPropagation();
    });

    // Handle table draw event
    $('#TableAllSpm').on('draw.dt', function() {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModalSpm);
        checkCheckedCheckboxsSpmModal();
    });

    // If there any checked checkboxs inside modal
    var checkCheckedCheckboxsSpmModal = function(){
        if ($('#TableAllSpm tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnSubmitModalSpm').attr('disabled','disabled');
        } else {
            $('#BtnSubmitModalSpm').removeAttr('disabled');
        }
    }
});

//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {
   var $table = table.table().container();
   var $chkbox_all = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all = $('thead input[type="checkbox"]', $table).get(0);

   // If none of the checkboxes are checked
   if ($chkbox_checked.length === 0) {
      chkbox_select_all.checked = false;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length) {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = true;
      }
   }
}
</script>
@stop