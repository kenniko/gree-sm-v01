<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | GREE Sales Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::to('css/ionicons.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('css/icheck.blue.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::to('css/skin-blue.min.css') }}">
    <!-- Main style -->
    <link rel="stylesheet" href="{{ URL::to('css/style.css') }}">

    <!-- Google Font -->
    {{-- <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'> --}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="apple-touch-icon" sizes="57x57" href="{{ URL::to('favicons/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ URL::to('favicons/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::to('favicons/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::to('favicons/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::to('favicons/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::to('favicons/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::to('favicons/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::to('favicons/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('favicons/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-194x194.png') }}" sizes="194x194">
    <link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ URL::to('favicons/android-chrome-192x192.png') }}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ URL::to('favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ URL::to('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="{{ URL::to('favicons/mstile-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <small><b>GREE</b> Sales Management</small>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Hello, have a good day&nbsp;&nbsp;( :</p>

            {{ Form::open(array('autocomplete' => 'off')) }}
                <div class="form-group has-feedback{{ $errors->has('login') ? ' has-error' : null }}">
                    {{ Form::text('login', null, array('class' => 'form-control', 'placeholder' => 'Username or Email')) }}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <p class="help-block">{{ $errors->first('login') }}</p>
                </div>
                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : null }}">
                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <p class="help-block">{{ $errors->first('password') }}</p>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                {{ Form::checkbox('remember') }} Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        {{ Form::submit('Login', array('class' => 'btn btn-primary btn-block btn-flat')) }}
                    </div>
                    <!-- /.col -->
                </div>
            {{ Form::close() }}

            </div>
            <!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{ URL::to('js/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::to('js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::to('js/icheck.min.js') }}"></script>
<!-- alertify.js -->
<script src="{{ URL::to('js/alertify.js') }}"></script>
<!-- Main JS -->
<script src="{{ URL::to('js/main.js') }}"></script>

@if ($message = Session::get('success'))
<script>
    $(function () { setTimeout(function() { 
        alertify.logPosition("top right");
        alertify.closeLogOnClick(true).success("{{ $message }}"); 
    }, 125); });
</script>
@endif

@if ($message = $errors->first())
<script>
    $(function () { setTimeout(function() { 
        alertify.logPosition("top right");
        alertify.closeLogOnClick(true).error("{{ $message }}"); 
    }, 125); });
</script>
@endif

</body>
</html>
