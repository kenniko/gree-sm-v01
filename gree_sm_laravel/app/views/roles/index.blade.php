@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Role <small>Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('user')}}">User</a></li>
            <li class="active">Role</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <a href="{{ URL::to('user')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Back to User</a>
                </div>
                <div class="box-body" style='position: relative;'>
                    <table id="table" class="table table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Role Name</th>
                                <th>Slug</th>
                                <th width="300">Permissions</th>
                                <th>Users</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($roles as $key => $value)
	                            <tr href="{{ URL::to('role/' . $value->id) }}">
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->slug }}</td>
	                                <td>@foreach(array_keys($value->permissions) as $key_permission => $permission)@if ($key_permission > 0), @endif{{$permission}}@endforeach
                                    </td>
                                    <td>{{ $value->users()->count(); }}</td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                {{ Form::close() }}

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
	<script>
    $(function() {

        // var $Table = $('#table').DataTable({
        //     // 'ajax': "{{ route('subcabang.index') }}",
        //     'columnDefs': [
        //         {
        //             "targets": [ 1 ],
        //             "visible": false,
        //             "searchable": false
        //         },
        //         {
        //             'targets': 0,
        //             'searchable': false,
        //             'orderable': false,
        //             'className': 'dt-body-center',
        //             'render': function (data, type, full, meta){
        //                 return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
        //             }
        //         }
        //     ],
        //     'order': [[1, 'asc']]
        // });

        // // Filter Role
        // $('#SelectRole').change( function() { 
        //     $Table.draw();
        // });

        // // Handle click on "Select all" control
        // $('#example-select-all').on('click', function(){
        //     // Get all rows with search applied
        //     var rows = $Table.rows({ 'search': 'applied' }).nodes();
        //     // Check/uncheck checkboxes for all rows in the table
        //     $('input[type="checkbox"]', rows).prop('checked', this.checked);
        //     checkCheckedCheckboxs();
        // });

        // // Handle click on checkbox to set state of "Select all" control
        // $('#table tbody').on('change', 'input[type="checkbox"]', function(){
        //     // If checkbox is not checked
        //     if(!this.checked){
        //         var el = $('#example-select-all').get(0);
        //         // If "Select all" control is checked and has 'indeterminate' property
        //         if(el && el.checked && ('indeterminate' in el)){
        //             // Set visual state of "Select all" control as 'indeterminate'
        //             el.indeterminate = true;
        //         }
        //     }
        //     checkCheckedCheckboxs();
        // });

        // // If there any checked checkboxs
        // var checkCheckedCheckboxs = function(){
        //     if ($('#table tbody').find('input[type="checkbox"]:checked').length == 0) {
        //         $('#BtnDeleteSelected').attr('disabled','disabled');
        //     } else {
        //         $('#BtnDeleteSelected').removeAttr('disabled');
        //     }
        // }

        // // on Delete
        // $('#BtnDeleteSelected').click(function(ev){
        //     ev.preventDefault();
        //     alertify.confirm("Are you sure you want to delete selected items?", function () {
        //         $('#FormDeleteSelected').submit(); 
        //     });
        // });

    });
	</script>
@stop