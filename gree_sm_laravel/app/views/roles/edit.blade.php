@extends('master')

@section('css')
    <!-- selectize.js -->
    <link rel="stylesheet" href="{{ URL::to('css/selectize.bootstrap3.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Role <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('user')}}">User</a></li>
            <li><a href="{{ URL::to('role')}}">Role</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($role, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['role.update', $role->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('slug') ? ' has-error' : null }}">
                                    {{ Form::label('slug', 'Slug *') }}
                                    {{ Form::text('slug', null, array('class' => 'form-control')) }}
                                    <p class="help-block">{{ $errors->first('slug') }}</p>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group{{ $errors->has('permissions') ? ' has-error' : null }}">
                                    {{ Form::label('permissions', 'Permissions *') }}
                                    {{Form::select('permissions[]', ([
                                        null => '',
                                        'superadmin' => 'Super Admin',
                                        'sellin' => 'Sell In',
                                        'sellout' => 'Sell Out',
                                        'stockopname' => 'Stock Opname',
                                        'report' => 'Report',
                                        'rangking' => 'Rangking',
                                        'cabang' => 'Cabang',
                                        'subcabang' => 'Sub Cabang',
                                        'dealer' => 'Dealer',
                                        'product' => 'Product',
                                        'customer' => 'Customer',
                                        'user' => 'User',
                                        'role' => 'Role'
                                    ]), $role->permissions, ['multiple' => 'multiple','id'=>'SelectPermissions', 'class'=>'form-control']);}}
                                    <p class="help-block">{{ $errors->first('permissions') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        <a href="{{ URL::to('role')}}" class="btn btn-default">Cancel</a>
                    </div>
                {{ Form::close() }}
                
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- selectize.js -->
    <script src="{{ URL::to('js/selectize.standalone.min.js') }}"></script>
@stop

@section('scripts')
    <script>
    $(function() {
        
        $('#SelectPermissions').selectize({
            plugins: ['remove_button'],
            maxItems: null
        });

        // SelectizeCabang  = $SelectCabang[0].selectize;

    });
    </script>
@stop