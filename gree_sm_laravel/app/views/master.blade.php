<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')GREE Sales Management</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	{{-- CSRF Token --}}
	{{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{ URL::to('css/ionicons.min.css') }}">
	<!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('css/icheck.blue.css') }}">

	@yield('css')

	<!-- Theme style -->
	<link rel="stylesheet" href="{{ URL::to('css/AdminLTE.min.css') }}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{ URL::to('css/skin-blue.min.css') }}">
	<!-- Pace style -->
	<link rel="stylesheet" href="{{ URL::to('css/pace.min.css') }}">
	<!-- Main style -->
	<link rel="stylesheet" href="{{ URL::to('css/style.css') }}">

	<!-- Google Font -->
	{{-- <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'> --}}

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="apple-touch-icon" sizes="57x57" href="{{ URL::to('favicons/apple-touch-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ URL::to('favicons/apple-touch-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ URL::to('favicons/apple-touch-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::to('favicons/apple-touch-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ URL::to('favicons/apple-touch-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::to('favicons/apple-touch-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ URL::to('favicons/apple-touch-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::to('favicons/apple-touch-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ URL::to('favicons/apple-touch-icon-180x180.png') }}">
	<link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-194x194.png') }}" sizes="194x194">
	<link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-96x96.png') }}" sizes="96x96">
	<link rel="icon" type="image/png" href="{{ URL::to('favicons/android-chrome-192x192.png') }}" sizes="192x192">
	<link rel="icon" type="image/png" href="{{ URL::to('favicons/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{{ URL::to('favicons/manifest.json') }}">
	<link rel="mask-icon" href="{{ URL::to('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="{{ URL::to('favicons/mstile-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">

</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="{{ URL::to('/') }}" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>G</b>SM</span>
				<!-- logo for regular state and mobile devices -->
				<small><span class="logo-lg"><b>GREE</b> Sales Management</span></small>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{{ getUserAvatar() }}" class="user-image" alt="User Image">
								<span class="hidden-xs">{{getUserFullName()}}</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="{{ getUserAvatar() }}" class="img-circle" alt="User Image">

									<p>
										{{getUserFullName()}}
										<small>{{getUserRole()->name}}</small>
										<small>{{getCompany()}}</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="{{ URL::to('user/'.getCurrectUser()->id) }}" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="{{ URL::to('logout') }}" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
	        <!-- sidebar: style can be found in sidebar.less -->
	        <section class="sidebar">
	            <!-- /.search form -->
	            <!-- sidebar menu: : style can be found in sidebar.less -->
	            <ul class="sidebar-menu">
	                <li class="header">MAIN MENU</li>
	                <li class="{{setActiveMenu('/')}}">
	                    <a href="{{ URL::to('/') }}">
	                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('sellin')}} {{hideIfNoAccess('sellin')}}">
	                    <a href="{{ URL::to('/sellin') }}">
	                        <i class="fa fa-arrow-left"></i> <span>Sell In</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('sellout')}} {{hideIfNoAccess('sellout')}}">
	                    <a href="{{ URL::to('/sellout') }}">
	                        <i class="fa fa-arrow-right"></i> <span>Sell Out</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('stockopname')}} {{hideIfNoAccess('stockopname')}}">
	                    <a href="{{ URL::to('/stockopname') }}">
	                        <i class="fa fa-pie-chart"></i> <span>Stock Opname</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('report')}} {{hideIfNoAccess('report')}}">
	                    <a href="{{ URL::to('/report') }}">
	                        <i class="fa fa-file-text"></i> <span>Report</span>
	                    </a>
	                </li>
	                {{-- 
	                <li class="treeview">
	                    <a href="#">
	                        <i class="fa fa-files-o"></i>
	                        <span>Layout Options</span>
	                        <span class="label label-primary pull-right">4</span>
	                    </a>
	                    <ul class="treeview-menu">
	                        <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
	                        <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
	                        <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
	                        <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
	                    </ul>
	                </li>
	                <li>
	                    <a href="../widgets.html">
	                        <i class="fa fa-th"></i> <span>Widgets</span>
	                        <small class="label pull-right bg-green">new</small>
	                    </a>
	                </li> --}}
	                <li class="header">SETTINGS</li>
	                @if (hasAccess('cabang') ||hasAccess('subcabang') ||hasAccess('dealer') ||hasAccess('product') || hasAccess('customer'))
	                <li class="treeview {{setActiveMenu('cabang')}}{{setActiveMenu('subcabang')}}{{setActiveMenu('dealer')}}{{setActiveMenu('product')}}{{setActiveMenu('customer')}}">
	                    <a href="#">
	                        <i class="fa fa-database"></i> <span>Database</span> <i class="fa fa-angle-left pull-right"></i>
	                    </a>
	                    <ul class="treeview-menu">
	                        <li class="{{ setActiveMenu('cabang') }} {{hideIfNoAccess('cabang')}}"><a href="{{ URL::to('cabang') }}"><i class="fa fa-industry"></i> Cabang</a></li>
	                        <li class="{{ setActiveMenu('subcabang') }} {{hideIfNoAccess('subcabang')}}"><a href="{{ URL::to('/subcabang') }}"><i class="fa fa-bank"></i> Sub Cabang</a></li>
	                        <li class="{{ setActiveMenu('dealer') }} {{hideIfNoAccess('dealer')}}"><a href="{{ URL::to('/dealer') }}"><i class="fa fa-building"></i> Dealer</a></li>
	                        <li class="{{ setActiveMenu('product') }} {{hideIfNoAccess('product')}}"><a href="{{ URL::to('/product') }}"><i class="fa fa-cubes"></i> Product</a></li>
	                        <li class="{{ setActiveMenu('customer') }} {{hideIfNoAccess('customer')}}"><a href="{{ URL::to('/customer') }}"><i class="fa fa-male"></i> Customer</a></li>
	                    </ul>
	                </li>
	                @endif
	                <li class="{{setActiveMenu('rangking')}} {{hideIfNoAccess('rangking')}}">
	                    <a href="{{ URL::to('/rangking') }}">
	                        <i class="fa fa-signal"></i> <span>Ranking</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('user')}} {{hideIfNoAccess('user')}}">
	                    <a href="{{ URL::to('/user') }}">
	                        <i class="fa fa-users"></i> <span>Users</span>
	                    </a>
	                </li>
	                <li class="{{setActiveMenu('role')}} {{hideIfNoAccess('role')}}">
	                    <a href="{{ URL::to('/role') }}">
	                        <i class="fa fa-unlock-alt"></i> <span>Roles</span>
	                    </a>
	                </li>
	                {{-- <li>
	                    <a href="../widgets.html">
	                        <i class="fa fa-th"></i> <span>Customer</span>
	                    </a>
	                </li> --}}
	            </ul>
	        </section>
	        <!-- /.sidebar -->
	    </aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
		    @yield('header')

		    <!-- Main content -->
		    <section class="content">
				@yield('content')
			</section>

			@yield('second_content')

		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.3.1
			</div>
			<strong>Copyright &copy; {{date('Y')}} <a href="http://gree.co.id/">GREE</a>.</strong> All rights
			reserved.
		</footer>
	</div>
	<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="{{ URL::to('js/jquery.min.js') }}"></script>
<!-- PACE -->
<script data-pace-options='{ "startOnPageLoad": false }' src="{{ URL::to('js/pace.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::to('js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ URL::to('js/icheck.min.js') }}"></script>
<!-- alertify.js -->
<script src="{{ URL::to('js/alertify.js') }}"></script>
<!-- Moment.js -->
<script src="{{ URL::to('js/moment.min.js') }}"></script>
<!-- SlimScroll -->
<!-- <script src="{{ URL::to('js/jquery.slimscroll.min.js') }}"></script> -->


@yield('js')

<!-- iCheck -->
<script src="{{ URL::to('js/icheck.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::to('js/app.min.js') }}"></script>
<!-- Main JS -->
<script src="{{ URL::to('js/main.js') }}"></script>

<!-- page script -->
@yield('scripts')

@if ($message = Session::get('success'))
<script>
    $(function () { setTimeout(function() { 
    	alertify.logPosition("top right");
    	alertify.delay(5000);
    	alertify.closeLogOnClick(true).success("{{ $message }}"); 
    }, 1); });
</script>
@endif

@if ($message = $errors->first())
<script>
    $(function () { setTimeout(function() { 
    	alertify.logPosition("top right");
    	alertify.delay(5000);
    	alertify.closeLogOnClick(true).error("{{ $message }}"); 
    }, 1); 

    {{--
    @foreach ($errors->all() as $error)
	    console.log('{{$error}}');
	@endforeach
	--}}

	});
</script>
@endif

</body>
</html>
