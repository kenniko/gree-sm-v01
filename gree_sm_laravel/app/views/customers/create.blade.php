@extends('master')

@section('header')
    <section class="content-header">
        <h1>Customer <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('customer')}}">Customer</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::open(array('url' => 'customer' ) ) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Customer name')) }}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : null }}">
                                    {{ Form::label('phone', 'Phone') }}
                                    {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Phone number')) }}
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}&nbsp;
                        <a href="{{ URL::to('customer')}}" class="btn btn-default">Cancel</a>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop