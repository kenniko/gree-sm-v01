@extends('master')

@section('header')
    <section class="content-header">
        <h1>Customer <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('customer')}}">Customer</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($customer, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['customer.update', $customer->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Customer name')) }}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : null }}">
                                    {{ Form::label('phone', 'Phone') }}
                                    {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Customer phone')) }}
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{ Form::hidden('customer_id', $customer->id ) }}
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        <a href="{{ URL::to('customer')}}" class="btn btn-default">Cancel</a>
                        <button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
                    </div>
                {{ Form::close() }}

                {{ Form::model($customer, ['id'=>'FormDelete', 'method' => 'DELETE', 'route' => ['customer.destroy', $customer->id]]) }}
                    {{ Form::hidden('name', $customer->name ) }}
                {{ Form::close() }}
            </div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });
});
</script>
@stop