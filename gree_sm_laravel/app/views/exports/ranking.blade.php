<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <table>
        <thead>
            <tr>
                <td colspan="5">Ranking</td>
            </tr>
            <tr>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>Cabang</th>
                <th>Bulan</th>
                <th></th>
                <th>Tahun</th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <th>Point</th>
                <th>Ranking</th>
                <th>Point</th>
                <th>Ranking</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rangkings as $key => $value)
                <tr>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->point_bulan }}</td>
                    <td>{{ $value->rangking_bulan }}</td>
                    <td>{{ $value->point_tahun }}</td>
                    <td>{{ $value->rangking_tahun }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>