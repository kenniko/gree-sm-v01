<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<table>
        <thead>
        	<tr>
        		<td colspan="17">Sell In</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td data-format="dd/mm/yyyy"></td>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
        	</tr>
        	<tr>
        		<td></td>
        	</tr>
        </thead>
        <thead>
        	<tr>
                <th>Dealer ID</th>
                <th>Dealer Name</th>
                <th>Cabang</th>
                <th>Sub Cabang</th>
                <th>Sales Name</th>
                <th>Date</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>QTY</th>
                <th>Amount</th>
                <th>Omsat</th>
                <th>Month</th>
                <th>Day</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($productins as $productin)
                <tr>
                    <td>{{ $productin->dealer_id }}</td>
                    <td>{{ $productin->dealer_name }}</td>
                    <td>{{ $productin->cabang_name }}</td>
                    <td>{{ $productin->subcabang_name }}</td>
                    <td>{{ $productin->sales_name }}</td>
                    <td data-format="dd/mm/yyyy"></td>
                    <td>{{ $productin->product->name }}</td>
                    <td>{{ $productin->product->code }}</td>
                    <td>{{ $productin->quantity }}</td>
                    <td>{{ $productin->price }}</td>
                    <td></td>
                    <td data-format="mm"></td>
                    <td data-format="dd"></td>
                </tr>
            @endforeach
            <tr>
                <td>TOTAL</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
	</table>
</body>
</html>