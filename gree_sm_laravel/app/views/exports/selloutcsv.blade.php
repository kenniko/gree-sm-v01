<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<table>
        <thead>
        	<tr>
        		<td colspan="17">Sell Out</td>
        	</tr>
        	<tr>
        		<td></td>
                <td data-format="dd/mm/yyyy"></td>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
        	</tr>
        	<tr>
        		<td></td>
        	</tr>
        </thead>
        <thead>
        	<tr>
                <th>Dealer ID</th>
                <th>Dealer Name</th>
                <th>Cabang</th>
                <th>Sub Cabang</th>
                <th>Sales Name</th>
                <th>Date</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>QTY</th>
                <th>Amount</th>
                <th>Omsat</th>
                <th>Transaction Type</th>
                <th>SPG/SPM Name</th>
                <th>Customer Name</th>
                <th>Customer Phone</th>
                <th>Month</th>
                <th>Day</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($productouts as $productout)
                <tr>
                    <td>{{ $productout->dealer_id }}</td>
                    <td>{{ $productout->dealer_name }}</td>
                    <td>{{ $productout->cabang_name }}</td>
                    <td>{{ $productout->subcabang_name }}</td>
                    <td>{{ $productout->sales_name }}</td>
                    <td data-format="dd/mm/yyyy"></td>
                    <td>{{ $productout->product->name }}</td>
                    <td>{{ $productout->product->code }}</td>
                    <td>{{ $productout->quantity }}</td>
                    <td>{{ $productout->price }}</td>
                    <td></td>
                    <td>{{ $productout->sellout->type }}</td>
                    <td>{{ $productout->spm_name }}</td>
                    <td>{{ $productout->customer_name }}</td>
                    <td>{{ $productout->customer_phone }}</td>
                    <td data-format="mm"></td>
                    <td data-format="dd"></td>
                </tr>
            @endforeach
            <tr>
                <td>TOTAL</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
	</table>
</body>
</html>