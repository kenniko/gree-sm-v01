<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <table>
        <thead>
            <tr>
                <td colspan="7">Stock Opname Dealer</td>
            </tr>
            <tr>
                <td>Date:</td>
                <td data-format="dd/mm/yyyy"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </thead>
        <thead>
            <tr>
                <td>All Dealers</td>
            </tr>
            <tr>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Total Sell In</th>
                <th>Total Sell Out</th>
                <th>Ongoing Sell In</th>
                <th>Ongoing Sell Out</th>
                <th>Balance Stock</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products_alldealers as $product)
                <tr>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['code']}}</td>
                    <td>{{$product['total_sellin']}}</td>
                    <td>{{$product['total_sellout']}}</td>
                    <td>{{$product['ongoing_sellin']}}</td>
                    <td>{{$product['ongoing_sellout']}}</td>
                    <td></td>
                </tr>
            @endforeach
            <tr>
                <td>TOTAL</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </tbody>
        @foreach($products_eachdealer as $data)
        <thead>
            <tr>
                <td>Dealer ID:</td>
                <td>{{$data['dealer_id']}}</td>
            </tr>
            <tr>
                <td>Dealer Name:</td>
                <td>{{$data['dealer_name']}}</td>
            </tr>
        </thead>
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Total Sell In</th>
                <th>Total Sell Out</th>
                <th>Ongoing Sell In</th>
                <th>Ongoing Sell Out</th>
                <th>Balance Stock</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data['products'] as $product)
                <tr>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['code']}}</td>
                    <td>{{$product['total_sellin']}}</td>
                    <td>{{$product['total_sellout']}}</td>
                    <td>{{$product['ongoing_sellin']}}</td>
                    <td>{{$product['ongoing_sellout']}}</td>
                    <td></td>
                </tr>
            @endforeach
            <tr>
                <td>TOTAL</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </tbody>
        @endforeach
    </table>
</body>
</html>