<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<table>
        <thead>
        	<tr>
        		<td colspan="13">Report PT. GREE Electric Appliances Indonesia Sales</td>
        	</tr>
        	<tr>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
                <td></td>
                <td data-format="dd/mm/yyyy"></td>
            </tr>
        	<tr>
        		<td></td>
        	</tr>
        </thead>
        <thead>
        	<tr>
                <th>Cabang</th>
                <th>Hari</th>
                <th></th>
                <th></th>
                <th></th>
                <th>Bulan</th>
                <th></th>
                <th></th>
                <th></th>
                <th>Tahun</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
            	<th></th>
                <th>Sale In</th>
                <th></th>
                <th>Sale Out</th>
                <th></th>
                <th>Sale In</th>
                <th></th>
                <th>Sale Out</th>
                <th></th>
                <th>Sale In</th>
                <th></th>
                <th>Sale Out</th>
                <th></th>
            </tr>
            <tr>
            	<th></th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Qty</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($cabangs as $key => $value)
                <tr>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->total_quantity_sellin_hari }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellin_hari }}</td>
                    <td>{{ $value->total_quantity_sellout_hari }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellout_hari }}</td>
                    <td>{{ $value->total_quantity_sellin_bulan }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellin_bulan }}</td>
                    <td>{{ $value->total_quantity_sellout_bulan }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellout_bulan }}</td>
                    <td>{{ $value->total_quantity_sellin_tahun }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellin_tahun }}</td>
                    <td>{{ $value->total_quantity_sellout_tahun }}</td>
                    <td data-format="Rp #,##0_-">{{ $value->total_price_sellout_tahun }}</td>
                </tr>
            @endforeach
            <tr>
                <td>TOTAL</td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
                <td></td>
                <td data-format="Rp #,##0_-"></td>
            </tr>
        </tbody>
	</table>
</body>
</html>