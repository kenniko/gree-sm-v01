@extends('master')

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::to('css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/select.bootstrap.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Sub Cabang <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('cabang')}}">Cabang</a></li>
            <li><a href="{{ URL::to('subcabang')}}">Sub Cabang</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<!-- form start -->
        		{{ Form::model($subcabang, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['subcabang.update', $subcabang->id]]) }}
        			<div class="box-body">
        				<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
		        					{{ Form::label('name', 'Name *') }}
		        					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Sub cabang name')) }}
		        					<p class="help-block">{{ $errors->first('name') }}</p>
		        				</div>	
		        			</div>
                            @if (empty($_GET['org']))
    		        			<div class="col-md-6">
    		        				<div class="form-group{{ $errors->has('cabang_id') ? ' has-error' : null }}">
    		        					{{ Form::label('cabang_id', 'Cabang *') }}
    		        					{{ Form::select('cabang_id', ([null => 'Select cabang'] + $cabangs), $subcabang->cabang_id, array('class' => 'form-control')) }}
    		        					<p class="help-block">{{ $errors->first('cabang_id') }}</p>
    		        				</div>
    		        			</div>
                            @endif
		        		</div>
                        <hr>

                        <!-- Dealers -->
                        <div class="sub-box">
                            <div class="sub-box-header">
                                <a href="{{ URL::to('dealer/create?org=subcabang&subcabid='.$subcabang->id)}}" class="btn btn-default btn-sm"><i class="fa fa-file"></i>&nbsp;&nbsp;New Dealer</a>&nbsp;
                                <button id="BtnAddDealer" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Dealer</button>&nbsp;
                                <button type="button" id="BtnRemoveSelected" class="btn btn-default btn-sm" disabled><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove Selected</button>
                            </div>
                            @if ($subcabang->dealers->count() > 0)
                                <table id="TableDealer" class="table table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">
                                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                                            </th>
                                            <th>Dealer ID</th>
                                            <th>Dealer Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($subcabang->dealers as $key => $value)
                                            <tr href="{{ URL::to('dealer/' . $value->id.'?org=subcabang&subcabid='.$subcabang->id)}}">
                                                <td disabled></td>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
        			</div>
        			<!-- /.box-body -->

        			<div class="box-footer">
                        {{ Form::hidden('subcabang_id', $subcabang->id ) }}
        				{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                        @if (!empty($_GET['org']))
                            {{ Form::hidden('cabang_id', $_GET['cabid'] ) }}
                            {{ Form::hidden('route_origin', $_GET['org'] ) }}
                            {{ Form::hidden('id_origin', $_GET['cabid'] ) }}
                            <a href="{{ URL::to('cabang/'.$_GET['cabid'])}}" class="btn btn-default">Back to cabang</a>
                        @else
                            <a href="{{ URL::to('subcabang')}}" class="btn btn-default">Cancel</a>
                        @endif
        				<button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
        			</div>
        		{{ Form::close() }}

                {{ Form::model($subcabang, ['id'=>'FormDelete', 'method' => 'DELETE', 'route' => ['subcabang.destroy', $subcabang->id]]) }}
                    {{ Form::hidden('name', $subcabang->name ) }}
                    @if (!empty($_GET['org']))
                        {{ Form::hidden('route_origin', $_GET['org'] ) }}
                        {{ Form::hidden('id_origin', $_GET['cabid'] ) }}
                    @endif
                {{ Form::close() }}
        	</div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Modal Add Sub Cabang -->
    <div class="modal fade" id="ModalAddDealer" tabindex="-1" role="dialog" aria-labelledby="ModalAddDealerLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalAddDealerLabel">All Dealer</h4>
                </div>
                {{Form::open(['url' => 'dealers/update', 'method' => 'PATCH', 'id'=>'FormUpdateSelected'])}}
                <div class="modal-body">
                    <table id="TableAllDealer" class="table table-bordered table-hover table-select" width="100%">
                        <thead>
                            <tr>
                                <th width="5">
                                    <input type="checkbox" name="select_all_modal" value="1" id="example-select-all">
                                </th>
                                <th>Dealer ID</th>
                                <th>Dealer Name</th>
                                <th>Sub Cabang</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    {{ Form::hidden('subcabang_id', $subcabang->id ) }}
                    <button id="BtnSubmitModal" type="submit" class="btn btn-primary" disabled>Add selected</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.select.min.js') }}"></script>
    <script src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
@stop

@section('scripts')
<script>
$(function () {

    // on Delete
    $('#BtnDelete').click(function(ev){
        ev.preventDefault();
        alertify.confirm("Are you sure you want to delete this?", function () {
            $('#FormDelete').submit(); 
        });
    });


    //
    // Table Main
    // ==========================================================================
    
    var $Table = $('#TableDealer').DataTable({
        // 'ajax': "{{ route('subcabang.index') }}",
        'columnDefs': [
            // {
            //     "targets": [ 1 ],
            //     "visible": false,
            //     "searchable": false
            // },
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){
                    // console.log(full);
                    return '<input type="checkbox" name="ids[]" value="'+full[1]+'">';
                }
            }
        ],
        'order': [[1, 'asc']]
    });

    // Handle click on "Select all" control
    $('#example-select-all').on('click', function(){
        // Get all rows with search applied
        var rows = $Table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
        checkCheckedCheckboxs();
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#TableDealer tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#example-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control as 'indeterminate'
                el.indeterminate = true;
            }
        }
        checkCheckedCheckboxs();
    });

    // If there any checked checkboxs
    var checkCheckedCheckboxs = function(){
        if ($('#TableDealer tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnRemoveSelected').attr('disabled','disabled');
        } else {
            $('#BtnRemoveSelected').removeAttr('disabled');
        }
    }

    // on Click Selected Sub Cabang
    $('#BtnRemoveSelected').click(function(ev){
        alertify.confirm("Are you sure you want to remove selected items?", function () {
            $('#FormMain').attr("action","{{ URL::to('dealers/remove') }}");
            // $('#FormMain').attr("method","post");
            $('#FormMain').submit();
        });
    });



    //
    // Table Modal
    // ==========================================================================
    
    var $TableModal = {};

    // click Add Sub Cabang
    $('#BtnAddDealer').click(function(ev){
        ev.preventDefault();

        // load Data Sub Cabang
        $TableModal = $('#TableAllDealer').DataTable({
            columnDefs: [
                // {
                //     "targets": [ 1 ],
                //     "visible": false,
                //     "searchable": false
                // },
                {
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="ids[]" value="'+full['id']+'">';
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            order: [[1, 'asc']],
            processing: true,
            serverSide: true,
            ajax: "{{ URL::to('ajax/dealers?origin=editsubcabang&id='.$subcabang->id) }}",
            columns: [
                { data: '', name: '' },
                { data: 'id', name: 'subcabangs.id' },
                { data: 'name', name: 'subcabangs.name' },
                { data: 'subcabang', name: 'subcabangs.name' },
            ]
        });

    });

    // show Modal after finish
    $('#TableAllDealer').on( 'draw.dt', function () {
        $('#ModalAddDealer').modal('show');

        // Handle click on heading containing "Select all" control
        $('thead', $TableModal.table().container()).on('click', 'th:first-child', function(e) {
            $('input[type="checkbox"]', this).trigger('click');
        });
    });

    // on Modal hidden
    $('#ModalAddDealer').on('hidden.bs.modal', function (e) {
        $TableModal.destroy();
        $TableModal = {};
    });

    // Handle row selection event
    $('#TableAllDealer').on('select.dt deselect.dt', function(e, api, type, items) {
        if (e.type === 'select') {
        $('tr.selected input[type="checkbox"]', api.table().container()).prop('checked', true);
        } else {
        $('tr:not(.selected) input[type="checkbox"]', api.table().container()).prop('checked', false);
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModal);
        checkCheckedCheckboxsModal();      
    });

    // Handle click on "Select all" control
    $('#TableAllDealer thead').on('click', 'input[type="checkbox"]', function(e) {
        if (this.checked) {
        $TableModal.rows({ page: 'current' }).select();        
        } else {
        $TableModal.rows({ page: 'current' }).deselect();
        }

        e.stopPropagation();
    });

    // Handle table draw event
    $('#TableAllDealer').on('draw.dt', function() {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl($TableModal);
        checkCheckedCheckboxsModal();
    });

    // If there any checked checkboxs inside modal
    var checkCheckedCheckboxsModal = function(){
        if ($('#TableAllDealer tbody').find('input[type="checkbox"]:checked').length == 0) {
            $('#BtnSubmitModal').attr('disabled','disabled');
        } else {
            $('#BtnSubmitModal').removeAttr('disabled');
        }
    }




});


//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {
   var $table = table.table().container();
   var $chkbox_all = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all = $('thead input[type="checkbox"]', $table).get(0);

   // If none of the checkboxes are checked
   if ($chkbox_checked.length === 0) {
      chkbox_select_all.checked = false;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length) {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if ('indeterminate' in chkbox_select_all) {
         chkbox_select_all.indeterminate = true;
      }
   }
}
</script>
@stop