@extends('master')

@section('header')
    <section class="content-header">
        <h1>Sub Cabang <small>Create</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('cabang')}}">Cabang</a></li>
            <li><a href="{{ URL::to('subcabang')}}">Sub Cabang</a></li>
            <li class="active">Create</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<div class="box box-primary">
        		<!-- form start -->
        		{{ Form::open(array('url' => 'subcabang' ) ) }}
        			<div class="box-body">
        				<div class="row">
		        			<div class="col-md-6">
		        				<div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
		        					{{ Form::label('name', 'Name *') }}
		        					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Sub cabang name')) }}
		        					<p class="help-block">{{ $errors->first('name') }}</p>
		        				</div>	
		        			</div>
                            @if (empty($_GET['org']))
    		        			<div class="col-md-6">
    		        				<div class="form-group{{ $errors->has('cabang_id') ? ' has-error' : null }}">
    		        					{{ Form::label('cabang_id', 'Cabang *') }}
    		        					{{ Form::select('cabang_id', ([null => 'Select cabang'] + $cabangs), null, array('class' => 'form-control')) }}
    		        					<p class="help-block">{{ $errors->first('cabang_id') }}</p>
    		        				</div>
    		        			</div>
                            @endif
		        		</div>
        			</div>
        			<!-- /.box-body -->

        			<div class="box-footer">
        				{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}&nbsp;
                        @if (!empty($_GET['org']))
                            {{ Form::hidden('cabang_id', $_GET['cabid'] ) }}
                            {{ Form::hidden('route_origin', $_GET['org'] ) }}
                            {{ Form::hidden('id_origin', $_GET['cabid'] ) }}
                            <a href="{{ URL::to('cabang/'.$_GET['cabid'])}}" class="btn btn-default">Back to cabang</a>
                        @else
                            <a href="{{ URL::to('subcabang')}}" class="btn btn-default">Cancel</a>
                        @endif
        			</div>
        		{{ Form::close() }}
        	</div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop