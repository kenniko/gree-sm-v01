@extends('master')

@section('css')
    <!-- selectize.js -->
    <link rel="stylesheet" href="{{ URL::to('css/selectize.bootstrap3.css') }}">
    <!-- datepicker3 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('header')
    <section class="content-header">
        <h1>Sell Out <small>Edit</small></h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('/')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ URL::to('sellout')}}">Sell Out</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- form start -->
                {{ Form::model($sellout, ['id'=>'FormMain', 'method' => 'PATCH', 'route' => ['sellout.update', $sellout->id]]) }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : null }}">
                                    {{ Form::label('type', 'Type *') }}
                                    @if(Input::old('type') && Input::old('type') != 'Retail' && Input::old('type') != 'Project' )
                                        {{Form::select('type', ([null => 'Select type', 'Retail' => 'Retail', 'Project'=>'Project', Input::old('type') => Input::old('type')]), null, ['id'=>'SelectType', 'class'=>'form-control']);}}
                                    @elseif($sellout->type != 'Retail' && $sellout->type != 'Project')
                                        {{Form::select('type', ([null => 'Select type', 'Retail' => 'Retail', 'Project'=>'Project', $sellout->type => $sellout->type]), null, ['id'=>'SelectType', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('type', ([null => 'Select type', 'Retail' => 'Retail', 'Project'=>'Project']), null, ['id'=>'SelectType', 'class'=>'form-control']);}}
                                    @endif
                                    <p class="help-block">{{ $errors->first('type') }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('datetime') ? ' has-error' : null }}">
                                    {{ Form::label('datetime', 'Created *') }}
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {{ Form::text('datetime', null, ['id'=>'DateTimePicker', 'class' => 'form-control', 'placeholder' => 'Created at']) }}
                                    </div>
                                    <p class="help-block">{{ $errors->first('datetime') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('cabang_id') ? ' has-error' : null }}">
                                    {{ Form::label('cabang_id', 'Cabang *') }}
                                    {{Form::select('cabang_id', ([null => 'Select cabang'] + $cabangs), null, ['id'=>'SelectCabang', 'class'=>'form-control']);}}
                                    <p class="help-block">{{ $errors->first('cabang_id') }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('subcabang_id') ? ' has-error' : null }}">
                                    {{ Form::label('subcabang_id', 'Sub Cabang *') }}
                                    @if(Input::old('subcabangs'))
                                        {{Form::select('subcabang_id', ([null => 'Select sub cabang'] + Input::old('subcabangs')), Input::old('subcabangs'), ['id'=>'SelectSubcabang', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('subcabang_id', ([null => 'Select sub cabang'] + $subcabangs), null, ['id'=>'SelectSubcabang', 'class'=>'form-control']);}}
                                    @endif
                                    <p class="help-block">{{ $errors->first('subcabang_id') }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('dealer_id') ? ' has-error' : null }}">
                                    {{ Form::label('dealer_id', 'Dealer *') }}
                                    @if(Input::old('dealers'))
                                        {{Form::select('dealer_id', ([null => 'Select dealer'] + Input::old('dealers')), Input::old('dealers'), ['id'=>'SelectDealer', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('dealer_id', ([null => 'Select dealer'] + $dealers), null, ['id'=>'SelectDealer', 'class'=>'form-control']);}}
                                    @endif
                                    <p class="help-block">{{ $errors->first('dealer_id') }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('sales_id') ? ' has-error' : null }}">
                                    {{ Form::label('sales_id', 'Sales *') }}
                                    @if(Input::old('sales'))
                                        {{Form::select('sales_id', ([null => 'Select sales'] + Input::old('sales')), Input::old('sales'), ['id'=>'SelectSales', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('sales_id', ([null => 'Select sales'] + $sales), null, ['id'=>'SelectSales', 'class'=>'form-control']);}}
                                    @endif
                                    <p class="help-block">{{ $errors->first('sales_id') }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('spm_id') ? ' has-error' : null }}">
                                    {{ Form::label('spm_id', 'SPM') }}
                                    @if(Input::old('spm'))
                                        {{Form::select('spm_id', ([null => 'Select SPM'] + Input::old('spm')), Input::old('spm'), ['id'=>'SelectSPM', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('spm_id', ([null => 'Select SPM'] + $spm), null, ['id'=>'SelectSPM', 'class'=>'form-control']);}}
                                    @endif
                                    <p class="help-block">{{ $errors->first('spm_id') }}</p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : null }}">
                                    {{ Form::label('name', 'Customer Name *') }}
                                    @if(Input::old('name'))
                                        {{Form::select('name', ([null => 'Search name or phone', Input::old('name') => Input::old('name_real')]), null, ['id'=>'SelectCustomer', 'class'=>'form-control']);}}
                                    @else
                                        {{Form::select('name', ([null => 'Search name or phone', $customer->id => $customer->name]), $customer->id, ['id'=>'SelectCustomer', 'class'=>'form-control']);}}
                                    @endif
                                    {{-- {{ Form::hidden('name', $_GET['subcabid'] ) }} --}}
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : null }}">
                                    {{ Form::label('phone', 'Customer Phone') }}
                                    {{ Form::text('phone', $customer->phone, ['id'=>'CustomerPhone', 'class' => 'form-control', 'placeholder' => 'Phone number']) }}
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- <h4>Product List</h4> -->
                        <table class="table" width="100%">
                            <thead>
                                <tr class="info">
                                    <th width="55"></th>
                                    <!-- <th width="33">#</th> -->
                                    <th width="">Product Name / Code *</th>
                                    <th width="100">Quantity *</th>
                                    <th width="250">Price per item *</th>
                                </tr>
                            </thead>
                            <tbody id="DynamicTable">
                                @if (Input::old('transactions'))
                                    @foreach(Input::old('transactions') as $key => $value)
                                        <tr class="TableItem_1">
                                            {{ Form::hidden('productout_id[]', Input::old('productout_id.'.$key) ) }}
                                            <td>
                                                @if ($key > 0)
                                                <button type="button" class="BtnRemoveProduct btn btn-danger btn-sm"><i class="fa fa-minus fa-lg"></i></button>
                                                @endif
                                            </td>
                                            <td class="SelectProductWrapper">
                                                <div class="form-group{{ $errors->has('product_id.'.$key) ? ' has-error' : null }}">
                                                    {{Form::select('product_id[]', ([null => 'Select product'] + $products), Input::old('product_id.'.$key), ['class'=>'SelectProduct form-control']);}}
                                                </div>
                                            </td>
                                            <td class="QuantityWrapper">
                                                <div class="form-group{{ $errors->has('quantity.'.$key) ? ' has-error' : null }}">
                                                    {{ Form::text('quantity[]', Input::old('quantity.'.$key), ['class' => 'form-control text-right']) }}
                                                    <p class="help-block">{{ $errors->has('quantity.'.$key) ? $errors->first('quantity.'.$key) : null }}</p>
                                                </div>
                                            </td>
                                            <td class="PriceWrapper">
                                                <div class="form-group{{ $errors->has('price.'.$key) ? ' has-error' : null }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Rp</span>
                                                        {{ Form::text('price[]', Input::old('price.'.$key), ['class' => 'PriceInput form-control text-right', 'placeholder' => 'price for one item']) }}
                                                    </div>
                                                    <p class="help-block">{{ $errors->has('price.'.$key) ? $errors->first('price.'.$key) : null }}</p>
                                                </div>
                                            </td>
                                        </tr>  
                                    @endforeach
                                @else
                                    @foreach($productouts as $key => $value)
                                        <tr class="TableItem_1">
                                            {{ Form::hidden('productout_id[]', $value->id ) }}
                                            <td>
                                                @if ($key > 0)
                                                <button type="button" class="BtnRemoveProduct btn btn-danger btn-sm"><i class="fa fa-minus fa-lg"></i></button>
                                                @endif
                                            </td>
                                            <td class="SelectProductWrapper">
                                                <div class="form-group{{ $errors->has('product_id.'.$key) ? ' has-error' : null }}">
                                                    {{Form::select('product_id[]', ([null => 'Select product'] + $products), $value->product_id, ['class'=>'SelectProduct form-control']);}}
                                                </div>
                                            </td>
                                            <td class="QuantityWrapper">
                                                <div class="form-group{{ $errors->has('quantity.'.$key) ? ' has-error' : null }}">
                                                    {{ Form::text('quantity[]', $value->quantity, ['class' => 'form-control text-right']) }}
                                                    <p class="help-block">{{ $errors->has('quantity.'.$key) ? $errors->first('quantity.'.$key) : null }}</p>
                                                </div>
                                            </td>
                                            <td class="PriceWrapper">
                                                <div class="form-group{{ $errors->has('price.'.$key) ? ' has-error' : null }}">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Rp</span>
                                                        {{ Form::text('price[]', $value->price, ['class' => 'PriceInput form-control text-right', 'placeholder' => 'price for one item']) }}
                                                    </div>
                                                    <p class="help-block">{{ $errors->has('price.'.$key) ? $errors->first('price.'.$key) : null }}</p>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr class="BtnAddProductWrapper">
                                    <td colspan="5"><button type="button" id="BtnAddProduct" class="btn btn-block btn-default btn-sm"><i class="fa fa-plus fa-lg"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        @if (getUserRole()->slug == 'spm')
                            @if (getCurrectUser()->dealer()->first()->id == $sellout->dealer_id)
                                {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                                <a href="{{ URL::to('sellout')}}" class="btn btn-default">Cancel</a>
                                <button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
                            @else
                                {{ Form::submit('Update', array('class' => 'btn btn-primary', 'disabled'=>'disabled')) }}&nbsp;
                                <a href="{{ URL::to('sellout')}}" class="btn btn-default">Cancel</a>
                                <button type="button" id="BtnDelete" class="btn btn-danger pull-right" disabled>Delete</button>
                            @endif
                        @else
                            {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}&nbsp;
                                <a href="{{ URL::to('sellout')}}" class="btn btn-default">Cancel</a>
                                <button type="button" id="BtnDelete" class="btn btn-danger pull-right">Delete</button>
                        @endif
                    </div>
                {{ Form::close() }}

                {{ Form::model($sellout, ['id'=>'FormDelete', 'method' => 'DELETE', 'route' => ['sellout.destroy', $sellout->id]]) }}
                {{ Form::close() }}
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('js')
    <!-- selectize.js -->
    <script src="{{ URL::to('js/selectize.standalone.min.js') }}"></script>
    <!-- datepicker3 -->
    <script src="{{ URL::to('js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- maskMoney -->
    <script src="{{ URL::to('js/jquery.maskMoney.min.js') }}"></script>
@stop

@section('scripts')
    <script>
    $(function() {

        // on Delete
        $('#BtnDelete').click(function(ev){
            ev.preventDefault();
            alertify.confirm("Are you sure you want to delete this?", function () {
                $('#FormDelete').submit(); 
            });
        });


        var xhr;
        var SelectizeCabang, $SelectCabang;
        var SelectizeSubcabang, $SelectSubcabang;
        var SelectizeDealer, $SelectDealer;
        var SelectizeCustomer, $SelectCustomer;
        var SelectizeSales, $SelectSales;
        var SelectizeSPM, $SelectSPM;
        var incrementProduct = 1,
            $CustomerPhoneInput = $('#CustomerPhone'),
            CustomerArray = [];

        // Date Time Picker
        $("#DateTimePicker").datetimepicker({
            format: 'DD/MM/YYYY - HH:mm',
            sideBySide: true,
            date: moment("{{$sellout->datetime}}")
        });

        // Selectize
        $('#SelectSales').selectize();
        $('#SelectSPM').selectize();
        $('#SelectType').selectize({ create: true, persist: false });
        $SelectCustomer = $('#SelectCustomer').selectize({ 
            create: true,
            persist: false,
            valueField: 'id',
            labelField: 'name',
            searchField: ['name', 'phone'],
            render: {
                option: function(item, escape) {
                    var name = item.name || item.phone;
                    var phone = item.name ? item.phone : null;
                    return '<div>' +    
                        '<span class="name">' + escape(name) + '</span>' +
                        (phone ? '<span class="caption">' + escape(phone) + '</span>' : '') +
                    '</div>';
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: '{{ URL::to('ajax/selectize?data=customers&query=') }}'+query,
                    type: 'GET',
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        var results = res.slice(0, 10);
                        callback(results);
                        for (var i = results.length - 1; i >= 0; i--) {
                            // console.log(results[i]);
                            CustomerArray[results[i].id] = results[i].phone;
                        }

                    }
                });
            },
            onItemRemove: function() {
                SelectizeCustomer.clearOptions();
                $CustomerPhoneInput.val('');
            },
            onItemAdd: function(value, $item) {
                if ($item.text() !== value) {
                    $CustomerPhoneInput.val(CustomerArray[value]);
                    // xhr && xhr.abort();
                    // xhr = $.ajax({
                    //     url: '{{ URL::to('ajax/get?data=customer&id=') }}'+value,
                    //     type: 'GET',
                    //     success: function(result) {
                    //         // console.log(result.phone);
                    //         $CustomerPhoneInput.val(result.phone);
                    //     },
                    //     error: function() {
                    //         // callback();
                    //     }
                    // })
                }
            },
        });
        $SelectCabang = $('#SelectCabang').selectize({
            onItemRemove: function(value) {
                SelectizeSubcabang.disable();
                SelectizeSubcabang.clearOptions();
                SelectizeDealer.disable();
                SelectizeDealer.clearOptions();
                SelectizeSales.disable();
                SelectizeSales.clearOptions();
                SelectizeSPM.disable();
                SelectizeSPM.clearOptions();
            },
            onChange: function(value) {
                if (!value.length) return;
                SelectizeSubcabang.disable();
                SelectizeSubcabang.clearOptions();
                SelectizeDealer.disable();
                SelectizeDealer.clearOptions();
                SelectizeSales.disable();
                SelectizeSales.clearOptions();
                SelectizeSPM.disable();
                SelectizeSPM.clearOptions();
                SelectizeSubcabang.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ URL::to('ajax/selectize?data=subcabangs&cabang_id=') }}'+value,
                        type: 'GET',
                        success: function(results) {
                            if (results[0]) {
                                SelectizeSubcabang.enable();
                                SelectizeSubcabang.focus();
                                callback(results);
                            } else {
                                setTimeout(function() { 
                                    alertify.logPosition("top right");
                                    alertify.delay(8000);
                                    alertify.closeLogOnClick(true).error("The selected Cabang has no Sub Cabang yet.<br>Pelase insert on Settings / Database / Cabang menu first.");  
                                }, 1);
                            }
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
                SelectizeSales.load(function(callback) {
                    // xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ URL::to('ajax/selectize?data=sales&cabang_id=') }}'+value,
                        type: 'GET',
                        success: function(results) {
                            if (results[0]) {
                                SelectizeSales.enable();
                                // SelectizeSales.focus();
                                callback(results);
                            } else {
                                setTimeout(function() { 
                                    alertify.logPosition("top right");
                                    alertify.delay(8000);
                                    alertify.closeLogOnClick(true).error("The selected Cabang has no Sales Person yet.<br>Pelase insert on Settings / Database / Cabang menu first.");  
                                }, 1);
                            }
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        $SelectSubcabang = $('#SelectSubcabang').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            onItemRemove: function(value) {
                SelectizeDealer.disable();
                SelectizeDealer.clearOptions();
            },
            onChange: function(value) {
                if (!value.length) return;
                SelectizeDealer.disable();
                SelectizeDealer.clearOptions();
                SelectizeDealer.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ URL::to('ajax/selectize?data=dealers&subcabang_id=') }}'+value,
                        type: 'GET',
                        success: function(results) {
                            if (results[0]) {
                                SelectizeDealer.enable();
                                SelectizeDealer.focus();
                                callback(results);
                            } else {
                                setTimeout(function() { 
                                    alertify.logPosition("top right");
                                    alertify.delay(7000);
                                    alertify.closeLogOnClick(true).error("The selected Sub Cabang has no Dealer yet.<br>Pelase insert on Settings / Database / Sub Cabang menu first.");  
                                }, 1);
                            }
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        $SelectDealer = $('#SelectDealer').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            onItemRemove: function(value) {
                SelectizeSPM.disable();
                SelectizeSPM.clearOptions();
            },
            onChange: function(value) {
                if (!value.length) return;
                SelectizeSPM.disable();
                SelectizeSPM.clearOptions();
                SelectizeSPM.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ URL::to('ajax/selectize?data=spm&dealer_id=') }}'+value,
                        type: 'GET',
                        success: function(results) {
                            if (results[0]) {
                                SelectizeSPM.enable();
                                SelectizeSPM.focus();
                                callback(results);
                            } else {
                                setTimeout(function() { 
                                    alertify.logPosition("top right");
                                    alertify.delay(7000);
                                    alertify.closeLogOnClick(true).error("The selected Dealer has no SPM yet.<br>Pelase insert on Setting / Database / Dealer menu first.");  
                                }, 1);
                            }
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        $SelectSales = $('#SelectSales').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name']
        });
        $SelectSPM = $('#SelectSPM').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name']
        });

        SelectizeCabang  = $SelectCabang[0].selectize;
        SelectizeSubcabang = $SelectSubcabang[0].selectize;
        SelectizeDealer = $SelectDealer[0].selectize;
        SelectizeCustomer = $SelectCustomer[0].selectize;
        SelectizeSales = $SelectSales[0].selectize;
        SelectizeSPM = $SelectSPM[0].selectize;

        //
        // Table Product
        // ==========================================================================
            
            var $SelectProductWrapper = '<td class="SelectProductWrapper"><div class="form-group">{{Form::select("product_id[]", ([null => "Select product"] + $products), null, ["class"=>"SelectProduct form-control"]);}}</div></td>',

                $QuantityWrapper = '<td class="QuantityWrapper"><div class="form-group">{{ Form::text("quantity[]", null, ["class" => "form-control text-right"]) }}</div></td>',

                $PriceWrapper = '<td class="PriceWrapper"><div class="form-group"><div class="input-group"><span class="input-group-addon">Rp</span>{{ Form::text("price[]", null, ["class" => "PriceInput form-control text-right", "placeholder"=>"price for one item"]) }}</div></div></td>';
        
            // selectize product
            // $('.TableItem_1 .SelectProduct').selectize();
            $('.SelectProduct').selectize();

            // maskMoney
            // $('.TableItem_1 .PriceInput').maskMoney({
            $('.PriceInput').maskMoney({
                thousands: '.',
                precision: 0
            });

            // dynamic input
            var $BtnAddProductWrapper = $(".BtnAddProductWrapper");
            $('#BtnAddProduct').click(function(ev){
                ev.preventDefault();
                incrementProduct++;
                // $(TableWrapper).clone().appendTo('#DynamicTable');
                var NewTableItem = '<tr class="TableItem_'+incrementProduct+'">';
                NewTableItem += '<td><button type="button" class="BtnRemoveProduct btn btn-danger btn-sm"><i class="fa fa-minus fa-lg"></i></button></td>';
                NewTableItem += $SelectProductWrapper;
                NewTableItem += $QuantityWrapper;
                NewTableItem += $PriceWrapper;
                $BtnAddProductWrapper.before(NewTableItem);

                // selectize product
                $('.TableItem_'+incrementProduct+' .SelectProduct').selectize();

                // maskMoney
                $('.TableItem_'+incrementProduct+' .PriceInput').maskMoney({
                    thousands: '.',
                    precision: 0
                });
            });
            $("#DynamicTable").on("click",".BtnRemoveProduct",function(ev){
                ev.preventDefault();
                var $Product = $(this);
                alertify.confirm("Are you sure you want to delete this?", function () {
                    $Product.parent().parent().remove();
                });
            });

    });
    </script>
@stop