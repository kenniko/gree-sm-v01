<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class RolesController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.role');
    }

	/**
	 * Display a listing of dealers
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'adminpusat') {
			$roles = Sentinel::getRoleRepository()->createModel()->whereNotIn('slug', ['superadmin', 'adminpusat'])->get();
		} else {
			$roles = Sentinel::getRoleRepository()->createModel()->get();
		}

		return View::make('roles.index', compact('roles'));
	}

	/**
	 * Show the form for creating a new dealer
	 *
	 * @return Response
	 */
	public function create()
	{
		return Response::view('404');
	}

	/**
	 * Store a newly created dealer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Response::view('404');
	}

	/**
	 * Display the specified dealer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$role = Sentinel::getRoleRepository()->createModel()->findOrFail($id);

		 	$role['permissions'] = array_keys($role->permissions);

			return View::make('roles.edit', compact('role'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('role.index');
		}

	}

	/**
	 * Show the form for editing the specified dealer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified dealer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$role = Sentinel::getRoleRepository()->createModel()->findOrFail($id);

		$rules = [
			'name' => 'required',
	        'slug' => 'required',
	        'permissions' => 'required'
        ];

        // echo json_encode($data);

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$permissions = [];

		foreach ($data['permissions'] as $permission) {
			$permissions[$permission] = true;	
		}
		$data['permissions'] = $permissions;
		
		$role->update($data);

		return Redirect::route('role.index')->withSuccess('Role '.Input::get('name').' has been updated successfully');
	}

	/**
	 * Remove the specified dealer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Response::view('404');
	}

}
