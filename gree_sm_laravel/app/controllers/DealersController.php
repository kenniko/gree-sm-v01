<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class DealersController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.dealer');
    }
	/**
	 * Display a listing of dealers
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'cabang') {
			$dealers = Dealer::leftJoin('subcabangs', 'subcabangs.id', '=', 'dealers.subcabang_id')
						->where('subcabangs.cabang_id', $user->cabang->id)
						->select('*', 'dealers.id AS id', 'dealers.name AS name')
						->distinct()->get();
		} else {
			$dealers = Dealer::all();
		}

		return View::make('dealers.index', compact('dealers'));
	}

	/**
	 * Show the form for creating a new dealer
	 *
	 * @return Response
	 */
	public function create()
	{
		$subcabangs = Subcabang::lists('name', 'id');
		
		return View::make('dealers.create', compact('subcabangs'));
	}

	/**
	 * Store a newly created dealer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Dealer::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$dealer = Dealer::create($data);

		if (Input::has('id_origin')) {
			return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
					->withSuccess('Dealer '.Input::get('name').' has been created successfully.');
		} else {
			// return Redirect::to(Input::get('route_origin', 'subcabang').'/'.Input::get('id_origin', $subcabang->id))
			// 			->with(compact('subcabang'))
			// 			->withSuccess('Subcabang '.Input::get('name').' has been created successfully.');
			return Redirect::to('dealer')->withSuccess('Dealer '.Input::get('name').' has been created successfully.');
		}
	}

	/**
	 * Display the specified dealer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$dealer = Dealer::findOrFail($id);

			$subcabangs = Subcabang::lists('name', 'id');

			return View::make('dealers.edit', compact('subcabangs', 'dealer'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('dealer.index');
		}

	}

	/**
	 * Show the form for editing the specified dealer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified dealer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$dealer = Dealer::findOrFail($id);

		$data = Input::all();

		$sellins = Sellin::where('dealer_id', $id)->count();
		$sellouts = Sellout::where('dealer_id', $id)->count();

		// check if this data have relation with others data
		if ( ($sellins > 0 || $sellouts > 0) && $dealer['subcabang_id'] != $data['subcabang_id'] ) {
			return Redirect::back()->withErrors(['subcabang_id'=>'Can not perform update subcabang.<br>This dealer has included in some transactions.']);

		// if this data free to go
		} else {

			$rules = Dealer::$rules;

			if ($data['name'] == $dealer['name']) {
				unset($rules['name']);
			}
			if ($data['id'] == $dealer['id']) {
				unset($rules['id']);
			}

			$validator = Validator::make($data, $rules);

			if ($validator->fails())
			{
				return Redirect::back()->withErrors($validator)->withInput();
			}

			// $subcabang = Subcabang::where('id', $data['subcabang_id'])->first();
			// DB::table('sellins')
	  //           ->where('dealer_id', $id)
	  //           ->update([
	  //           	'subcabang_id' => $subcabang->id,
	  //           	'cabang_id' => $subcabang->cabang_id
	  //           ]);
	  //       DB::table('sellouts')
	  //           ->where('dealer_id', $id)
	  //           ->update([
	  //           	'subcabang_id' => $subcabang->id,
	  //           	'cabang_id' => $subcabang->cabang_id
	  //           ]);

			$dealer->update($data);

			if (!empty(Input::get('route_origin'))) {
				return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
						->withSuccess('Dealer '.Input::get('name').' has been updated successfully.');
			} else {
				return Redirect::route('dealer.index')->withSuccess('Dealer '.Input::get('name').' has been updated successfully.');
			}
		}

	}
	public function updateSelected()
	{
		$sellins = Sellin::whereIn('dealer_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('dealer_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform add.<br>The selected dealers has included in some transactions.']);

		// if this data free to go
		} else {

			$subcabangID = Input::get('subcabang_id');

	    	Dealer::whereIn('id', Input::get('ids') )->update(array('subcabang_id' => $subcabangID));

			return Redirect::to('subcabang/'.$subcabangID)->withSuccess('Selected Dealer has been updated successfully.');
		}
	}
	public function removeSelected()
	{
		$sellins = Sellin::whereIn('dealer_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('dealer_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform remove.<br>The selected dealers has included in some transactions.']);

		// if this data free to go
		} else {

			$subcabangID = Input::get('subcabang_id');
			
	    	Dealer::whereIn('id', Input::get('ids') )->update(array('subcabang_id' => null));

			return Redirect::to('subcabang/'.$subcabangID)->withSuccess('Selected Dealer has been remove successfully.');
		}
	}

	/**
	 * Remove the specified dealer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sellins = Sellin::where('dealer_id', $id)->count();
		$sellouts = Sellout::where('dealer_id', $id)->count();
		$spms = User::where('dealer_id', $id)->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>This dealer has included in some transactions.']);
		} elseif ($spms > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. This dealer has SPM.<br>Please update SPM that have relation with this dealer first.']);

		// if this data free to go
		} else {
			Dealer::destroy($id);

			if (!empty(Input::get('route_origin'))) {
				return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
						->withSuccess('Dealer '.Input::get('name').' has been deleted successfully.');
			} else {
				return Redirect::route('dealer.index')->withSuccess('Dealer '.Input::get('name').' has been deleted successfully.');
			}
		}
	}
	public function destroySelected()
	{
		$sellins = Sellin::whereIn('dealer_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('dealer_id', Input::get('ids'))->count();
		$spms = User::whereIn('dealer_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0 || $spms > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>Some dealers have included in some transactions or already have SPM.']);

		// if this data free to go
		} else {
			Dealer::destroy(Input::get('ids'));
			return Redirect::route('dealer.index')->withSuccess('Selected Dealer has been deleted successfully.');
		}
	}

}
