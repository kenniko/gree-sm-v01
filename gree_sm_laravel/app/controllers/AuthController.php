<?php

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;

class AuthController extends BaseController {

	//
	// show Login Page
	// ==========================================================================
		public function index() 
		{
			return View::make('login');
		}

	//
	// process Login
	// ==========================================================================
		public function store() 
		{
			try {
				$input = Input::all();

				$rules = [
					'login'    => 'required',
					'password' => 'required',
				];

				$validator = Validator::make($input, $rules);

				if ($validator->fails()) {
					return Redirect::back()
						->withInput()
						->withErrors($validator);
				}

				$remember = (bool) Input::get('remember', false);

				if (Sentinel::authenticate(Input::all(), $remember)) {
					return Redirect::intended('/');
				}

				$errors = 'Invalid login or password.';
			}
			// catch (NotActivatedException $e) {
			// 	$errors = 'Account is not activated!';

			// 	return Redirect::to('reactivate')->with('user', $e->getUser());
			// }
			catch (ThrottlingException $e) {
				$delay = $e->getDelay();

				$errors = "Your account is blocked for {$delay} second(s).";
			}

			return Redirect::back()
				->withInput()
				->withErrors($errors);
		}

	//
	// process Logout
	// ==========================================================================	
		public function destroy() 
		{
			Sentinel::logout();
			return Redirect::to('/login');
		}

}
