<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CabangsController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.cabang', ['except' => ['showReport', 'export', 'showRangking', 'editRangking'] ]);
        $this->beforeFilter('auth.report', ['only' => ['showReport', 'export'] ]);
        $this->beforeFilter('auth.rangking', ['only' => ['showRangking', 'editRangking'] ]);
    }
    
	/**
	 * Display a listing of cabangs
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'cabang') {
			// $cabangs = Cabang::find($user->cabang->id);
			$cabangs = Cabang::where('name', $user->cabang->name)->get();
		} else {
			$cabangs = Cabang::all();
		}

		return View::make('cabangs.index', compact('cabangs'));
	}

	public function indexRangking()
	{
		$cabangs = Cabang::all();

		return View::make('rangkings.index', compact('cabangs'));
	}

	/**
	 * Show the form for creating a new cabang
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('cabangs.create');
	}

	/**
	 * Store a newly created cabang in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Cabang::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$cabang = Cabang::create($data);

		// return Redirect::to('cabang/'.$cabang->id)->with(compact('cabang'))->withSuccess('Cabang '.Input::get('name').' has been created successfully');
		return Redirect::to('cabang')->withSuccess('Cabang '.Input::get('name').' has been created successfully');
	}

	/**
	 * Display the specified cabang.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$cabang = Cabang::find($id);

		 	$salesRole = Sentinel::findRoleBySlug('sales');
			$sales = $salesRole->users()->where('cabang_id', $cabang->id)->get();
		 	
		 	return View::make('cabangs.edit', compact('cabang', 'sales'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('cabang.index');
		}
	}

	public function showRangking($id)
	{
		try 
		{
		 	$cabang = Cabang::find($id);
		 	
		 	return View::make('rangkings.edit', compact('cabang'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('rangking.index');
		}
	}

	/**
	 * Show the form for editing the specified cabang.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');

		// $cabang = Cabang::find($id);

		// return View::make('cabangs.edit', compact('cabang'));
	}

	/**
	 * Update the specified cabang in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cabang = Cabang::findOrFail($id);

		$data = Input::all();

		$rules = Cabang::$rules;

		if($data['name'] == $cabang->name) {
			unset($rules['name']);
		}

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$cabang->update($data);

		return Redirect::route('cabang.index')->withSuccess('Cabang '.Input::get('name').' has been updated successfully');
	}

	public function updateRangking($id)
	{
		$cabang = Cabang::findOrFail($id);

		$data = Input::all();

		$rules['percentage'] = 'required|numeric|min:1|max:100';

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$cabang->percentage = $data['percentage'];
		$cabang->save();

		return Redirect::route('rangking.index')->withSuccess('Rangking Cabang '.Input::get('name').' has been updated successfully');
	}

	/**
	 * Remove the specified cabang from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sellins = Sellin::where('cabang_id', $id)->count();
		$sellouts = Sellout::where('cabang_id', $id)->count();
		$sales = User::where('cabang_id', $id)->count();
		$subcabang = Subcabang::where('cabang_id', $id)->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>This Cabang has included in some transactions.']);
		} elseif ($subcabang > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. This Cabang has Sub Cabang.<br>Please update Sub Cabang '.$subcabang->name.' first.']);
		} elseif ($sales > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. This Cabang has Sales.<br>Please update Sales that have relation with this Cabang first.']);

		// if this data free to go
		} else {
			Cabang::destroy($id);

			return Redirect::route('cabang.index')->withSuccess('Cabang '.Input::get('name').' has been deleted successfully');
		}
	}
	public function destroySelected()
	{
		$sellins = Sellin::whereIn('cabang_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('cabang_id', Input::get('ids'))->count();
		$sales = User::whereIn('cabang_id', Input::get('ids'))->count();
		$subcabangs = Subcabang::whereIn('cabang_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0 || $sales > 0 || $subcabangs > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>Some cabangs have included in some transactions or already have Sub Cabang and Sales.']);

		// if this data free to go
		} else {

			Cabang::destroy(Input::get('ids'));

			return Redirect::route('cabang.index')->withSuccess('Selected Cabang has been deleted successfully');
		}
	}

	public function showReport()
	{

		// $reports = Cabang::leftJoin('sellins', 'sellins.cabang_id', '=', 'cabangs.id')
		// 			->leftJoin('sellouts', 'sellouts.cabang_id', '=', 'cabangs.id')
		// 			->leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
		// 			->leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
		// 			->select(
		// 				'cabangs.id',
		// 				'cabangs.name',
		// 				DB::raw('sum(productins.quantity*productins.price) AS total_price_sellin_hari'),
		// 				DB::raw('sum(productins.quantity) AS total_qty_sellin_hari'),
		// 				DB::raw('sum(productouts.quantity*productouts.price) AS total_price_sellout_hari'),
		// 				DB::raw('sum(productouts.quantity) AS total_qty_sellout_hari')
		// 			)->groupBy('cabangs.id')->get();

		$cabangs = Cabang::get(['id','name','percentage']);

		foreach ($cabangs as &$cabang) {

			//
			// Sell In Price Hari
			// ==========================================================================
				$sum_price_hari = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->where('sellins.cabang_id', '=', $cabang->id )
					->whereDay('sellins.datetime', '=', date('d') )
					->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
					// ->groupBy('sellins.id')
					->first();
				if (isset($sum_price_hari->total_price)) {
					$cabang->total_price_sellin_hari = 'Rp ' . number_format($sum_price_hari->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellin_hari = '-';
				}

			//
			// Sell In Price Bulan
			// ==========================================================================
				$sum_price_bulan = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->where('sellins.cabang_id', '=', $cabang->id )
					->whereMonth('sellins.datetime', '=', date('m') )
					->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
					// ->groupBy('sellins.id')
					->first();
				if (isset($sum_price_bulan->total_price)) {
					$cabang->total_price_sellin_bulan = 'Rp ' . number_format($sum_price_bulan->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellin_bulan = '-';
				}

			//
			// Sell In Price Tahun
			// ==========================================================================
				$sum_price_tahun = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->where('sellins.cabang_id', '=', $cabang->id )
					->whereYear('sellins.datetime', '=', date('Y') )
					->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
					// ->groupBy('sellins.id')
					->first();
				if (isset($sum_price_tahun->total_price)) {
					$cabang->total_price_sellin_tahun = 'Rp ' . number_format($sum_price_tahun->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellin_tahun = '-';
				}

			//
			// Sell In Quantity
			// ==========================================================================
				$sum_quantity_hari = $cabang->productins()
					->whereDay('sellins.datetime', '=', date('d') )
					// ->groupBy('sellins.id')
					->sum('quantity');
				if ($sum_quantity_hari > 0) {
					$cabang->total_quantity_sellin_hari = $sum_quantity_hari;
				} else {
					$cabang->total_quantity_sellin_hari = '-';
				}
				$sum_quantity_bulan = $cabang->productins()
					->whereMonth('sellins.datetime', '=', date('m') )
					// ->groupBy('sellins.id')
					->sum('quantity');
				if ($sum_quantity_bulan > 0) {
					$cabang->total_quantity_sellin_bulan = $sum_quantity_bulan;
				} else {
					$cabang->total_quantity_sellin_bulan = '-';
				}
				$sum_quantity_tahun = $cabang->productins()
					->whereYear('sellins.datetime', '=', date('Y') )
					// ->groupBy('sellins.id')
					->sum('quantity');
				if ($sum_quantity_tahun > 0) {
					$cabang->total_quantity_sellin_tahun = $sum_quantity_tahun;
				} else {
					$cabang->total_quantity_sellin_tahun = '-';
				}


			//
			// Sell Out Price Hari
			// ==========================================================================
				$sum_price_hari = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->where('sellouts.cabang_id', '=', $cabang->id )
					->whereDay('sellouts.datetime', '=', date('d') )
					->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
					// ->groupBy('sellouts.id')
					->first();
				if (isset($sum_price_hari->total_price)) {
					$cabang->total_price_sellout_hari = 'Rp ' . number_format($sum_price_hari->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellout_hari = '-';
				}

			//
			// Sell Out Price Bulan
			// ==========================================================================
				$sum_price_bulan = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->where('sellouts.cabang_id', '=', $cabang->id )
					->whereMonth('sellouts.datetime', '=', date('m') )
					->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
					// ->groupBy('sellouts.id')
					->first();
				if (isset($sum_price_bulan->total_price)) {
					$cabang->total_price_sellout_bulan = 'Rp ' . number_format($sum_price_bulan->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellout_bulan = '-';
				}

			//
			// Sell Out Price Tahun
			// ==========================================================================
				$sum_price_tahun = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->where('sellouts.cabang_id', '=', $cabang->id )
					->whereYear('sellouts.datetime', '=', date('Y') )
					->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
					// ->groupBy('sellouts.id')
					->first();
				if (isset($sum_price_tahun->total_price)) {
					$cabang->total_price_sellout_tahun = 'Rp ' . number_format($sum_price_tahun->total_price, 0, '', '.');
				} else {
					$cabang->total_price_sellout_tahun = '-';
				}

			//
			// Sell Out Quantity
			// ==========================================================================
				$sum_quantity_hari = $cabang->productouts()
					->whereDay('sellouts.datetime', '=', date('d') )
					// ->groupBy('sellouts.id')
					->sum('quantity');
				if ($sum_quantity_hari > 0) {
					$cabang->total_quantity_sellout_hari = $sum_quantity_hari;
				} else {
					$cabang->total_quantity_sellout_hari = '-';
				}
				$sum_quantity_bulan = $cabang->productouts()
					->whereMonth('sellouts.datetime', '=', date('m') )
					// ->groupBy('sellouts.id')
					->sum('quantity');
				if ($sum_quantity_bulan > 0) {
					$cabang->total_quantity_sellout_bulan = $sum_quantity_bulan;
				} else {
					$cabang->total_quantity_sellout_bulan = '-';
				}
				$sum_quantity_tahun = $cabang->productouts()
					->whereYear('sellouts.datetime', '=', date('Y') )
					// ->groupBy('sellouts.id')
					->sum('quantity');
				if ($sum_quantity_tahun > 0) {
					$cabang->total_quantity_sellout_tahun = $sum_quantity_tahun;
				} else {
					$cabang->total_quantity_sellout_tahun = '-';
				}

		}

		//
		// Rangking
		// ==========================================================================
		$rangkings = $cabangs;

		foreach ($rangkings as &$rangking) {
			$total_sellout_cabang_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
										->where('sellouts.cabang_id', '=', $rangking->id)
										->whereMonth('sellouts.datetime', '=', date('m') )
										->sum('productouts.price');
			$total_sellout_all_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
										->whereMonth('sellouts.datetime', '=', date('m') )
										->sum('productouts.price');
			$total_sellout_cabang_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
										->where('sellouts.cabang_id', '=', $rangking->id)
										->whereYear('sellouts.datetime', '=', date('Y') )
										->sum('productouts.price');
			$total_sellout_all_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
										->whereYear('sellouts.datetime', '=', date('Y') )
										->sum('productouts.price');
			$point_bulan = ($total_sellout_all_bulan > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_bulan) / ($total_sellout_all_bulan * $rangking->percentage / 100) : '-';
			$point_tahun = ($total_sellout_all_tahun > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_tahun) / ($total_sellout_all_tahun * $rangking->percentage / 100) : '-';

			$rangking['point_bulan'] = $point_bulan;
			$rangking['rangking_bulan'] = $total_sellout_all_bulan;
			$rangking['point_tahun'] = $point_tahun;
			$rangking['rangking_tahun'] = $total_sellout_all_tahun;
		}

		foreach ($rangkings as &$rangking) {
			$rangking_bulan = ($total_sellout_all_bulan == 0) ? 0 : 1;
			$rangking_tahun = ($total_sellout_all_tahun == 0) ? 0 : 1;
			for ($i=0; $i < count($rangkings); $i++) { 
				if ($rangking->point_bulan < $rangkings[$i]->point_bulan) {
					$rangking_bulan++;
				}
			}
			for ($i=0; $i < count($rangkings); $i++) { 
				if ($rangking->point_tahun < $rangkings[$i]->point_tahun) {
					$rangking_tahun++;
				}
			}
			$rangking['rangking_bulan'] = $rangking_bulan;
			$rangking['rangking_tahun'] = $rangking_tahun;	
		}

		//
		// Grand TOTAL
		// ==========================================================================
			// $grand_total_price_sellin_hari = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereDay('sellins.datetime', '=', date('d') )
			// 	->select(DB::raw('sum(productins.quantity*productins.price) AS total_price') )
			// 	->groupBy('sellins.id')->get()->sum('total_price');
			// $grand_total_price_sellin_bulan = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereMonth('sellins.datetime', '=', date('m') )
			// 	->select(DB::raw('sum(productins.quantity*productins.price) AS total_price') )
			// 	->groupBy('sellins.id')->get()->sum('total_price');
			// $grand_total_price_sellin_tahun = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereYear('sellins.datetime', '=', date('Y') )
			// 	->select(DB::raw('sum(productins.quantity*productins.price) AS total_price') )
			// 	->groupBy('sellins.id')->get()->sum('total_price');
			// $grand_total_price_sellout_hari = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereDay('sellouts.datetime', '=', date('d') )
			// 	->select(DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
			// 	->groupBy('sellouts.id')->get()->sum('total_price');
			// $grand_total_price_sellout_bulan = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereMonth('sellouts.datetime', '=', date('m') )
			// 	->select(DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
			// 	->groupBy('sellouts.id')->get()->sum('total_price');
			// $grand_total_price_sellout_tahun = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereYear('sellouts.datetime', '=', date('Y') )
			// 	->select(DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
			// 	->groupBy('sellouts.id')->get()->sum('total_price');

			// $grand_total_quantity_sellin_hari = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereDay('sellins.datetime', '=', date('d') )
			// 	->select(DB::raw('sum(productins.quantity) AS total_quantity') )
			// 	->groupBy('sellins.id')->get()->sum('total_quantity');
			// $grand_total_quantity_sellin_bulan = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereMonth('sellins.datetime', '=', date('m') )
			// 	->select(DB::raw('sum(productins.quantity) AS total_quantity') )
			// 	->groupBy('sellins.id')->get()->sum('total_quantity');
			// $grand_total_quantity_sellin_tahun = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
			// 	->whereYear('sellins.datetime', '=', date('Y') )
			// 	->select(DB::raw('sum(productins.quantity) AS total_quantity') )
			// 	->groupBy('sellins.id')->get()->sum('total_quantity');
			// $grand_total_quantity_sellout_hari = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereDay('sellouts.datetime', '=', date('d') )
			// 	->select(DB::raw('sum(productouts.quantity) AS total_quantity') )
			// 	->groupBy('sellouts.id')->get()->sum('total_quantity');
			// $grand_total_quantity_sellout_bulan = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereMonth('sellouts.datetime', '=', date('m') )
			// 	->select(DB::raw('sum(productouts.quantity) AS total_quantity') )
			// 	->groupBy('sellouts.id')->get()->sum('total_quantity');
			// $grand_total_quantity_sellout_tahun = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
			// 	->whereYear('sellouts.datetime', '=', date('Y') )
			// 	->select(DB::raw('sum(productouts.quantity) AS total_quantity') )
			// 	->groupBy('sellouts.id')->get()->sum('total_quantity');


		// return json_encode($cabangs);

		return View::make('cabangs.report', compact(
			'cabangs',
			'rangkings'
			// 'grand_total_price_sellin_hari',
			// 'grand_total_price_sellin_bulan',
			// 'grand_total_price_sellin_tahun',
			// 'grand_total_price_sellout_hari',
			// 'grand_total_price_sellout_bulan',
			// 'grand_total_price_sellout_tahun',
			// 'grand_total_quantity_sellin_hari',
			// 'grand_total_quantity_sellin_bulan',
			// 'grand_total_quantity_sellin_tahun',
			// 'grand_total_quantity_sellout_hari',
			// 'grand_total_quantity_sellout_bulan',
			// 'grand_total_quantity_sellout_tahun'
		));
	}


	public function exportAll(){

		Excel::create('PT GREE SALES REPORT ('.date("Y-m-d").') h'.date('H').'m'.date('i'), function($excel) {

			// ==========================================================================
			// Sales Report
			// ==========================================================================
				$excel->sheet('Sales Report', function($sheet) {

					$cabangs = Cabang::get(['id','name']);

					foreach ($cabangs as &$cabang) {

						//
						// Sell In Price Hari
						// ==========================================================================
							$sum_price_hari = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
								->where('sellins.cabang_id', '=', $cabang->id )
								->whereDay('sellins.datetime', '=', date('d') )
								->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
								// ->groupBy('sellins.id')
								->first();
							if (isset($sum_price_hari->total_price)) {
								$cabang->total_price_sellin_hari = $sum_price_hari->total_price;
							} else {
								$cabang->total_price_sellin_hari = '-';
							}

						//
						// Sell In Price Bulan
						// ==========================================================================
							$sum_price_bulan = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
								->where('sellins.cabang_id', '=', $cabang->id )
								->whereMonth('sellins.datetime', '=', date('m') )
								->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
								// ->groupBy('sellins.id')
								->first();
							if (isset($sum_price_bulan->total_price)) {
								$cabang->total_price_sellin_bulan = $sum_price_bulan->total_price;
							} else {
								$cabang->total_price_sellin_bulan = '-';
							}

						//
						// Sell In Price Tahun
						// ==========================================================================
							$sum_price_tahun = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
								->where('sellins.cabang_id', '=', $cabang->id )
								->whereYear('sellins.datetime', '=', date('Y') )
								->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
								// ->groupBy('sellins.id')
								->first();
							if (isset($sum_price_tahun->total_price)) {
								$cabang->total_price_sellin_tahun = $sum_price_tahun->total_price;
							} else {
								$cabang->total_price_sellin_tahun = '-';
							}

						//
						// Sell In Quantity
						// ==========================================================================
							$sum_quantity_hari = $cabang->productins()
								->whereDay('sellins.datetime', '=', date('d') )
								// ->groupBy('sellins.id')
								->sum('quantity');
							if ($sum_quantity_hari > 0) {
								$cabang->total_quantity_sellin_hari = $sum_quantity_hari;
							} else {
								$cabang->total_quantity_sellin_hari = '-';
							}
							$sum_quantity_bulan = $cabang->productins()
								->whereMonth('sellins.datetime', '=', date('m') )
								// ->groupBy('sellins.id')
								->sum('quantity');
							if ($sum_quantity_bulan > 0) {
								$cabang->total_quantity_sellin_bulan = $sum_quantity_bulan;
							} else {
								$cabang->total_quantity_sellin_bulan = '-';
							}
							$sum_quantity_tahun = $cabang->productins()
								->whereYear('sellins.datetime', '=', date('Y') )
								// ->groupBy('sellins.id')
								->sum('quantity');
							if ($sum_quantity_tahun > 0) {
								$cabang->total_quantity_sellin_tahun = $sum_quantity_tahun;
							} else {
								$cabang->total_quantity_sellin_tahun = '-';
							}


						//
						// Sell Out Price Hari
						// ==========================================================================
							$sum_price_hari = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
								->where('sellouts.cabang_id', '=', $cabang->id )
								->whereDay('sellouts.datetime', '=', date('d') )
								->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
								// ->groupBy('sellouts.id')
								->first();
							if (isset($sum_price_hari->total_price)) {
								$cabang->total_price_sellout_hari = $sum_price_hari->total_price;
							} else {
								$cabang->total_price_sellout_hari = '-';
							}

						//
						// Sell Out Price Bulan
						// ==========================================================================
							$sum_price_bulan = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
								->where('sellouts.cabang_id', '=', $cabang->id )
								->whereMonth('sellouts.datetime', '=', date('m') )
								->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
								// ->groupBy('sellouts.id')
								->first();
							if (isset($sum_price_bulan->total_price)) {
								$cabang->total_price_sellout_bulan = $sum_price_bulan->total_price;
							} else {
								$cabang->total_price_sellout_bulan = '-';
							}

						//
						// Sell Out Price Tahun
						// ==========================================================================
							$sum_price_tahun = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
								->where('sellouts.cabang_id', '=', $cabang->id )
								->whereYear('sellouts.datetime', '=', date('Y') )
								->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
								// ->groupBy('sellouts.id')
								->first();
							if (isset($sum_price_tahun->total_price)) {
								$cabang->total_price_sellout_tahun = $sum_price_tahun->total_price;
							} else {
								$cabang->total_price_sellout_tahun = '-';
							}

						//
						// Sell Out Quantity
						// ==========================================================================
							$sum_quantity_hari = $cabang->productouts()
								->whereDay('sellouts.datetime', '=', date('d') )
								// ->groupBy('sellouts.id')
								->sum('quantity');
							if ($sum_quantity_hari > 0) {
								$cabang->total_quantity_sellout_hari = $sum_quantity_hari;
							} else {
								$cabang->total_quantity_sellout_hari = '-';
							}
							$sum_quantity_bulan = $cabang->productouts()
								->whereMonth('sellouts.datetime', '=', date('m') )
								// ->groupBy('sellouts.id')
								->sum('quantity');
							if ($sum_quantity_bulan > 0) {
								$cabang->total_quantity_sellout_bulan = $sum_quantity_bulan;
							} else {
								$cabang->total_quantity_sellout_bulan = '-';
							}
							$sum_quantity_tahun = $cabang->productouts()
								->whereYear('sellouts.datetime', '=', date('Y') )
								// ->groupBy('sellouts.id')
								->sum('quantity');
							if ($sum_quantity_tahun > 0) {
								$cabang->total_quantity_sellout_tahun = $sum_quantity_tahun;
							} else {
								$cabang->total_quantity_sellout_tahun = '-';
							}

					}

					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
						$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
					}

					$sheet->loadView('exports.report', compact('cabangs'));

					$dataCount = $cabangs->count();
					$lastRow = $dataCount + 7;
					$beforeLastRow = $lastRow - 1;

					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$sheet->setCellValue('A2', 'From:');
						$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
						$sheet->setCellValue('C2', 'Until:');
						$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
					} else {
						$sheet->setCellValue('A2', 'All Time');
					}
					$sheet->cells('B2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('D2', function($cells){
						$cells->setAlignment('left');
					});

					foreach (range('B', 'M') as $char) {
						$sheet->setCellValue($char.$lastRow,'=SUM('.$char.'7:'.$char.$beforeLastRow.')');
					}

					$sheet->getStyle('A1')->applyFromArray(array(
					    'font' => array(
					        'size'      =>  20,
        					'bold'      =>  true
					    )
					));
					// $sheet->mergeCells('B2:E2');
					// $sheet->setCellValue('B2','=DATE('.date('Y').', '.date('m').', '.date('d').')');
					$sheet->mergeCells('A4:A6');
					$sheet->cells('A4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('B4:E4');
					$sheet->cells('B4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('F4:I4');
					$sheet->cells('F4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('J4:M4');
					$sheet->cells('J4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('B5:C5');
					$sheet->cells('B5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('D5:E5');
					$sheet->cells('D5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('F5:G5');
					$sheet->cells('F5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('H5:I5');
					$sheet->cells('h5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('J5:K5');
					$sheet->cells('J5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('L5:M5');
					$sheet->cells('L5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->cells('B6:M6', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->cells('B'.$lastRow.':M'.$lastRow, function($cells){
						$cells->setFont(array(
						    'bold'       =>  true
						));
					});
					$sheet->cells('B7:M'.$lastRow, function($cells){
						$cells->setAlignment('right');
					});

					$sheet->setBorder('A4:M6', 'thin');

					foreach (range('A', 'M') as $char) {
						$sheet->cells($char.'7:'.$char.$lastRow, function($cells){
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}

				});	

			// ==========================================================================
			// Stock Opname
			// ==========================================================================

				$excel->sheet('Stock Opname Dealer', function($sheet) {

					//
					// All Dealers
					// ==========================================================================
						$products_alldealers = Product::all();
						foreach ($products_alldealers as &$product) {
							$product->total_sellin = $product->productins()
				                ->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
				                ->sum('productins.quantity');
				            $product->total_sellout = $product->productouts()
				                ->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
				                ->sum('productouts.quantity');
				            $product->ongoing_sellin = $product->productins()
				                ->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
				                ->where('sellins.datetime', '>=', Carbon::now()->startOfMonth() )
				                ->sum('productins.quantity');
				            $product->ongoing_sellout = $product->productouts()
				                ->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
				                ->where('sellouts.datetime', '>=', Carbon::now()->startOfMonth() )
				                ->sum('productouts.quantity');
						}

					//
					// Each Dealer
					// ==========================================================================
						$dealers = Dealer::all();

						$increment = 0;
						$products_eachdealer = [];

						for ($i=0; $i < count($dealers); $i++) { 
							
							$products = Product::select('products.id', 'products.name', 'products.code')
								->leftJoin('productins', 'productins.product_id', '=', 'products.id')
								->leftJoin('productouts', 'productouts.product_id', '=', 'products.id')
								->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
								->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
								->where('sellins.dealer_id', $dealers[$i]->id)
								->orWhere('sellouts.dealer_id', $dealers[$i]->id)
								->distinct()->get();

							if (!empty($products[0])) {

								$products_eachdealer[$increment]['dealer_id'] = $dealers[$i]->id;
								$products_eachdealer[$increment]['dealer_name'] = $dealers[$i]->name;
								$products_eachdealer[$increment]['products'] = $products;

								for ($index=0; $index < count($products); $index++) { 
									$products_eachdealer[$increment]['products'][$index]['total_sellin'] = $products[$index]->productins()
						                ->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
						                ->where('sellins.dealer_id', $dealers[$i]->id)
						                ->sum('productins.quantity');
						           	$products_eachdealer[$increment]['products'][$index]['total_sellout'] = $products[$index]->productouts()
						                ->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
						                ->sum('productouts.quantity');
						            $products_eachdealer[$increment]['products'][$index]['ongoing_sellin'] = $products[$index]->productins()
						                ->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
						                ->where('sellins.datetime', '>=', Carbon::now()->startOfMonth() )
						                ->sum('productins.quantity');
						            $products_eachdealer[$increment]['products'][$index]['ongoing_sellout'] = $products[$index]->productouts()
						                ->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
						                ->where('sellouts.datetime', '>=', Carbon::now()->startOfMonth() )
						                ->sum('productouts.quantity');
								}
								$increment++;
							}

						}

					// return View::make('exports.stockopname', compact('products_alldealers', 'products_eachdealer'));

					// echo json_encode($products_alldealers);

					$sheet->loadView('exports.stockopname', compact(
						'products_alldealers',
						'products_eachdealer'
					));

					$dataCount = $products_alldealers->count();
					$lastRow = $dataCount + 6;
					$beforeLastRow = $lastRow - 1;

					$sheet->getStyle('A1')->applyFromArray(array(
					    'font' => array(
					        'size'      =>  20,
        					'bold'      =>  true
					    )
					));
					$sheet->setCellValue('B2','=DATE('.date('Y').', '.date('m').', '.date('d').')');
					$sheet->cells('B2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('A5:G5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->setBorder('A5:G5', 'thin');
					for ($i=0; $i < count($products_alldealers); $i++) { 
						$num = 6 + $i;
						$sheet->setCellValue('G'.$num,'=C'.$num.'-D'.$num);
					}
					foreach (range('C', 'G') as $char) {
						$sheet->setCellValue($char.$lastRow,'=SUM('.$char.'6:'.$char.$beforeLastRow.')');
					}
					$sheet->cells('A'.$lastRow.':G'.$lastRow, function($cells){
						$cells->setFont(array(
						    'bold'       =>  true
						));
					});
					foreach (range('A', 'G') as $char) {
						$sheet->cells($char.'6:'.$char.$lastRow, function($cells){
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}

					for ($i=0; $i < count($products_eachdealer); $i++) { 

						$dataCount = count($products_eachdealer[$i]['products']);
						$firstRow = $lastRow + 3;
						$lastRow = $dataCount + $lastRow + 6;
						$beforeLastRow = $lastRow - 1;

						$sheet->cells('B'.$firstRow, function($cells){
							$cells->setAlignment('left');
						});

						$sheet->cells('A'.($firstRow+2).':G'.($firstRow+2), function($cells){
							$cells->setAlignment('center');
							$cells->setValignment('center');
						});
						$sheet->setBorder('A'.($firstRow+2).':G'.($firstRow+2), 'thin');

						for ($index=0; $index < $dataCount; $index++) { 
							$num = $firstRow + $index + 3;
							$sheet->setCellValue('G'.$num,'=C'.$num.'-D'.$num);
						}

						foreach (range('C', 'G') as $char) {
							$sheet->setCellValue($char.$lastRow,'=SUM('.$char.($firstRow+2).':'.$char.$beforeLastRow.')');
						}
						$sheet->cells('A'.$lastRow.':G'.$lastRow, function($cells){
							$cells->setFont(array(
							    'bold'       =>  true
							));
						});
						foreach (range('A', 'G') as $char) {
							$sheet->cells($char.($firstRow+2).':'.$char.$lastRow, function($cells){
								$cells->setBorder('thin', 'thin', 'thin', 'thin');
							});
						}
					}

				});

			// ==========================================================================
			// Sell Out
			// ==========================================================================

				$excel->sheet('Sell Out', function($sheet) {

					$productouts = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
						->leftJoin('dealers', 'sellouts.dealer_id', '=', 'dealers.id')
						->leftJoin('cabangs', 'sellouts.cabang_id', '=', 'cabangs.id')
						->leftJoin('subcabangs', 'sellouts.subcabang_id', '=', 'subcabangs.id')
						// ->groupBy('productouts.id')
						->select(
							'*',
							'dealers.id AS dealer_id', 
							'dealers.name AS dealer_name',
							'cabangs.name AS cabang_name',
							'subcabangs.name AS subcabang_name'
						);

					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
						$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
						$productouts = $productouts->where('sellouts.datetime', '>=', $start )
								->where('sellouts.datetime', '<=', $end );
					}

					$productouts = $productouts->get();

					for ($index=0; $index < $productouts->count() ; $index++) { 
						$sellout = Sellout::find( $productouts[$index]['sellout_id'] );
						$sales = $sellout->sales()->first();
						$spm = $sellout->spm()->first();
						$customer = $sellout->customer()->first();
						$productouts[$index]->sales_name = $sales->first_name.' '.$sales->last_name;
						$productouts[$index]->spm_name = $spm->first_name.' '.$spm->last_name;
						$productouts[$index]->customer_name = $customer->name;
						$productouts[$index]->customer_phone = $customer->phone;
					}

					// echo json_encode($productouts);
					// return View::make('exports.sellout', compact('productouts'));

					$sheet->loadView('exports.sellout', compact('productouts'));

					$dataCount = $productouts->count();
					$firstRow = 5;
					$lastRow = $firstRow + $dataCount;
					$beforeLastRow = $lastRow - 1;

					$sheet->getStyle('A1')->applyFromArray(array(
					    'font' => array(
					        'size'      =>  20,
        					'bold'      =>  true
					    )
					));
					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$sheet->setCellValue('A2', 'From:');
						$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
						$sheet->setCellValue('C2', 'Until:');
						$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
					} else {
						$sheet->setCellValue('A2', 'All Time');
					}
					$sheet->cells('B2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('D2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('A4:Q4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->setBorder('A4:Q4', 'thin');
					for ($index=0; $index < $dataCount ; $index++) { 
						$sellout = Sellout::find( $productouts[$index]['sellout_id'] );
						$year = date('Y', strtotime($sellout->datetime));
						$month = date('m', strtotime($sellout->datetime));
						$day = date('d', strtotime($sellout->datetime));
						$sheet->setCellValue('F'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
						$sheet->setCellValue('P'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
						$sheet->setCellValue('Q'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
					}
					$sheet->cells('A'.$firstRow.':A'.$beforeLastRow, function($cells){
						$cells->setAlignment('left');
					});
					for ($index=0; $index < $dataCount; $index++) { 
						$num = $firstRow + $index;
						$sheet->setCellValue('K'.$num,'=I'.$num.'*J'.$num);
					}
					if ($dataCount > 0) {
						$sheet->setCellValue('I'.$lastRow,'=SUM(I'.$firstRow.':I'.$beforeLastRow.')');
						$sheet->setCellValue('K'.$lastRow,'=SUM(K'.$firstRow.':K'.$beforeLastRow.')');
					}
					$sheet->cells('A'.$lastRow.':Q'.$lastRow, function($cells){
						$cells->setFont(array(
						    'bold'       =>  true
						));
					});
					foreach (range('A', 'Q') as $char) {
					    $sheet->cells($char.$firstRow.':'.$char.$lastRow, function($cells){
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}
					$sheet->setAutoSize(true);
					$sheet->setWidth('K', 16);
					$sheet->setFreeze('A5');
				});

			// ==========================================================================
			// Sell In
			// ==========================================================================

				$excel->sheet('Sell In', function($sheet) {

					$productins = Productin::leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
						->leftJoin('dealers', 'sellins.dealer_id', '=', 'dealers.id')
						->leftJoin('cabangs', 'sellins.cabang_id', '=', 'cabangs.id')
						->leftJoin('subcabangs', 'sellins.subcabang_id', '=', 'subcabangs.id')
						// ->groupBy('productins.id')
						->select(
							'*',
							'dealers.id AS dealer_id', 
							'dealers.name AS dealer_name',
							'cabangs.name AS cabang_name',
							'subcabangs.name AS subcabang_name'
						);

					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
						$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
						$productins = $productins->where('sellins.datetime', '>=', $start )
								->where('sellins.datetime', '<=', $end );
					}

					$productins = $productins->get();

					for ($index=0; $index < $productins->count() ; $index++) { 
						$sellin = Sellin::find( $productins[$index]['sellin_id'] );
						$sales = $sellin->sales()->first();
						$productins[$index]->sales_name = $sales->first_name.' '.$sales->last_name;
					}

					// return View::make('exports.sellin', compact('productins'));
					
					$sheet->loadView('exports.sellin', compact('productins'));

					$dataCount = $productins->count();
					$firstRow = 5;
					$lastRow = $firstRow + $dataCount;
					$beforeLastRow = $lastRow - 1;

					$sheet->getStyle('A1')->applyFromArray(array(
					    'font' => array(
					        'size'      =>  20,
        					'bold'      =>  true
					    )
					));
					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$sheet->setCellValue('A2', 'From:');
						$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
						$sheet->setCellValue('C2', 'Until:');
						$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
					} else {
						$sheet->setCellValue('A2', 'All Time');
					}
					$sheet->cells('B2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('D2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('A4:M4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->setBorder('A4:M4', 'thin');
					for ($index=0; $index < $dataCount ; $index++) { 
						$sellin = Sellin::find( $productins[$index]['sellin_id'] );
						$year = date('Y', strtotime($sellin->datetime));
						$month = date('m', strtotime($sellin->datetime));
						$day = date('d', strtotime($sellin->datetime));
						$sheet->setCellValue('F'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
						$sheet->setCellValue('L'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
						$sheet->setCellValue('M'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
					}
					$sheet->cells('A'.$firstRow.':A'.$beforeLastRow, function($cells){
						$cells->setAlignment('left');
					});
					for ($index=0; $index < $dataCount; $index++) { 
						$num = $firstRow + $index;
						$sheet->setCellValue('K'.$num,'=I'.$num.'*J'.$num);
					}
					if ($dataCount > 0) {
						$sheet->setCellValue('I'.$lastRow,'=SUM(I'.$firstRow.':I'.$beforeLastRow.')');
						$sheet->setCellValue('K'.$lastRow,'=SUM(K'.$firstRow.':K'.$beforeLastRow.')');
					}
					$sheet->cells('A'.$lastRow.':M'.$lastRow, function($cells){
						$cells->setFont(array(
						    'bold'       =>  true
						));
					});
					foreach (range('A', 'M') as $char) {
					    $sheet->cells($char.$firstRow.':'.$char.$lastRow, function($cells){
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}
					$sheet->setAutoSize(true);
					$sheet->setWidth('K', 16);
					$sheet->setFreeze('A5');
				});

			// ==========================================================================
			// Report
			// ==========================================================================
				$excel->sheet('Ranking', function($sheet) {

					$rangkings = Cabang::all();

					foreach ($rangkings as &$rangking) {
						$total_sellout_cabang_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
													->where('sellouts.cabang_id', '=', $rangking->id)
													->whereMonth('sellouts.datetime', '=', date('m') )
													->sum('productouts.price');
						$total_sellout_all_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
													->whereMonth('sellouts.datetime', '=', date('m') )
													->sum('productouts.price');
						$total_sellout_cabang_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
													->where('sellouts.cabang_id', '=', $rangking->id)
													->whereYear('sellouts.datetime', '=', date('Y') )
													->sum('productouts.price');
						$total_sellout_all_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
													->whereYear('sellouts.datetime', '=', date('Y') )
													->sum('productouts.price');
						$point_bulan = ($total_sellout_all_bulan > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_bulan) / ($total_sellout_all_bulan * $rangking->percentage / 100) : '-';
						$point_tahun = ($total_sellout_all_tahun > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_tahun) / ($total_sellout_all_tahun * $rangking->percentage / 100) : '-';

						$rangking['point_bulan'] = $point_bulan;
						$rangking['rangking_bulan'] = $total_sellout_all_bulan;
						$rangking['point_tahun'] = $point_tahun;
						$rangking['rangking_tahun'] = $total_sellout_all_tahun;
					}

					foreach ($rangkings as &$rangking) {
						$rangking_bulan = 1;
						$rangking_tahun = 1;
						for ($i=0; $i < count($rangkings); $i++) { 
							if ($rangking->point_bulan < $rangkings[$i]->point_bulan) {
								$rangking_bulan++;
							}
						}
						for ($i=0; $i < count($rangkings); $i++) { 
							if ($rangking->point_tahun < $rangkings[$i]->point_tahun) {
								$rangking_tahun++;
							}
						}
						$rangking['rangking_bulan'] = $rangking_bulan;
						$rangking['rangking_tahun'] = $rangking_tahun;	
					}

					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
						$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
					}

					$sheet->loadView('exports.ranking', compact('rangkings'));

					$dataCount = $rangkings->count();
					$firstRow = 6;
					$lastRow = $firstRow + $dataCount;
					$beforeLastRow = $lastRow - 1;

					$sheet->getStyle('A1')->applyFromArray(array(
					    'font' => array(
					        'size'      =>  20,
        					'bold'      =>  true
					    )
					));
					if (!empty($_GET['start']) && !empty($_GET['end'])) {
						$sheet->setCellValue('A2', 'From:');
						$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
						$sheet->setCellValue('C2', 'Until:');
						$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
					} else {
						$sheet->setCellValue('A2', 'All Time');
					}

					$sheet->cells('B2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('D2', function($cells){
						$cells->setAlignment('left');
					});
					$sheet->cells('A4:E4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->mergeCells('A4:A5');
					$sheet->mergeCells('B4:C4');
					$sheet->mergeCells('D4:E4');
					$sheet->setBorder('A4:E5', 'thin');
					$sheet->cells('A4:E4', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->cells('B5:E5', function($cells){
						$cells->setAlignment('center');
						$cells->setValignment('center');
					});
					$sheet->cells('B'.$firstRow.':E'.$beforeLastRow, function($cells){
						$cells->setAlignment('right');
					});
					foreach (range('A', 'E') as $char) {
					    $sheet->cells($char.$firstRow.':'.$char.$beforeLastRow, function($cells){
							$cells->setBorder('thin', 'thin', 'thin', 'thin');
						});
					}
					$sheet->setAutoSize(true);


				});
				
				
				

				$excel->setActiveSheetIndex(0);

		})->download('xls');
	}

	public function export($page, $type){

		if ($type !== 'xls' && $type !== 'csv') return Response::view('404');

		switch ($page) {

			// ==========================================================================
			// Sales Report
			// ==========================================================================

				case 'report':
					Excel::create('PT GREE - Report ('.date("Y-m-d").') h'.date('H').'m'.date('i'), function($excel) use ($type) {

						$excel->sheet('Sales Report', function($sheet) use ($type) {

							$cabangs = Cabang::get(['id','name']);

							foreach ($cabangs as &$cabang) {

								//
								// Sell In Price Hari
								// ==========================================================================
									$sum_price_hari = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
										->where('sellins.cabang_id', '=', $cabang->id )
										->whereDay('sellins.datetime', '=', date('d') )
										->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
										// ->groupBy('sellins.id')
										->first();
									if (isset($sum_price_hari->total_price)) {
										$cabang->total_price_sellin_hari = $sum_price_hari->total_price;
									} else {
										$cabang->total_price_sellin_hari = '-';
									}

								//
								// Sell In Price Bulan
								// ==========================================================================
									$sum_price_bulan = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
										->where('sellins.cabang_id', '=', $cabang->id )
										->whereMonth('sellins.datetime', '=', date('m') )
										->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
										// ->groupBy('sellins.id')
										->first();
									if (isset($sum_price_bulan->total_price)) {
										$cabang->total_price_sellin_bulan = $sum_price_bulan->total_price;
									} else {
										$cabang->total_price_sellin_bulan = '-';
									}

								//
								// Sell In Price Tahun
								// ==========================================================================
									$sum_price_tahun = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
										->where('sellins.cabang_id', '=', $cabang->id )
										->whereYear('sellins.datetime', '=', date('Y') )
										->select( DB::raw('sum(productins.quantity*productins.price) AS total_price') )
										// ->groupBy('sellins.id')
										->first();
									if (isset($sum_price_tahun->total_price)) {
										$cabang->total_price_sellin_tahun = $sum_price_tahun->total_price;
									} else {
										$cabang->total_price_sellin_tahun = '-';
									}

								//
								// Sell In Quantity
								// ==========================================================================
									$sum_quantity_hari = $cabang->productins()
										->whereDay('sellins.datetime', '=', date('d') )
										// ->groupBy('sellins.id')
										->sum('quantity');
									if ($sum_quantity_hari > 0) {
										$cabang->total_quantity_sellin_hari = $sum_quantity_hari;
									} else {
										$cabang->total_quantity_sellin_hari = '-';
									}
									$sum_quantity_bulan = $cabang->productins()
										->whereMonth('sellins.datetime', '=', date('m') )
										// ->groupBy('sellins.id')
										->sum('quantity');
									if ($sum_quantity_bulan > 0) {
										$cabang->total_quantity_sellin_bulan = $sum_quantity_bulan;
									} else {
										$cabang->total_quantity_sellin_bulan = '-';
									}
									$sum_quantity_tahun = $cabang->productins()
										->whereYear('sellins.datetime', '=', date('Y') )
										// ->groupBy('sellins.id')
										->sum('quantity');
									if ($sum_quantity_tahun > 0) {
										$cabang->total_quantity_sellin_tahun = $sum_quantity_tahun;
									} else {
										$cabang->total_quantity_sellin_tahun = '-';
									}


								//
								// Sell Out Price Hari
								// ==========================================================================
									$sum_price_hari = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
										->where('sellouts.cabang_id', '=', $cabang->id )
										->whereDay('sellouts.datetime', '=', date('d') )
										->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
										// ->groupBy('sellouts.id')
										->first();
									if (isset($sum_price_hari->total_price)) {
										$cabang->total_price_sellout_hari = $sum_price_hari->total_price;
									} else {
										$cabang->total_price_sellout_hari = '-';
									}

								//
								// Sell Out Price Bulan
								// ==========================================================================
									$sum_price_bulan = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
										->where('sellouts.cabang_id', '=', $cabang->id )
										->whereMonth('sellouts.datetime', '=', date('m') )
										->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
										// ->groupBy('sellouts.id')
										->first();
									if (isset($sum_price_bulan->total_price)) {
										$cabang->total_price_sellout_bulan = $sum_price_bulan->total_price;
									} else {
										$cabang->total_price_sellout_bulan = '-';
									}

								//
								// Sell Out Price Tahun
								// ==========================================================================
									$sum_price_tahun = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
										->where('sellouts.cabang_id', '=', $cabang->id )
										->whereYear('sellouts.datetime', '=', date('Y') )
										->select( DB::raw('sum(productouts.quantity*productouts.price) AS total_price') )
										// ->groupBy('sellouts.id')
										->first();
									if (isset($sum_price_tahun->total_price)) {
										$cabang->total_price_sellout_tahun = $sum_price_tahun->total_price;
									} else {
										$cabang->total_price_sellout_tahun = '-';
									}

								//
								// Sell Out Quantity
								// ==========================================================================
									$sum_quantity_hari = $cabang->productouts()
										->whereDay('sellouts.datetime', '=', date('d') )
										// ->groupBy('sellouts.id')
										->sum('quantity');
									if ($sum_quantity_hari > 0) {
										$cabang->total_quantity_sellout_hari = $sum_quantity_hari;
									} else {
										$cabang->total_quantity_sellout_hari = '-';
									}
									$sum_quantity_bulan = $cabang->productouts()
										->whereMonth('sellouts.datetime', '=', date('m') )
										// ->groupBy('sellouts.id')
										->sum('quantity');
									if ($sum_quantity_bulan > 0) {
										$cabang->total_quantity_sellout_bulan = $sum_quantity_bulan;
									} else {
										$cabang->total_quantity_sellout_bulan = '-';
									}
									$sum_quantity_tahun = $cabang->productouts()
										->whereYear('sellouts.datetime', '=', date('Y') )
										// ->groupBy('sellouts.id')
										->sum('quantity');
									if ($sum_quantity_tahun > 0) {
										$cabang->total_quantity_sellout_tahun = $sum_quantity_tahun;
									} else {
										$cabang->total_quantity_sellout_tahun = '-';
									}

							}

							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
								$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
							}

							$dataCount = $cabangs->count();
							$lastRow = $dataCount + 7;
							$beforeLastRow = $lastRow - 1;

							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$sheet->setCellValue('A2', 'From:');
								$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
								$sheet->setCellValue('C2', 'Until:');
								$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
							} else {
								$sheet->setCellValue('A2', 'All Time');
							}
							$sheet->cells('B2', function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('D2', function($cells){
								$cells->setAlignment('left');
							});

							foreach (range('B', 'M') as $char) {
								$sheet->setCellValue($char.$lastRow,'=SUM('.$char.'7:'.$char.$beforeLastRow.')');
							}

							$sheet->getStyle('A1')->applyFromArray(array(
							    'font' => array(
							        'size'      =>  20,
		        					'bold'      =>  true
							    )
							));
							// $sheet->mergeCells('B2:E2');
							// $sheet->setCellValue('B2','=DATE('.date('Y').', '.date('m').', '.date('d').')');
							$sheet->mergeCells('A4:A6');
							$sheet->cells('A4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('B4:E4');
							$sheet->cells('B4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('F4:I4');
							$sheet->cells('F4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('J4:M4');
							$sheet->cells('J4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('B5:C5');
							$sheet->cells('B5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('D5:E5');
							$sheet->cells('D5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('F5:G5');
							$sheet->cells('F5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('H5:I5');
							$sheet->cells('h5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('J5:K5');
							$sheet->cells('J5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('L5:M5');
							$sheet->cells('L5', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->cells('B6:M6', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->cells('B'.$lastRow.':M'.$lastRow, function($cells){
								$cells->setFont(array(
								    'bold'       =>  true
								));
							});
							$sheet->cells('B7:M'.$lastRow, function($cells){
								$cells->setAlignment('right');
							});

							$sheet->setBorder('A4:M6', 'thin');

							foreach (range('A', 'M') as $char) {
								$sheet->cells($char.'7:'.$char.$lastRow, function($cells){
									$cells->setBorder('thin', 'thin', 'thin', 'thin');
								});
							}

							//
							// Rangkings
							// ==========================================================================

							$rangkings = Cabang::all();

							foreach ($rangkings as &$rangking) {
								$total_sellout_cabang_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
															->where('sellouts.cabang_id', '=', $rangking->id)
															->whereMonth('sellouts.datetime', '=', date('m') )
															->sum('productouts.price');
								$total_sellout_all_bulan = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
															->whereMonth('sellouts.datetime', '=', date('m') )
															->sum('productouts.price');
								$total_sellout_cabang_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
															->where('sellouts.cabang_id', '=', $rangking->id)
															->whereYear('sellouts.datetime', '=', date('Y') )
															->sum('productouts.price');
								$total_sellout_all_tahun = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
															->whereYear('sellouts.datetime', '=', date('Y') )
															->sum('productouts.price');
								$point_bulan = ($total_sellout_all_bulan > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_bulan) / ($total_sellout_all_bulan * $rangking->percentage / 100) : '-';
								$point_tahun = ($total_sellout_all_tahun > 0 && $rangking->percentage > 0) ? (60 * $total_sellout_cabang_tahun) / ($total_sellout_all_tahun * $rangking->percentage / 100) : '-';

								$rangking['point_bulan'] = $point_bulan;
								$rangking['rangking_bulan'] = $total_sellout_all_bulan;
								$rangking['point_tahun'] = $point_tahun;
								$rangking['rangking_tahun'] = $total_sellout_all_tahun;
							}

							foreach ($rangkings as &$rangking) {
								$rangking_bulan = 1;
								$rangking_tahun = 1;
								for ($i=0; $i < count($rangkings); $i++) { 
									if ($rangking->point_bulan < $rangkings[$i]->point_bulan) {
										$rangking_bulan++;
									}
								}
								for ($i=0; $i < count($rangkings); $i++) { 
									if ($rangking->point_tahun < $rangkings[$i]->point_tahun) {
										$rangking_tahun++;
									}
								}
								$rangking['rangking_bulan'] = $rangking_bulan;
								$rangking['rangking_tahun'] = $rangking_tahun;	
							}

							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
								$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
							}


							if ($type === 'xls') {
								$sheet->loadView('exports.reportandranking', compact('cabangs', 'rangkings'));
							} else if ($type === 'csv') {
								$sheet->loadView('exports.reportandrankingcsv', compact('cabangs', 'rangkings'));
							}

							$titleRow = $lastRow + 3;
							$timeRow = $lastRow + 4;
							$headRow1 = $lastRow + 6;
							$headRow2 = $lastRow + 7;
							$dataCount = $rangkings->count();
							$firstRow = 5 + $titleRow;
							$lastRow = $firstRow + $dataCount;
							$beforeLastRow = $lastRow - 1;

							$sheet->getStyle('A'.$titleRow)->applyFromArray(array(
							    'font' => array(
							        'size'      =>  20,
		        					'bold'      =>  true
							    )
							));
							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$sheet->setCellValue('A'.$timeRow, 'From:');
								$sheet->setCellValue('B'.$timeRow,'=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
								$sheet->setCellValue('C'.$timeRow, 'Until:');
								$sheet->setCellValue('D'.$timeRow,'=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
							} else {
								$sheet->setCellValue('A'.$timeRow, 'All Time');
							}

							$sheet->cells('B'.$timeRow, function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('D'.$timeRow, function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('A'.$headRow1.':E'.$headRow1, function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->mergeCells('A'.$headRow1.':A'.$headRow2);
							$sheet->mergeCells('B'.$headRow1.':C'.$headRow1);
							$sheet->mergeCells('D'.$headRow1.':E'.$headRow1);
							$sheet->setBorder('A'.$headRow1.':E'.$headRow2, 'thin');
							$sheet->cells('A'.$headRow1.':E'.$headRow1, function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->cells('B'.$headRow2.':E'.$headRow2, function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->cells('B'.$firstRow.':E'.$beforeLastRow, function($cells){
								$cells->setAlignment('right');
							});
							foreach (range('A', 'E') as $char) {
							    $sheet->cells($char.$firstRow.':'.$char.$beforeLastRow, function($cells){
									$cells->setBorder('thin', 'thin', 'thin', 'thin');
								});
							}
							$sheet->setAutoSize(true);

						});

					})->download($type);

					break;

			
			// ==========================================================================
			// Sell In
			// ==========================================================================

				case 'sellin':

					Excel::create('PT GREE - Sell In ('.date("Y-m-d").') h'.date('H').'m'.date('i'), function($excel) use ($type) {
						$excel->sheet('Sell In', function($sheet) use ($type) {

							$productins = Productin::leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
								->leftJoin('dealers', 'sellins.dealer_id', '=', 'dealers.id')
								->leftJoin('cabangs', 'sellins.cabang_id', '=', 'cabangs.id')
								->leftJoin('subcabangs', 'sellins.subcabang_id', '=', 'subcabangs.id')
								// ->groupBy('productins.id')
								->select(
									'*',
									'dealers.id AS dealer_id', 
									'dealers.name AS dealer_name',
									'cabangs.name AS cabang_name',
									'subcabangs.name AS subcabang_name'
								);

							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
								$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
								$productins = $productins->where('sellins.datetime', '>=', $start )
										->where('sellins.datetime', '<=', $end );
							}

							$productins = $productins->get();

							for ($index=0; $index < $productins->count() ; $index++) { 
								$sellin = Sellin::find( $productins[$index]['sellin_id'] );
								$sales = $sellin->sales()->first();
								$productins[$index]->sales_name = $sales->first_name.' '.$sales->last_name;
							}

							// return View::make('exports.sellin', compact('productins'));
							
							if ($type === 'xls') {
								$sheet->loadView('exports.sellin', compact('productins'));
							} else if ($type === 'csv') {
								$sheet->loadView('exports.sellincsv', compact('productins'));
							}

							$dataCount = $productins->count();
							$firstRow = 5;
							$lastRow = $firstRow + $dataCount;
							$beforeLastRow = $lastRow - 1;

							$sheet->getStyle('A1')->applyFromArray(array(
							    'font' => array(
							        'size'      =>  20,
		        					'bold'      =>  true
							    )
							));
							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$sheet->setCellValue('A2', 'From:');
								$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
								$sheet->setCellValue('C2', 'Until:');
								$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
							} else {
								$sheet->setCellValue('A2', 'All Time');
							}
							$sheet->cells('B2', function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('D2', function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('A4:M4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->setBorder('A4:M4', 'thin');
							for ($index=0; $index < $dataCount ; $index++) { 
								$sellin = Sellin::find( $productins[$index]['sellin_id'] );
								$year = date('Y', strtotime($sellin->datetime));
								$month = date('m', strtotime($sellin->datetime));
								$day = date('d', strtotime($sellin->datetime));
								$sheet->setCellValue('F'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
								$sheet->setCellValue('L'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
								$sheet->setCellValue('M'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
							}
							$sheet->cells('A'.$firstRow.':A'.$beforeLastRow, function($cells){
								$cells->setAlignment('left');
							});
							for ($index=0; $index < $dataCount; $index++) { 
								$num = $firstRow + $index;
								$sheet->setCellValue('K'.$num,'=I'.$num.'*J'.$num);
							}
							if ($dataCount > 0) {
								$sheet->setCellValue('I'.$lastRow,'=SUM(I'.$firstRow.':I'.$beforeLastRow.')');
								$sheet->setCellValue('K'.$lastRow,'=SUM(K'.$firstRow.':K'.$beforeLastRow.')');
							}
							$sheet->cells('A'.$lastRow.':M'.$lastRow, function($cells){
								$cells->setFont(array(
								    'bold'       =>  true
								));
							});
							foreach (range('A', 'M') as $char) {
							    $sheet->cells($char.$firstRow.':'.$char.$lastRow, function($cells){
									$cells->setBorder('thin', 'thin', 'thin', 'thin');
								});
							}
							$sheet->setAutoSize(true);
							$sheet->setWidth('K', 16);
							$sheet->setFreeze('A5');
						});

					})->download($type);

					break;

			// ==========================================================================
			// Sell Out
			// ==========================================================================

				case 'sellout':

					Excel::create('PT GREE - Sell Out ('.date("Y-m-d").') h'.date('H').'m'.date('i'), function($excel) use ($type) {
						
						$excel->sheet('Sell Out', function($sheet) use ($type) {

							$productouts = Productout::leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
								->leftJoin('dealers', 'sellouts.dealer_id', '=', 'dealers.id')
								->leftJoin('cabangs', 'sellouts.cabang_id', '=', 'cabangs.id')
								->leftJoin('subcabangs', 'sellouts.subcabang_id', '=', 'subcabangs.id')
								// ->groupBy('productouts.id')
								->select(
									'*',
									'dealers.id AS dealer_id', 
									'dealers.name AS dealer_name',
									'cabangs.name AS cabang_name',
									'subcabangs.name AS subcabang_name'
								);

							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$start = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['start'].'00:00:00');
								$end = Carbon::createFromFormat('Y-m-d H:i:s', $_GET['end'].'23:59:59');
								$productouts = $productouts->where('sellouts.datetime', '>=', $start )
										->where('sellouts.datetime', '<=', $end );
							}

							$productouts = $productouts->get();

							for ($index=0; $index < $productouts->count() ; $index++) { 
								$sellout = Sellout::find( $productouts[$index]['sellout_id'] );
								$sales = $sellout->sales()->first();
								$spm = $sellout->spm()->first();
								$customer = $sellout->customer()->first();
								$productouts[$index]->sales_name = $sales->first_name.' '.$sales->last_name;
								$productouts[$index]->spm_name = $spm->first_name.' '.$spm->last_name;
								$productouts[$index]->customer_name = $customer->name;
								$productouts[$index]->customer_phone = $customer->phone;
							}

							// echo json_encode($productouts);
							// return View::make('exports.sellout', compact('productouts'));

							if ($type === 'xls') {
								$sheet->loadView('exports.sellout', compact('productouts'));
							} else if ($type === 'csv') {
								$sheet->loadView('exports.selloutcsv', compact('productouts'));
							}

							$dataCount = $productouts->count();
							$firstRow = 5;
							$lastRow = $firstRow + $dataCount;
							$beforeLastRow = $lastRow - 1;

							$sheet->getStyle('A1')->applyFromArray(array(
							    'font' => array(
							        'size'      =>  20,
		        					'bold'      =>  true
							    )
							));
							if (!empty($_GET['start']) && !empty($_GET['end'])) {
								$sheet->setCellValue('A2', 'From:');
								$sheet->setCellValue('B2','=DATE('.$start->format('Y').', '.$start->format('m').', '.$start->format('d').')');
								$sheet->setCellValue('C2', 'Until:');
								$sheet->setCellValue('D2','=DATE('.$end->format('Y').', '.$end->format('m').', '.$end->format('d').')');
							} else {
								$sheet->setCellValue('A2', 'All Time');
							}
							$sheet->cells('B2', function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('D2', function($cells){
								$cells->setAlignment('left');
							});
							$sheet->cells('A4:Q4', function($cells){
								$cells->setAlignment('center');
								$cells->setValignment('center');
							});
							$sheet->setBorder('A4:Q4', 'thin');
							for ($index=0; $index < $dataCount ; $index++) { 
								$sellout = Sellout::find( $productouts[$index]['sellout_id'] );
								$year = date('Y', strtotime($sellout->datetime));
								$month = date('m', strtotime($sellout->datetime));
								$day = date('d', strtotime($sellout->datetime));
								$sheet->setCellValue('F'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
								$sheet->setCellValue('P'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
								$sheet->setCellValue('Q'.($index+5),'=DATE('.$year.', '.$month.', '.$day.')');
							}
							$sheet->cells('A'.$firstRow.':A'.$beforeLastRow, function($cells){
								$cells->setAlignment('left');
							});
							for ($index=0; $index < $dataCount; $index++) { 
								$num = $firstRow + $index;
								$sheet->setCellValue('K'.$num,'=I'.$num.'*J'.$num);
							}
							if ($dataCount > 0) {
								$sheet->setCellValue('I'.$lastRow,'=SUM(I'.$firstRow.':I'.$beforeLastRow.')');
								$sheet->setCellValue('K'.$lastRow,'=SUM(K'.$firstRow.':K'.$beforeLastRow.')');
							}
							$sheet->cells('A'.$lastRow.':Q'.$lastRow, function($cells){
								$cells->setFont(array(
								    'bold'       =>  true
								));
							});
							foreach (range('A', 'Q') as $char) {
							    $sheet->cells($char.$firstRow.':'.$char.$lastRow, function($cells){
									$cells->setBorder('thin', 'thin', 'thin', 'thin');
								});
							}
							$sheet->setAutoSize(true);
							$sheet->setWidth('K', 16);
							$sheet->setFreeze('A5');
						});

					})->download($type);

					break;
			
			default:
				return Response::view('404');
				break;
		}
	}

}
