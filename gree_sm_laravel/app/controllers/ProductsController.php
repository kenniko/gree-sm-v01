<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductsController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.product', ['except' => 'showStockopname']);
        $this->beforeFilter('auth.stockopname', ['only' => 'showStockopname']);
    }

	/**
	 * Display a listing of products
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();

		return View::make('products.index', compact('products'));
	}

	/**
	 * Show the form for creating a new product
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('products.create');
	}

	/**
	 * Store a newly created product in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Product::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Product::create($data);

		return Redirect::route('product.index')->withSuccess('Product '.Input::get('name').' has been created successfully.');
	}

	/**
	 * Display the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$product = Product::findOrFail($id);

		 	return View::make('products.edit', compact('product'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('product.index');
		}
	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified product in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$product = Product::findOrFail($id);

		$data = Input::all();

		$rules = Product::$rules;

		if ($data['name'] == $product['name']) {
			unset($rules['name']);
		}
		if ($data['code'] == $product['code']) {
			unset($rules['code']);
		}

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$product->update($data);

		return Redirect::route('product.index')->withSuccess('Product '.Input::get('name').' has been updated successfully.');
	}

	/**
	 * Remove the specified product from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		$productins = Productin::where('product_id', $id)->count();
		$productouts = Productout::where('product_id', $id)->count();

		// check if this data have relation with others data
		if ($productins > 0 || $productins > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>This product has included in some transactions.']);

		// if this data free to go
		} else {
			Product::destroy($id);
			return Redirect::route('product.index')->withSuccess('Product '.Input::get('name').' has been deleted successfully.');
		}
	}
	public function destroySelected()
	{
		$productins = Productin::whereIn('product_id', Input::get('ids'))->count();
		$productouts = Productout::whereIn('product_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($productins > 0 || $productins > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>Some products have included in some transactions.']);

		// if this data free to go
		} else {
			Product::destroy(Input::get('ids'));
			return Redirect::route('product.index')->withSuccess('Selected Product has been deleted successfully.');
		}
	}

	public function showStockopname()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'sales' || $userRole == 'cabang')  {
			$cabangID = $user->cabang()->first()->id;
			$dealers = Dealer::leftJoin('subcabangs', 'subcabangs.id', '=', 'dealers.subcabang_id')
							->where('subcabangs.cabang_id', '=', $cabangID)
							->select('dealers.name AS dealer_name', 'dealers.id AS dealer_id')
							->distinct()
							->lists('dealer_name', 'dealer_id');
		} else {
			$dealers = Dealer::lists('name', 'id');
		}

		return View::make('products.stockopname', compact('dealers'));
	}

}
