<?php

class UsersController extends AuthorizedController {

	/**
	 * Holds the Sentinel Users repository.
	 *
	 * @var \Cartalyst\Sentinel\Users\EloquentUser
	 */
	protected $users;

	/**
	 * Constructor.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->beforeFilter('auth');
        $this->beforeFilter('auth.user', ['only' => ['index', 'create', 'store', 'updateSelected', 'removeSelected', 'destroy', 'destroySelected'] ]);

		$this->users = Sentinel::getUserRepository();
	}

	/**
	 * Display a listing of users.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'cabang') {
			$users = User::leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
						->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
						->whereIn('roles.slug', ['sales', 'spm'])
						->orWhere('users.id', '=', $user->id)
						->select('users.*')
						->distinct()->get();
			$roles = Sentinel::getRoleRepository()->createModel()->whereIn('slug', ['cabang', 'sales', 'spm'])->lists('name', 'name');
		} elseif ($userRole == 'adminpusat') {
			$users = User::leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
						->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
						->where('roles.slug', '!=', 'superadmin')
						->select('users.*')
						->distinct()->get();
			$roles = Sentinel::getRoleRepository()->createModel()->where('slug', '!=', 'superadmin')->lists('name', 'name');
		} else {
			$users = $this->users->get();
			$roles = Sentinel::getRoleRepository()->createModel()->lists('name', 'name');
		}

		foreach ($users as &$user) {
			if(!empty($user->cabang->name)) {
				$user['under'] = 'Cabang '.$user->cabang->name;
			} elseif (!empty($user->dealer->name)) {
				$user['under'] = 'Dealer '.$user->dealer->name;
			} else {
				$user['under'] = '';
			}
		}

		return View::make('users.index', compact('users', 'roles'));
	}

	/**
	 * Show the form for creating new user.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'cabang') {
			$roles = Sentinel::getRoleRepository()->createModel()->whereIn('slug', ['sales', 'cabang', 'spm'])->lists('name', 'slug');
			$cabangs = [$user->cabang->id => $user->cabang->name];
			$dealers = Dealer::leftJoin('subcabangs', 'subcabangs.id', '=', 'dealers.subcabang_id')
						->where('subcabangs.cabang_id', $user->cabang->id)
						->select('*', 'dealers.id AS id', 'dealers.name AS name')
						->distinct()->lists('name', 'id');
		} elseif ($userRole == 'adminpusat') {
			$roles = Sentinel::getRoleRepository()->createModel()->where('slug', '!=', 'superadmin')->lists('name', 'slug');
			$cabangs = Cabang::lists('name', 'id');
			$dealers = Dealer::lists('name', 'id');
		} else {
			$roles = Sentinel::getRoleRepository()->createModel()->lists('name', 'slug');
			$cabangs = Cabang::lists('name', 'id');
			$dealers = Dealer::lists('name', 'id');
		}

		return View::make('users.create', compact('roles', 'cabangs', 'dealers'));
	}

	/**
	 * Handle posting of the form for creating new user.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		$data = Input::all();

		$rules = User::$rules;

		if ($data['role'] == 'sales' || $data['role'] == 'cabang') {
			$rules['cabang'] = 'required';
		} else if ($data['role'] == 'spm') {
			$rules['dealer'] = 'required';
		}

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$role = Sentinel::findRoleBySlug($data['role']);

		if ($data['role'] == 'sales' || $data['role'] == 'cabang') {
			$data['cabang_id'] = $data['cabang'];
			unset($data['cabang']);
		} else if ($data['role'] == 'spm') {
			$data['dealer_id'] = $data['dealer'];
			unset($data['dealer']);
		}

		// $user = User::create($data);
		$user = Sentinel::registerAndActivate($data);

		$role->users()->attach($user);

		if (Input::has('id_origin')) {
			return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
					->withSuccess('User '.Input::get('username').' has been created successfully');
		} else {
			return Redirect::to('user')->withSuccess('User '.Input::get('username').' has been created successfully');
		}
	}

	/**
	 * Show the form for updating user.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function show($id)
	{
		try 
		{
		 	$user = User::findOrFail($id);

			$currentUser = Sentinel::getUser();
			$userRole = $currentUser->roles()->first()->slug;

			if ($userRole == 'cabang') {
				$roles = Sentinel::getRoleRepository()->createModel()->whereIn('slug', ['sales', 'cabang', 'spm'])->lists('name', 'slug');
				$cabangs = [$user->cabang->id => $user->cabang->name];
				$dealers = Dealer::leftJoin('subcabangs', 'subcabangs.id', '=', 'dealers.subcabang_id')
							->where('subcabangs.cabang_id', $user->cabang->id)
							->select('*', 'dealers.id AS id', 'dealers.name AS name')
							->distinct()->lists('name', 'id');
			} elseif ($userRole == 'adminpusat') {
				$roles = Sentinel::getRoleRepository()->createModel()->where('slug', '!=', 'superadmin')->lists('name', 'slug');
				$cabangs = Cabang::lists('name', 'id');
				$dealers = Dealer::lists('name', 'id');
			} else {
				$roles = Sentinel::getRoleRepository()->createModel()->lists('name', 'slug');
				$cabangs = Cabang::lists('name', 'id');
				$dealers = Dealer::lists('name', 'id');
			}

			return View::make('users.edit', compact('user', 'roles', 'cabangs', 'dealers'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('user.index');
		}
	}

	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Handle posting of the form for updating user.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		$data = Input::all();

		$user = User::find($id);

		$rules = User::$rules;

		if ($data['role'] == 'sales' || $data['role'] == 'cabang') {
			$rules['cabang'] = 'required';
		} else if ($data['role'] == 'spm') {
			$rules['dealer'] = 'required';
		}

		if(empty($data['password']) && empty($data['password_confirmation'])) {
			unset($rules['password']);
			unset($rules['password_confirmation']);
			unset($data['password']);
			unset($data['password_confirmation']);
		}

		if ($data['email'] == $user['email']) {
			unset($rules['email']);
		}

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		if ($user->roles()->first()->slug != $data['role']) {
			$oldRole = Sentinel::findRoleBySlug($user->roles()->first()->slug);
			$newRole = Sentinel::findRoleBySlug($data['role']);

			$oldRole->users()->detach($user);
			$newRole->users()->attach($user);
		}

		if ($data['role'] == 'sales' || $data['role'] == 'cabang') {
			$data['cabang_id'] = $data['cabang'];
			unset($data['cabang']);
		} else if ($data['role'] == 'spm') {
			$data['dealer_id'] = $data['dealer'];
			unset($data['dealer']);
		}

		Sentinel::update($user, $data);

		if (Sentinel::hasAccess('user'))
		{
			if (Input::has('id_origin')) {
				return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
					->withSuccess('User '.Input::get('username').' has been created successfully');
			} else {
				return Redirect::route('user.index')->withSuccess('User '.Input::get('username').' has been updated successfully');
			}
		} 
		else 
		{
			return Redirect::to('/')->withSuccess('User '.Input::get('username').' has been updated successfully');
		}
	}
	public function updateSelected()
	{
		$cabangID = Input::get('cabang_id');
		$dealerID = Input::get('dealer_id');

		if (!empty($cabangID)) 
		{
			User::whereIn('id', Input::get('ids') )->update(array('cabang_id' => $cabangID));
			return Redirect::to('cabang/'.$cabangID)->withSuccess('Selected Sales has been updated successfully');
		} 
		else if (!empty($dealerID))
		{
			User::whereIn('id', Input::get('ids') )->update(array('dealer_id' => $dealerID));
			return Redirect::to('dealer/'.$dealerID)->withSuccess('Selected SPM has been updated successfully');
		}
	}
	public function removeSelected()
	{
		$cabangID = Input::get('cabang_id');
		$dealerID = Input::get('dealer_id');

		if (!empty($cabangID))
		{
			User::whereIn('id', Input::get('ids') )->update(array('cabang_id' => null));
			return Redirect::to('cabang/'.$cabangID)->withSuccess('Selected Sales has been remove successfully');
		} 
		else if (!empty($dealerID))
		{
			User::whereIn('id', Input::get('ids') )->update(array('dealer_id' => null));
			return Redirect::to('dealer/'.$dealerID)->withSuccess('Selected SPM has been remove successfully');
		}
	}

	/**
	 * Remove the specified user.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		$sellins = Sellin::where('sales_id', $id)->count();
		$sellouts = Sellout::where('sales_id', $id)->orWhere('spm_id', $id)->count();
		$cabangsOrDealers = User::where('id', $id)->where(function($query)
            {
                $query->whereNotNull('cabang_id')
                	  ->orWhereNotNull('dealer_id');
            })->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0 || $cabangsOrDealers > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. This item has relation with others.']);

		// if this data free to go
		} else {

			User::destroy($id);

			return Redirect::route('user.index')->withSuccess('User '.Input::get('username').' has been deleted successfully');
		}
	}
	public function destroySelected()
	{
		$sellins = Sellin::whereIn('sales_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('sales_id', Input::get('ids'))->orWhereIn('spm_id', Input::get('ids'))->count();
		$cabangsOrDealers = User::whereIn('id', Input::get('ids'))->where(function($query)
            {
                $query->whereNotNull('cabang_id')
                	  ->orWhereNotNull('dealer_id');
            })->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0 || $cabangsOrDealers > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. Some items has relation with others.']);

		// if this data free to go
		} else {

			User::destroy(Input::get('ids'));

			return Redirect::route('user.index')->withSuccess('Selected User has been deleted successfully');
		}
	}

}
