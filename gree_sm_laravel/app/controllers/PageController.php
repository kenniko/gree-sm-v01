<?php

class PageController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function dashboard()
	{
		$total_sellin = Productin::all()->sum('quantity');
		$total_sellout = Productout::all()->sum('quantity');
		return View::make('dashboard', compact('total_sellin', 'total_sellout'));
	}

}
