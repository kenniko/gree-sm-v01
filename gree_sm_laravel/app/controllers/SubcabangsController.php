<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubcabangsController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.subcabang');
    }
	/**
	 * Display a listing of subcabangs
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'cabang') {
			$subcabangs = Subcabang::where('cabang_id', $user->cabang->id)->get();
		} else {
			$subcabangs = Subcabang::all();
		}
			
		return View::make('subcabangs.index', compact('subcabangs'));
	}

	/**
	 * Show the form for creating a new subcabang
	 *
	 * @return Response
	 */
	public function create()
	{
		$cabangs = Cabang::lists('name', 'id');
		
		return View::make('subcabangs.create', compact('cabangs'));
	}

	/**
	 * Store a newly created subcabang in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Subcabang::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$subcabang = Subcabang::create($data);

		if (Input::has('id_origin')) {
			return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
					->withSuccess('Subcabang '.Input::get('name').' has been created successfully');
		} else {
			// return Redirect::to(Input::get('route_origin', 'subcabang').'/'.Input::get('id_origin', $subcabang->id))
			// 			->with(compact('subcabang'))
			// 			->withSuccess('Subcabang '.Input::get('name').' has been created successfully');
			return Redirect::to('subcabang')->withSuccess('Subcabang '.Input::get('name').' has been created successfully');
		}

	}

	/**
	 * Display the specified subcabang.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$subcabang = Subcabang::findOrFail($id);

			$cabangs = Cabang::lists('name', 'id');

			return View::make('subcabangs.edit', compact('subcabang', 'cabangs'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('subcabang.index');
		}
	}

	/**
	 * Show the form for editing the specified subcabang.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified subcabang in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$subcabang = Subcabang::findOrFail($id);

		$data = Input::all();

		$sellins = Sellin::where('subcabang_id', $id)->count();
		$sellouts = Sellout::where('subcabang_id', $id)->count();

		// check if this data have relation with others data
		if ( ($sellins > 0 || $sellouts > 0) && $subcabang['cabang_id'] != $data['cabang_id'] ) {
			return Redirect::back()->withErrors(['subcabang_id'=>'Can not perform update cabang.<br>This sub canang has included in some transactions.']);

		// if this data free to go
		} else {

			$rules = Subcabang::$rules;

			if ($data['name'] == $subcabang['name']) {
				unset($rules['name']);
			}

			$validator = Validator::make($data, $rules);

			if ($validator->fails())
			{
				return Redirect::back()->withErrors($validator)->withInput();
			}

			$subcabang->update($data);

			if (!empty(Input::get('route_origin'))) {
				return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
						->withSuccess('Sub Cabang '.Input::get('name').' has been updated successfully');
			} else {
				return Redirect::route('subcabang.index')->withSuccess('Sub Cabang '.Input::get('name').' has been updated successfully');
			}
		}
	}
	public function updateSelected()
	{
		$sellins = Sellin::whereIn('subcabang_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('subcabang_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform add.<br>The selected sub cabangs has included in some transactions.']);

		// if this data free to go
		} else {

			$cabangID = Input::get('cabang_id');

	    	Subcabang::whereIn('id', Input::get('ids') )->update(array('cabang_id' => $cabangID));

			return Redirect::to('cabang/'.$cabangID)->withSuccess('Selected Subcabang has been updated successfully');
		}
	}
	public function removeSelected()
	{
		$sellins = Sellin::whereIn('subcabang_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('subcabang_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform remove.<br>The selected sub cabangs has included in some transactions.']);

		// if this data free to go
		} else {

			$cabangID = Input::get('cabang_id');
			
	    	Subcabang::whereIn('id', Input::get('ids') )->update(array('cabang_id' => null));

			return Redirect::to('cabang/'.$cabangID)->withSuccess('Selected Subcabang has been remove successfully');
		}
	}

	/**
	 * Remove the specified subcabang from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sellins = Sellin::where('subcabang_id', $id)->count();
		$sellouts = Sellout::where('subcabang_id', $id)->count();
		$dealer = Dealer::where('subcabang_id', $id)->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>This Sub Cabang has included in some transactions.']);
		} elseif ($dealer > 0) {
			return Redirect::back()->withErrors(['Can not perform delete. This Sub Cabang has Dealer.<br>Please update Dealer '.$dealer->name.' first.']);

		// if this data free to go
		} else {

			Subcabang::destroy($id);

			if (!empty(Input::get('route_origin'))) {
				return Redirect::to(Input::get('route_origin').'/'.Input::get('id_origin'))
						->withSuccess('Sub Cabang '.Input::get('name').' has been deleted successfully');
			} else {
				return Redirect::route('subcabang.index')->withSuccess('Sub Cabang '.Input::get('name').' has been deleted successfully');
			}
		}
	}
	public function destroySelected()
	{
		$sellins = Sellin::whereIn('subcabang_id', Input::get('ids'))->count();
		$sellouts = Sellout::whereIn('subcabang_id', Input::get('ids'))->count();
		$dealers = Dealer::whereIn('subcabang_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellins > 0 || $sellouts > 0 || $dealers > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>Some sub cabangs have included in some transactions or already have Dealer.']);

		// if this data free to go
		} else {

			Subcabang::destroy(Input::get('ids'));

			return Redirect::route('subcabang.index')->withSuccess('Selected Sub Cabang has been deleted successfully');
		}
	}

}
