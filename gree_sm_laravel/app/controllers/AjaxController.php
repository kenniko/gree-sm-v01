<?php

use Carbon\Carbon;

class AjaxController extends BaseController {

	public function getAllData($data)
	{
		if (empty($data) || !Request::ajax()) {

			return Response::view('404');

		} else {

			switch ($data) {

				case 'subcabangs':
					
					if ($_GET['origin'] == 'editcabang') {
						$user = Sentinel::getUser();
						$userRole = $user->roles()->first()->slug;
						if ($userRole == 'cabang') {
							$result = Subcabang::leftJoin('cabangs','subcabangs.cabang_id','=','cabangs.id')
								->where('subcabangs.cabang_id', '!=', $_GET['id'])
								->where('subcabangs.cabang_id', '=', $user->cabang->id)
								->orWhere('subcabangs.cabang_id', '=', null)
								->select(array('subcabangs.id', 'subcabangs.name', 'cabangs.name as cabang'));
						} else {
							$result = Subcabang::leftJoin('cabangs','subcabangs.cabang_id','=','cabangs.id')
								->where('subcabangs.cabang_id', '!=', $_GET['id'])
								->orWhere('subcabangs.cabang_id', '=', null)
								->select(array('subcabangs.id', 'subcabangs.name', 'cabangs.name as cabang'));
						}
					} else {
						$result = DB::table($data)->select('*');
					}
					return Datatables::of($result)->make(true);
					break;

				case 'sales':

					$roleSales = Sentinel::findRoleBySlug('sales');

					if ($_GET['origin'] == 'editcabang') {
						$sales = User::leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
							->leftJoin('cabangs', 'cabangs.id', '=', 'users.cabang_id')
							->where('role_users.role_id', '=', $roleSales->id)
							->where(function($query)
				            {
				                $query->where('users.cabang_id', '!=', $_GET['id'])
										->orWhere('users.cabang_id', '=', null);
				            })
				            ->select('users.id AS id','users.first_name AS first_name', 'users.last_name AS last_name', 'users.username AS username', 'users.email AS email', 'cabangs.name AS cabang')
				            ->distinct();
					} else {
						$sales = $roleSales->users();
					}
					return Datatables::of($sales)->make(true);

					break;

				case 'spm':

					$roleSpm = Sentinel::findRoleBySlug('spm');

					if ($_GET['origin'] == 'editdealer') {
						$spm = User::leftJoin('role_users', 'role_users.user_id', '=', 'users.id')
							->leftJoin('dealers', 'dealers.id', '=', 'users.dealer_id')
							->where('role_users.role_id', '=', $roleSpm->id)
							->where(function($query)
				            {
				                $query->where('users.dealer_id', '!=', $_GET['id'])
										->orWhere('users.dealer_id', '=', null);
				            })
				            ->select('users.id AS id','users.first_name AS first_name', 'users.last_name AS last_name', 'users.username AS username', 'users.email AS email', 'dealers.name AS dealer')
				            ->distinct();
					} else {
						$spm = $roleSpm->users();
					}
					return Datatables::of($spm)->make(true);

					break;

				case 'dealers':

					if ($_GET['origin'] == 'editsubcabang') {
						$user = Sentinel::getUser();
						$userRole = $user->roles()->first()->slug;
						if ($userRole == 'cabang') {
							$result = Dealer::leftJoin('subcabangs','dealers.subcabang_id','=','subcabangs.id')
								->where('dealers.subcabang_id', '!=', $_GET['id'])
								->where('subcabangs.cabang_id', '=', $user->cabang->id)
								->orWhere('dealers.subcabang_id', '=', null)
								->select(array('dealers.id', 'dealers.name', 'subcabangs.name as subcabang'));
						} else {
							$result = Dealer::leftJoin('subcabangs','dealers.subcabang_id','=','subcabangs.id')
								->where('dealers.subcabang_id', '!=', $_GET['id'])
								->orWhere('dealers.subcabang_id', '=', null)
								->select(array('dealers.id', 'dealers.name', 'subcabangs.name as subcabang'));
						}
					} else {
						$result = DB::table($data)->select('*');
					}
					return Datatables::of($result)->make(true);
					break;

				case 'selectize':

					if ($_GET['data'] == 'subcabangs') {
						$subcabangs = Subcabang::where('cabang_id', $_GET['cabang_id'])->select('id', 'name')->get();
						return $subcabangs->toArray();
					} elseif ($_GET['data'] == 'dealers') {
						$dealers = Dealer::where('subcabang_id', $_GET['subcabang_id'])->select('id', 'name')->get();
						return $dealers->toArray();
					} elseif ($_GET['data'] == 'sales') {
						$salesRole = Sentinel::findRoleBySlug('sales');
						$sales = $salesRole->users()->where('cabang_id', $_GET['cabang_id'])->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get();
						return $sales->toArray();
					} elseif ($_GET['data'] == 'spm') {
						$spm = User::where('dealer_id', $_GET['dealer_id'])->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get();
						return $spm->toArray();
					} elseif ($_GET['data'] == 'customers' && $_GET['query']) {
						$customers = Customer::where('name', 'LIKE', '%'.$_GET['query'].'%')
												->orWhere('phone', 'LIKE', '%'.$_GET['query'].'%')
												->select('id', 'name', 'phone')->distinct()->get();
						return $customers->toArray();
					}
					break;

				case 'get':

					if ($_GET['data'] == 'customer') {
						$customer = Customer::find($_GET['id']);
						return $customer->toArray();
					}
					break;

				case 'stockopname':

					$testid = 1;

					$products = Product::select('products.id', 'products.name', 'products.code');
					if (isset($_GET['cabang_id'])) {
						$products = $products->leftJoin('productins', 'productins.product_id', '=', 'products.id')
							->leftJoin('productouts', 'productouts.product_id', '=', 'products.id')
							->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
							->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
							->where(function($query)
				            {
				                $query->where('sellins.cabang_id', $_GET['cabang_id'])
									  ->orWhere('sellouts.cabang_id', $_GET['cabang_id']);
							});
					} elseif (isset($_GET['dealer_id'])) {
						$products = $products->leftJoin('productins', 'productins.product_id', '=', 'products.id')
							->leftJoin('productouts', 'productouts.product_id', '=', 'products.id')
							->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
							->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
							->where(function($query)
				            {
				                $query->where('sellins.dealer_id', $_GET['dealer_id'])
									  ->orWhere('sellouts.dealer_id', $_GET['dealer_id']);
				            });
					}
					$products = $products->distinct();

					return Datatables::of($products)
							->addColumn('total_sellin', function ($product) {
								$total_sellin = $product->productins()
				                	->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id');
				                if (isset($_GET['cabang_id'])) {
				                	$total_sellin->where('sellins.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$total_sellin->where('sellins.dealer_id', $_GET['dealer_id']);
				                }
				                return $total_sellin->sum('productins.quantity');
				            })
				            ->addColumn('total_sellout', function ($product) {
				            	$total_sellout = $product->productouts()
				            		->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id');
				            	if (isset($_GET['cabang_id'])) {
				                	$total_sellout->where('sellouts.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$total_sellout->where('sellouts.dealer_id', $_GET['dealer_id']);
				                }
				                return $total_sellout->sum('productouts.quantity');
				            })
				            ->addColumn('ongoing_sellin', function ($product) {
				            	$ongoing_sellin = $product->productins()
				                	->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id')
				                	->where('sellins.created_at', '>=', Carbon::now()->startOfMonth() );
				                if (isset($_GET['cabang_id'])) {
				                	$ongoing_sellin->where('sellins.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$ongoing_sellin->where('sellins.dealer_id', $_GET['dealer_id']);
				                }
				                return $ongoing_sellin->sum('productins.quantity');
				            })
				            ->addColumn('ongoing_sellout', function ($product) {
				            	$ongoing_sellout = $product->productouts()
				            		->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id')
				            		->where('sellouts.created_at', '>=', Carbon::now()->startOfMonth() );
				            	if (isset($_GET['cabang_id'])) {
				                	$ongoing_sellout->where('sellouts.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$ongoing_sellout->where('sellouts.dealer_id', $_GET['dealer_id']);
				                }
				                return $ongoing_sellout->sum('productouts.quantity');
				            })
				            ->addColumn('balance_stock', function ($product) {
								$total_sellin = $product->productins()
				                	->leftJoin('sellins', 'sellins.id', '=', 'productins.sellin_id');
				                if (isset($_GET['cabang_id'])) {
				                	$total_sellin->where('sellins.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$total_sellin->where('sellins.dealer_id', $_GET['dealer_id']);
				                }
				                $total_sellin = $total_sellin->sum('productins.quantity');
				                $total_sellout = $product->productouts()
				            		->leftJoin('sellouts', 'sellouts.id', '=', 'productouts.sellout_id');
				            	if (isset($_GET['cabang_id'])) {
				                	$total_sellout->where('sellouts.cabang_id', $_GET['cabang_id']);
				                }
				                if (isset($_GET['dealer_id'])) {
				                	$total_sellout->where('sellouts.dealer_id', $_GET['dealer_id']);
				                }
				                $total_sellout = $total_sellout->sum('productouts.quantity');
				                return $total_sellin - $total_sellout;
				            })
							->make(true);
					break;
				
				default:
					return Response::view('404');
					break;
			}

		}
	}

}
