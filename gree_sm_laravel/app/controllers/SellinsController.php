<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;

class SellinsController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.sellin');
    }

	/**
	 * Display a listing of sellins
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'sales') 
		{
			$sellins = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->where('sellins.sales_id', '=', $user->id)
					->select('sellins.*', DB::raw('sum(productins.quantity*productins.price) AS total_price'))
					->groupBy('sellins.id')->get();
		} 
		elseif ($userRole == 'cabang') 
		{
			$sellins = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->where('sellins.cabang_id', '=', $user->cabang->id)
					->select('sellins.*', DB::raw('sum(productins.quantity*productins.price) AS total_price'))
					->groupBy('sellins.id')->get();
		} 
		else 
		{
			$sellins = Sellin::leftJoin('productins', 'productins.sellin_id', '=', 'sellins.id')
					->select('sellins.*', DB::raw('sum(productins.quantity*productins.price) AS total_price'))
					->groupBy('sellins.id')->get();
		}

		return View::make('sellins.index', compact('sellins'));
	}

	/**
	 * Show the form for creating a new sellin
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		$products = Product::all()->lists('name_and_code', 'id');

		if ($userRole == 'sales') {
			$sales = [$user->id => $user->full_name];
			$cabangs = [$user->cabang->id => $user->cabang->name];
			$subcabangs = Subcabang::where('cabang_id', $user->cabang->id)->lists('name', 'id');
			return View::make('sellins.create', compact('sales', 'cabangs', 'subcabangs', 'products'));
		} if ($userRole == 'cabang') {
			$salesRole = Sentinel::findRoleBySlug('sales');
			$sales = $salesRole->users()->where('cabang_id', $user->cabang->id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
			$cabangs = [$user->cabang->id => $user->cabang->name];
			$subcabangs = Subcabang::where('cabang_id', $user->cabang->id)->lists('name', 'id');
			return View::make('sellins.create', compact('sales', 'cabangs', 'subcabangs', 'products'));
		} else {
			$cabangs = Cabang::lists('name', 'id');
			return View::make('sellins.create', compact('cabangs', 'products'));
		}
	}

	/**
	 * Store a newly created sellin in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// get rule
		$rules = Sellin::$rules;

		// get inputs
		$inputs = Input::only('sales_id', 'datetime', 'cabang_id', 'subcabang_id', 'dealer_id');

		// convert date
		$datetime_origin = $inputs['datetime'];
		if ($inputs['datetime']) {
			$datetime = str_replace('- ', '', $inputs['datetime']);
			$dates = explode("/", $datetime);
			$inputs['datetime'] = $dates[1].'/'.$dates[0].'/'.$dates[2];
		}

		$inputs['transactions'] = [];
		$price_origin = [];

		$transactions_count = count(Input::get('product_id'));

		for ($key=0; $key < $transactions_count; $key++)
		{
			$rules['product_id.'.$key] = Productin::$rules['product_id'];
			$inputs['product_id.'.$key] = Input::get('product_id.'.$key);
			$rules['quantity.'.$key] = Productin::$rules['quantity'];
			$inputs['quantity.'.$key] = Input::get('quantity.'.$key);
			$rules['price.'.$key] = Productin::$rules['price'];
			array_push($price_origin, Input::get('price.'.$key));
			$inputs['price.'.$key] = str_replace('.', '', Input::get('price.'.$key));

			array_push($inputs['transactions'], $key);

		}

		// validate
		$validator = Validator::make($inputs, $rules);

		// check if cabang filled
		if (!empty($cabang_id = $inputs['cabang_id'])) {
			$inputs['subcabangs'] = Subcabang::where('cabang_id', $cabang_id)->lists('name', 'id');
			$salesRole = Sentinel::findRoleBySlug('sales');
			$inputs['sales'] = $salesRole->users()->where('cabang_id', $cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}
		// check if subcabang filled
		if (!empty($subcabang_id = $inputs['subcabang_id'])) {
			$inputs['dealers'] = Dealer::where('subcabang_id', $subcabang_id)->lists('name', 'id');
		}

		if ($validator->fails())
		{
			// conver input to origin
			$inputs['datetime'] = $datetime_origin;

			for ($key=0; $key < $transactions_count; $key++)
			{
				$inputs['price.'.$key] = $price_origin[$key];
			}

			return Redirect::back()->withErrors($validator)->withInput($inputs);
		}

		// save Sellin
		$Sellin = Sellin::create([
			'sales_id' => $inputs['sales_id'],
			'cabang_id' => $inputs['cabang_id'],
			'subcabang_id' => $inputs['subcabang_id'],
			'dealer_id' => $inputs['dealer_id'],
			'datetime' => date_create_from_format('m/d/Y H:i', $inputs['datetime'])
		]);

		for ($key=0; $key < $transactions_count; $key++)
		{
			Productin::create([
				'product_id' => $inputs['product_id.'.$key],
				'quantity' => $inputs['quantity.'.$key],
				'price' => $inputs['price.'.$key],
				'sellin_id' => $Sellin->id
			]);
		}

		return Redirect::route('sellin.index')->withSuccess('Sellin has been created successfully');
	}

	/**
	 * Display the specified sellin.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$sellin = Sellin::findOrFail($id);
			
			$user = Sentinel::getUser();
			$userRole = $user->roles()->first()->slug;

			if ($userRole == 'sales') {
				$sales = [$user->id => $user->full_name];
				// if sales still on the same cabang
				if($sellin->cabang_id == $user->cabang->id) {
					$cabangs = [$user->cabang->id => $user->cabang->name];
				// if sales has move to to other cabang
				} else {
					$cabangs = Cabang::where('id', $sellin->cabang_id)->lists('name', 'id');
				}
			} elseif ($userRole == 'cabang') {
				$cabangs = [$user->cabang->id => $user->cabang->name];
				$salesRole = Sentinel::findRoleBySlug('sales');
				$sales = $salesRole->users()->where('cabang_id', $user->cabang->id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
			} else {
				$cabangs = Cabang::lists('name', 'id');
				$salesRole = Sentinel::findRoleBySlug('sales');
				$sales = $salesRole->users()->where('cabang_id', $sellin->cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
			}

		 	$subcabangs = Subcabang::where('cabang_id', $sellin->cabang_id)->lists('name', 'id');
		 	$dealers = Dealer::where('subcabang_id', $sellin->subcabang_id)->lists('name', 'id');

		 	$productins = Productin::where('sellin_id', $id)->get();
		 	foreach ($productins as &$productin) {
		 		$productin['price'] = number_format($productin['price'], 0, '', '.');
		 	}

		 	$products = Product::all()->lists('name_and_code', 'id');
		 	
		 	return View::make('sellins.edit', compact('sellin', 'sales', 'cabangs', 'subcabangs', 'dealers', 'productins', 'products'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('sellin.index');
		}
	}

	/**
	 * Show the form for editing the specified sellin.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified sellin in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($sellin_id)
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		// if sales has move to to other cabang
		if ($userRole == 'sales' && Input::get('cabang_id') != $user->cabang->id) {
			return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Cabang.']);
		}

		// get rule
		$rules = Sellin::$rules;

		// get inputs
		$inputs = Input::only('sales_id', 'datetime', 'cabang_id', 'subcabang_id', 'dealer_id');

		// convert date
		$datetime_origin = $inputs['datetime'];
		if ($inputs['datetime']) {
			$datetime = str_replace('- ', '', $inputs['datetime']);
			$dates = explode("/", $datetime);
			$inputs['datetime'] = $dates[1].'/'.$dates[0].'/'.$dates[2];
		}

		$data_sellin = $inputs;

		$inputs['transactions'] = [];
		$price_origin = [];

		$transactions_count = count(Input::get('product_id'));

		for ($key=0; $key < $transactions_count; $key++)
		{
			$inputs['productin_id.'.$key] = Input::get('productin_id.'.$key);

			$rules['product_id.'.$key] = Productin::$rules['product_id'];
			$inputs['product_id.'.$key] = Input::get('product_id.'.$key);

			$rules['quantity.'.$key] = Productin::$rules['quantity'];
			$inputs['quantity.'.$key] = Input::get('quantity.'.$key);

			$rules['price.'.$key] = Productin::$rules['price'];
			array_push($price_origin, Input::get('price.'.$key));
			$inputs['price.'.$key] = str_replace('.', '', Input::get('price.'.$key));

			array_push($inputs['transactions'], $key);

		}

		// validate
		$validator = Validator::make($inputs, $rules);

		// check if cabang filled
		if (!empty($cabang_id = $inputs['cabang_id'])) {
			$inputs['subcabangs'] = Subcabang::where('cabang_id', $cabang_id)->lists('name', 'id');
			$salesRole = Sentinel::findRoleBySlug('sales');
			$inputs['sales'] = $salesRole->users()->where('cabang_id', $cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}
		// check if subcabang filled
		if (!empty($subcabang_id = $inputs['subcabang_id'])) {
			$inputs['dealers'] = Dealer::where('subcabang_id', $subcabang_id)->lists('name', 'id');
		}

		if ($validator->fails())
		{
			// conver input to origin
			$inputs['datetime'] = $datetime_origin;
			for ($key=0; $key < $transactions_count; $key++)
			{
				$inputs['price.'.$key] = $price_origin[$key];
			}

			return Redirect::back()->withErrors($validator)->withInput($inputs);
		}



		// update sell in
		$sellin = Sellin::findOrFail($sellin_id);
		$sellin->sales_id = $inputs['sales_id'];
		$sellin->cabang_id = $inputs['cabang_id'];
		$sellin->subcabang_id = $inputs['subcabang_id'];
		$sellin->dealer_id = $inputs['dealer_id'];
		$sellin->datetime = date_create_from_format('m/d/Y H:i', $inputs['datetime']);
		$sellin->save();
		// $sellin->update($data_sellin);

		// echo $inputs['datetime'];
		// // echo Carbon::createFromFormat('d/m/Y H:i', $inputs['datetime'])->toDateTimeString();
		// $date = date_create_from_format('d/m/Y H:i', $inputs['datetime']);
		// echo $date->getTimestamp();

		// prepare data
		$sellins_count = count($sellin->productins);
		$productins = $sellin->productins;

		// loop product in from db
		foreach ($productins as $key_productin => $productin) 
		{
			$match = false;

			// loop product in form input
			for ($key_input=0; $key_input < $transactions_count; $key_input++)
			{
				if ($productin->id == $inputs['productin_id.'.$key_input]) $match = true;
			}

			// if not match delete product in
			if (!$match) {
				Productin::destroy($productin->id);
			}

		}

		// loop product in form input
		for ($key_input=0; $key_input < $transactions_count; $key_input++)
		{

			$productin = Productin::find(Input::get('productin_id.'.$key_input));

			// if match then update product in
			if ($productin) {

				// echo '<h3>Update: '.$inputs['product_id.'.$key_input].'</h3>';

				$productin->product_id = $inputs['product_id.'.$key_input];
				$productin->quantity = $inputs['quantity.'.$key_input];
				$productin->price = $inputs['price.'.$key_input];
				$productin->save();

			// if not match create new product in
			} else {

				// echo '<h3>Create: '.$inputs['product_id.'.$key_input].'</h3>';
				Productin::create([
					'product_id' => $inputs['product_id.'.$key_input],
					'quantity' => $inputs['quantity.'.$key_input],
					'price' => $inputs['price.'.$key_input],
					'sellin_id' => $sellin_id
				]);
			}
		}

		return Redirect::route('sellin.index')->withSuccess('Sellin has been updated successfully');
	}

	/**
	 * Remove the specified sellin from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;
		$sellin = Sellin::findOrFail($id);

		// if sales has move to to other cabang
		if ($userRole == 'sales' && $sellin->cabang_id != $user->cabang->id) {
			return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Cabang.']);
		}

		Sellin::destroy($id);

		return Redirect::route('sellin.index')->withSuccess('Sellin has been deleted successfully');
	}
	public function destroySelected()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'sales') {
			$sellins = Sellin::whereIn('id', Input::get('ids'))->get();
			for ($i=0; $i < $sellins->count(); $i++) { 
				// if sales has move to to other cabang
				if ($sellins[$i]->cabang_id != $user->cabang->id) {
					return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Cabang.']);
				}
			}
		}

		Sellin::destroy(Input::get('ids'));

		return Redirect::route('sellin.index')->withSuccess('Selected Sellin has been deleted successfully');
	}

}
