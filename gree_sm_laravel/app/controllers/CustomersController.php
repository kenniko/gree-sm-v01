<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomersController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.customer');
    }

	/**
	 * Display a listing of customers
	 *
	 * @return Response
	 */
	public function index()
	{
		$customers = Customer::all();

		return View::make('customers.index', compact('customers'));
	}

	/**
	 * Show the form for creating a new customer
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('customers.create');
	}

	/**
	 * Store a newly created customer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Customer::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Customer::create($data);

		return Redirect::route('customer.index')->withSuccess('Customer '.Input::get('name').' has been created successfully.');
	}

	/**
	 * Display the specified customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$customer = Customer::findOrFail($id);
		 	
		 	return View::make('customers.edit', compact('customer'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('customer.index');
		}
	}

	/**
	 * Show the form for editing the specified customer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified customer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$customer = Customer::findOrFail($id);

		$data = Input::all();

		$rules = Customer::$rules;

		if ($data['name'] == $customer['name']) {
			unset($rules['name']);
		}

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$customer->update($data);

		return Redirect::route('customer.index')->withSuccess('Customer '.Input::get('name').' has been updated successfully.');
	}

	/**
	 * Remove the specified customer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sellouts = Sellout::where('customer_id', $id)->count();

		// check if this data have relation with others data
		if ($sellouts > 0) {
			return Redirect::back()->withErrors(['Can not perform delete.<br>This customer has included in some transactions.']);

		// if this data free to go
		} else {
			Customer::destroy($id);
			return Redirect::route('customer.index')->withSuccess('Customer '.Input::get('name').' has been deleted successfully');
		}
	}
	public function destroySelected()
	{
		$sellouts = Sellout::whereIn('customer_id', Input::get('ids'))->count();

		// check if this data have relation with others data
		if ($sellouts > 0) {
			return Redirect::route('customer.index')->withErrors(['Can not perform delete.<br>Some customers have included in some transactions.']);

		// if this data free to go
		} else {
			Customer::destroy(Input::get('ids'));
			return Redirect::route('customer.index')->withSuccess('Selected Customer has been deleted successfully.');
		}
	}

}
