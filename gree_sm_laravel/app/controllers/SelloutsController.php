<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;

class SelloutsController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth.sellout');
    }

	/**
	 * Display a listing of sellouts
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'spm') 
		{
			$sellouts = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->where('sellouts.spm_id', '=', $user->id)
					->select('sellouts.*', DB::raw('sum(productouts.quantity*productouts.price) AS total_price'))
					->groupBy('sellouts.id')->get();
		} 
		elseif ($userRole == 'cabang') 
		{
			$sellouts = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->where('sellouts.cabang_id', '=', $user->cabang->id)
					->select('sellouts.*', DB::raw('sum(productouts.quantity*productouts.price) AS total_price'))
					->groupBy('sellouts.id')->get();
		} 
		else 
		{
			$sellouts = Sellout::leftJoin('productouts', 'productouts.sellout_id', '=', 'sellouts.id')
					->select('sellouts.*', DB::raw('sum(productouts.quantity*productouts.price) AS total_price'))
					->groupBy('sellouts.id')->get();
		}
		// $sellouts = Sellout::all();

		return View::make('sellouts.index', compact('sellouts'));
	}

	/**
	 * Show the form for creating a new sellout
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		// $products = Product::all()->lists('name_and_code', 'id');
		$products = Product::leftJoin('productins', 'productins.product_id', '=', 'products.id')
						->distinct()->get()->lists('name_and_code', 'product_id');

		if ($userRole == 'spm') {
			$spm = [$user->id => $user->full_name];
			$cabangs = [$user->dealer->subcabang->cabang->id => $user->dealer->subcabang->cabang->name];
			$subcabangs = [$user->dealer->subcabang->id => $user->dealer->subcabang->name];
			$dealers = [$user->dealer->id => $user->dealer->name];
			return View::make('sellouts.create', compact('sales', 'cabangs', 'subcabangs', 'dealers', 'products', 'spm'));
		} if ($userRole == 'cabang') {
			$salesRole = Sentinel::findRoleBySlug('sales');
			$sales = $salesRole->users()->where('cabang_id', $user->cabang->id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
			$cabangs = [$user->cabang->id => $user->cabang->name];
			$subcabangs = Subcabang::where('cabang_id', $user->cabang->id)->lists('name', 'id');
			return View::make('sellouts.create', compact('sales', 'cabangs', 'subcabangs', 'products'));
		} else {
			$cabangs = Cabang::lists('name', 'id');
			return View::make('sellouts.create', compact('cabangs', 'products'));
		}
	}

	/**
	 * Store a newly created sellout in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// get sell out rules
		$rules = Sellout::$rules;
		unset($rules['customer_id']);
		// get customer rules

		// get sell out & customer inputs
		$inputs = Input::only('type', 'datetime', 'sales_id', 'spm_id', 'cabang_id', 'subcabang_id', 'dealer_id', 'name', 'phone');

		if (!is_numeric($inputs['name'])) {
			$rules['name'] = Customer::$rules['name'];
		}

		// convert date
		$datetime_origin = $inputs['datetime'];
		if ($inputs['datetime']) {
			$datetime = str_replace('- ', '', $inputs['datetime']);
			$dates = explode("/", $datetime);
			$inputs['datetime'] = $dates[1].'/'.$dates[0].'/'.$dates[2];
		}

		$inputs['transactions'] = [];
		$price_origin = [];

		$transactions_count = count(Input::get('product_id'));

		for ($key=0; $key < $transactions_count; $key++)
		{
			$rules['product_id.'.$key] = Productout::$rules['product_id'];
			$inputs['product_id.'.$key] = Input::get('product_id.'.$key);

			$productInSum = Productin::where('product_id', '=', Input::get('product_id.'.$key))->sum('quantity');
			$rules['quantity.'.$key] = 'required|numeric|min:1|max:'.$productInSum;
			$inputs['quantity.'.$key] = Input::get('quantity.'.$key);

			$rules['price.'.$key] = Productout::$rules['price'];
			array_push($price_origin, Input::get('price.'.$key));
			$inputs['price.'.$key] = str_replace('.', '', Input::get('price.'.$key));

			array_push($inputs['transactions'], $key);

		}

		$messages = array(
		    'required' => 'This field is required.',
		    'numeric' => 'This field must be a number.',
		    "min" => "This field must be at least :min.",
			"max" => "This field may not be greater than :max."
		);

		// validate
		$validator = Validator::make($inputs, $rules, $messages);

		// check if cabang filled
		if (!empty($cabang_id = $inputs['cabang_id'])) {
			$inputs['subcabangs'] = Subcabang::where('cabang_id', $cabang_id)->lists('name', 'id');
			$salesRole = Sentinel::findRoleBySlug('sales');
			$inputs['sales'] = $salesRole->users()->where('cabang_id', $cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}
		// check if subcabang filled
		if (!empty($subcabang_id = $inputs['subcabang_id'])) {
			$inputs['dealers'] = Dealer::where('subcabang_id', $subcabang_id)->lists('name', 'id');
		}
		// check if dealer filled
		if (!empty($dealer_id = $inputs['dealer_id'])) {
			$spmRole = Sentinel::findRoleBySlug('spm');
			$inputs['spm'] = $spmRole->users()->where('dealer_id', $dealer_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}

		if ($validator->fails())
		{
			// conver input to origin
			$inputs['datetime'] = $datetime_origin;
			if (is_numeric($inputs['name'])) {
				$inputs['name_real'] = Customer::where('id', $inputs['name'] )->first()->name;
			} else {
				$inputs['name_real'] = $inputs['name'];
			}

			for ($key=0; $key < $transactions_count; $key++)
			{
				$inputs['price.'.$key] = $price_origin[$key];
			}

			return Redirect::back()->withErrors($validator)->withInput($inputs);
		}

		// create or update Customer
		if (is_numeric($inputs['name'])) {
			$Customer = Customer::find($inputs['name']);
			$Customer->phone = $inputs['phone'];
			$Customer->save();
		} else {
			$Customer = Customer::create([
				'name' => $inputs['name'],
				'phone' => $inputs['phone']
			]);
		}

		// create Sellout
		$Sellout = Sellout::create([
			'sales_id' => $inputs['sales_id'],
			'cabang_id' => $inputs['cabang_id'],
			'subcabang_id' => $inputs['subcabang_id'],
			'dealer_id' => $inputs['dealer_id'],
			'spm_id' => $inputs['spm_id'],
			'type' => $inputs['type'],
			'customer_id' => $Customer->id,
			'datetime' => date_create_from_format('m/d/Y H:i', $inputs['datetime'])
		]);

		for ($key=0; $key < $transactions_count; $key++)
		{
			Productout::create([
				'product_id' => $inputs['product_id.'.$key],
				'quantity' => $inputs['quantity.'.$key],
				'price' => $inputs['price.'.$key],
				'sellout_id' => $Sellout->id
			]);
		}

		return Redirect::route('sellout.index')->withSuccess('Sellout has been created successfully');
	}

	/**
	 * Display the specified sellout.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try 
		{
		 	$sellout = Sellout::findOrFail($id);

		 	$user = Sentinel::getUser();
			$userRole = $user->roles()->first()->slug;

			if ($userRole == 'spm') {
				$spm = [$user->id => $user->full_name];
				// if spm still on the same dealer
				if($sellout->dealer_id == $user->dealer->id) {
					$cabangs = [$user->dealer->subcabang->cabang->id => $user->dealer->subcabang->cabang->name];
					$subcabangs = [$user->dealer->subcabang->id => $user->dealer->subcabang->name];
					$dealers = [$user->dealer->id => $user->dealer->name];
				// if spm has move to to other dealer
				} else {
					$cabangs = Cabang::where('id', $sellout->cabang_id)->lists('name', 'id');
					$subcabangs = Subcabang::where('id', $sellout->subcabang_id)->lists('name', 'id');
					$dealers = Dealer::where('id', $sellout->dealer_id)->lists('name', 'id');
				}
			} elseif ($userRole == 'cabang') {
				$cabangs = [$user->cabang->id => $user->cabang->name];
				$subcabangs = Subcabang::where('cabang_id', $sellout->cabang_id)->lists('name', 'id');
		 		$dealers = Dealer::where('subcabang_id', $sellout->subcabang_id)->lists('name', 'id');
		 		$salesRole = Sentinel::findRoleBySlug('sales');
				$sales = $salesRole->users()->where('cabang_id', $user->cabang->id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
				$spmRole = Sentinel::findRoleBySlug('spm');
				$spm = $spmRole->users()->where('dealer_id', $sellout->dealer_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
			} else {
				$cabangs = Cabang::lists('name', 'id');
				$subcabangs = Subcabang::where('cabang_id', $sellout->cabang_id)->lists('name', 'id');
		 		$dealers = Dealer::where('subcabang_id', $sellout->subcabang_id)->lists('name', 'id');
		 		$salesRole = Sentinel::findRoleBySlug('sales');
				$sales = $salesRole->users()->where('cabang_id', $sellout->cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
				$spmRole = Sentinel::findRoleBySlug('spm');
				$spm = $spmRole->users()->where('dealer_id', $sellout->dealer_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->get()->lists('name', 'id');
			}

		 	$customer = Customer::find($sellout->customer_id);

		 	$productouts = Productout::where('sellout_id', $id)->get();
		 	foreach ($productouts as &$productout) {
		 		$productout['price'] = number_format($productout['price'], 0, '', '.');
		 	}

		 	// $products = Product::all()->lists('name_and_code', 'id');
		 	$products = Product::leftJoin('productins', 'productins.product_id', '=', 'products.id')
						->distinct()->get()->lists('name_and_code', 'product_id');
		 	
		 	return View::make('sellouts.edit', compact('sellout', 'spm', 'sales', 'cabangs', 'subcabangs', 'dealers', 'customer', 'productouts', 'products'));
		} 
		catch (ModelNotFoundException $ex) 
		{
		  	return Redirect::route('sellout.index');
		}
	}

	/**
	 * Show the form for editing the specified sellout.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Response::view('404');
	}

	/**
	 * Update the specified sellout in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($sellout_id)
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		// if sales has move to to other cabang
		if ($userRole == 'spm' && Input::get('dealer_id') != $user->dealer->id) {
			return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Dealer.']);
		}

		// get sell out rules
		$rules = Sellout::$rules;
		unset($rules['customer_id']);
		// get customer rules

		// get sell out & customer inputs
		$inputs = Input::only('type', 'datetime', 'sales_id', 'spm_id', 'cabang_id', 'subcabang_id', 'dealer_id', 'name', 'phone');

		if (!is_numeric($inputs['name'])) {
			$rules['name'] = Customer::$rules['name'];
		}

		// convert date
		$datetime_origin = $inputs['datetime'];
		if ($inputs['datetime']) {
			$datetime = str_replace('- ', '', $inputs['datetime']);
			$dates = explode("/", $datetime);
			$inputs['datetime'] = $dates[1].'/'.$dates[0].'/'.$dates[2];
		}

		$inputs['transactions'] = [];
		$price_origin = [];

		$transactions_count = count(Input::get('product_id'));

		for ($key=0; $key < $transactions_count; $key++)
		{
			$inputs['productout_id.'.$key] = Input::get('productout_id.'.$key);

			$rules['product_id.'.$key] = Productout::$rules['product_id'];
			$inputs['product_id.'.$key] = Input::get('product_id.'.$key);

			$productInSum = Productin::where('product_id', '=', Input::get('product_id.'.$key))->sum('quantity');
			$rules['quantity.'.$key] = 'required|numeric|min:1|max:'.$productInSum;
			$inputs['quantity.'.$key] = Input::get('quantity.'.$key);
			
			$rules['price.'.$key] = Productout::$rules['price'];
			array_push($price_origin, Input::get('price.'.$key));
			$inputs['price.'.$key] = str_replace('.', '', Input::get('price.'.$key));

			array_push($inputs['transactions'], $key);

		}

		$messages = array(
		    'required' => 'This field is required.',
		    'numeric' => 'This field must be a number.',
		    "min" => "This field must be at least :min.",
			"max" => "This field may not be greater than :max."
		);

		// validate
		$validator = Validator::make($inputs, $rules, $messages);

		// check if cabang filled
		if (!empty($cabang_id = $inputs['cabang_id'])) {
			$inputs['subcabangs'] = Subcabang::where('cabang_id', $cabang_id)->lists('name', 'id');
			$salesRole = Sentinel::findRoleBySlug('sales');
			$inputs['sales'] = $salesRole->users()->where('cabang_id', $cabang_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}
		// check if subcabang filled
		if (!empty($subcabang_id = $inputs['subcabang_id'])) {
			$inputs['dealers'] = Dealer::where('subcabang_id', $subcabang_id)->lists('name', 'id');
		}
		// check if dealer filled
		if (!empty($dealer_id = $inputs['dealer_id'])) {
			$spmRole = Sentinel::findRoleBySlug('spm');
			$inputs['spm'] = $spmRole->users()->where('dealer_id', $dealer_id)->select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))->lists('name', 'id');
		}

		if ($validator->fails())
		{
			// conver input to origin
			$inputs['datetime'] = $datetime_origin;
			if (is_numeric($inputs['name'])) {
				$inputs['name_real'] = Customer::find( $inputs['name'] )->first()->name;
			} else {
				$inputs['name_real'] = $inputs['name'];
			}

			for ($key=0; $key < $transactions_count; $key++)
			{
				$inputs['price.'.$key] = $price_origin[$key];
			}

			return Redirect::back()->withErrors($validator)->withInput($inputs);
		}

		// create or update Customer
		if (is_numeric($inputs['name'])) {
			$Customer = Customer::find($inputs['name']);
			$Customer->phone = $inputs['phone'];
			$Customer->save();
		} else {
			$Customer = Customer::create([
				'name' => $inputs['name'],
				'phone' => $inputs['phone']
			]);
		}

		// update sell in
		$Sellout = Sellout::findOrFail($sellout_id);
		$Sellout->sales_id = $inputs['sales_id'];
		$Sellout->cabang_id = $inputs['cabang_id'];
		$Sellout->subcabang_id = $inputs['subcabang_id'];
		$Sellout->dealer_id = $inputs['dealer_id'];
		$Sellout->spm_id = $inputs['spm_id'];
		$Sellout->type = $inputs['type'];
		$Sellout->customer_id = $Customer->id;
		$Sellout->datetime = date_create_from_format('m/d/Y H:i', $inputs['datetime']);
		$Sellout->save();

		// prepare data
		$sellouts_count = count($Sellout->productouts);
		$productouts = $Sellout->productouts;

		// loop product in from db
		foreach ($productouts as $key_productout => $productout) 
		{
			$match = false;

			// loop product in form input
			for ($key_input=0; $key_input < $transactions_count; $key_input++)
			{
				if ($productout->id == $inputs['productout_id.'.$key_input]) $match = true;
			}

			// if not match delete product in
			if (!$match) {
				Productout::destroy($productout->id);
			}

		}

		// loop product in form input
		for ($key_input=0; $key_input < $transactions_count; $key_input++)
		{

			$productout = Productout::find(Input::get('productout_id.'.$key_input));

			// if match then update product in
			if ($productout) {

				// echo '<h3>Update: '.$inputs['product_id.'.$key_input].'</h3>';

				$productout->product_id = $inputs['product_id.'.$key_input];
				$productout->quantity = $inputs['quantity.'.$key_input];
				$productout->price = $inputs['price.'.$key_input];
				$productout->save();

			// if not match create new product in
			} else {

				// echo '<h3>Create: '.$inputs['product_id.'.$key_input].'</h3>';
				Productout::create([
					'product_id' => $inputs['product_id.'.$key_input],
					'quantity' => $inputs['quantity.'.$key_input],
					'price' => $inputs['price.'.$key_input],
					'sellout_id' => $sellout_id
				]);
			}
		}

		return Redirect::route('sellout.index')->withSuccess('Sellout has been updated successfully');
	}

	/**
	 * Remove the specified sellout from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;
		$sellout = Sellout::findOrFail($id);

		// if sales has move to other cabang
		if ($userRole == 'spm' && $sellout->dealer_id != $user->dealer->id) {
			return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Dealer.']);
		}

		Sellout::destroy($id);

		return Redirect::route('sellout.index')->withSuccess('Sellout has been deleted successfully');
	}
	public function destroySelected()
	{
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;

		if ($userRole == 'spm') {
			$sellouts = Sellout::whereIn('id', Input::get('ids'))->get();
			for ($i=0; $i < $sellouts->count(); $i++) { 
				// if sales has move to to other cabang
				if ($sellouts[$i]->dealer_id != $user->dealer->id) {
					return Redirect::back()->withErrors(['Can not perform update or delete.<br>You already move to other Dealer.']);
				}
			}
		}

		Sellout::destroy(Input::get('ids'));

		return Redirect::route('sellout.index')->withSuccess('Selected Sellout has been deleted successfully');
	}

}
