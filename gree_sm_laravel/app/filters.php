<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	// check Loging
	if (!Sentinel::check()) {
		return Redirect::guest('login');

	// check Active
	} else {
		$user = Sentinel::getUser();
		$userRole = $user->roles()->first()->slug;
		if ($userRole == 'sales' && empty($user->cabang()->first()->id) && Route::getCurrentRoute()->getPath() != 'user/{user}') {
			return Redirect::to('/user/'.$user->id)->withErrors(["Set your Cabang to activate your account."]);
		} elseif ($userRole == 'spm' && empty($user->dealer()->first()->id) && Route::getCurrentRoute()->getPath() != 'user/{user}') {
			return Redirect::to('/user/'.$user->id)->withErrors(["Set your Dealer to activate your account."]);
		} elseif ($userRole == 'cabang' && empty($user->cabang()->first()->id) && Route::getCurrentRoute()->getPath() != 'user/{user}') {
			return Redirect::to('/user/'.$user->id)->withErrors(["Set your Cabang to activate your account."]);
		}
	}
});

Route::filter('auth.cabang', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('cabang')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.subcabang', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('subcabang')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.dealer', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('dealer')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.customer', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('customer')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.product', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('product')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.sellin', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('sellin')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.sellout', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('sellout')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.stockopname', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('stockopname')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.report', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('report')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.user', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('user')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.role', function()
{
	if (!Sentinel::check()) {
		return Redirect::guest('login');
	} elseif (Sentinel::check() && ! Sentinel::hasAccess('role')) {
		return Redirect::to('/')->withErrors(["Sorry, you don't have access to this page."]);
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Sentinel::check()) return Redirect::intended('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
