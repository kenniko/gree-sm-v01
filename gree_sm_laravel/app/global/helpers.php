<?php

	function getUserFullName()
	{
		return Sentinel::getUser()->first_name.' '.Sentinel::getUser()->last_name;
	}

	function getUserRole()
	{
		if ($user = Sentinel::getUser()) {
	        return $user->roles()->first();
	    }
	}

	function getCurrectUser()
	{
		return Sentinel::getUser();
	}

	function getUserAvatar()
	{
		if (Sentinel::getUser()->gender == 'Male') return URL::to('img/avatar5.png');
		if (Sentinel::getUser()->gender == 'Female') return URL::to('img/avatar3.png');
	}

	function getCompany()
	{
		if ($user = Sentinel::getUser()) {
	        $role = $user->roles()->first()->slug;
	        if ($role == 'sales') {
	        	$cabang = $user->cabang()->first();
	        	if ($cabang) return 'Cabang '.$cabang->name;
	        } else if ($role == 'cabang') {
	        	$cabang = $user->cabang()->first();
	        	if ($cabang) return $cabang->name;
	        } else if ($role == 'spm') {
	        	$dealer = $user->dealer()->first();
	        	if ($dealer) return 'Dealer '.$dealer->name;
	        }
	    }
	}

	function setActiveMenu($path, $active='active')
	{
		return Request::is($path) || Request::is($path . '/*') ? $active: '';
	}

	function hideIfNoAccess($permission)
	{
		if (! Sentinel::hasAccess($permission)) return 'hidden';
	}

	function hasAccess($permission)
	{
		if (Sentinel::hasAccess($permission)) return true;
		else return false;
	}


	// function set_active($path, $active='active')
	// {
	// 		return Request::is($path) || Request::is($path . '/*') ? $active: '';
	// }

	// function set_active_admin($path, $active='active')
	// {
	// 		return Request::is($path) ? $active: '';
	// }