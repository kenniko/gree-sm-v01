<?php

# CSRF Protection
Route::when('*', 'csrf', ['POST', 'PUT', 'PATCH', 'DELETE']);

# Authentication
Route::get('login', 'AuthController@index')->before('guest');
Route::post('login', 'AuthController@store');
Route::get('logout', 'AuthController@destroy');

# Standard User Routes
Route::group(['before' => 'auth'], function() {
	Route::get('/', 'PageController@dashboard');

	Route::get('ajax/{data}', 'AjaxController@getAllData');

	Route::resource('cabang', 'CabangsController');
	Route::delete('cabangs/delete', 'CabangsController@destroySelected');

	Route::get('rangking', ['as' => 'rangking.index', 'uses' => 'CabangsController@indexRangking' ]);
	Route::get('rangking/{id}', 'CabangsController@showRangking');
	Route::patch('rangking/{id}', ['as' => 'rangking.update', 'uses' => 'CabangsController@updateRangking' ]);

	Route::resource('subcabang', 'SubcabangsController');
	Route::delete('subcabangs/delete', 'SubcabangsController@destroySelected');
	Route::patch('subcabangs/update', 'SubcabangsController@updateSelected');
	Route::patch('subcabangs/remove', 'SubcabangsController@removeSelected');

	Route::resource('dealer', 'DealersController');
	Route::delete('dealers/delete', 'DealersController@destroySelected');
	Route::patch('dealers/update', 'DealersController@updateSelected');
	Route::patch('dealers/remove', 'DealersController@removeSelected');

	Route::resource('product', 'ProductsController');
	Route::delete('products/delete', 'ProductsController@destroySelected');

	Route::resource('customer', 'CustomersController');
	Route::delete('customers/delete', 'CustomersController@destroySelected');

	Route::resource('sellin', 'SellinsController');
	Route::delete('sellins/delete', 'SellinsController@destroySelected');

	Route::resource('sellout', 'SelloutsController');
	Route::delete('sellouts/delete', 'SelloutsController@destroySelected');

	Route::get('stockopname', 'ProductsController@showStockopname');

	Route::get('report', 'CabangsController@showReport');
	Route::get('export/{page}/{type}', 'CabangsController@export');

	Route::resource('user', 'UsersController');
	Route::delete('users/delete', 'UsersController@destroySelected');
	Route::patch('users/update', 'UsersController@updateSelected');
	Route::patch('users/remove', 'UsersController@removeSelected');

	Route::resource('role', 'RolesController');

});


#IF Method not Allowed
// Route::any('{all}', function(){
//     return View::make('404');
// })->where('all', '.*');