// window.addEventListener('load', function() {
//     new FastClick(document.body);
// }, false);

$(function () {

	$(document).ajaxStart(function() { Pace.restart(); });

	// IChek
	$('.icheck').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		// increaseArea: '20%' // optional
	});

	// click on Table
	$('tr[href]').find('td:not([disabled])').click(function(ev){
		ev.preventDefault();
		// console.log("test");
		var url = $(this).parent().attr('href');
		window.location.href = url;
	});

	// prettify Date
	$('.prettify-date').each(function(i, e){
		var date = $(e).text(),
			prettyDate = moment( date );
		$(e).html( prettyDate.format('MMMM D, YYYY') );
		// $(e).html( prettyDate.calendar() );
		// console.log(date);
	});

	// prettify TIme
	$('.prettify-time').each(function(i, e){
		var date = $(e).text(),
			prettyDate = moment( date );
		$(e).html( prettyDate.format('HH:mm A') );
	});

	$('.btn--print').click(function(event) {
		event.preventDefault();
		window.print();
	})
	
});